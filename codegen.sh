#!/bin/sh
#
# Copyright (C) 2004, Christian Thaeter <chth@gmx.net>
#
# This file is part of the MaLa extension Language.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, contact me.

for module in std string arith system gnu; do
  ../tools/codegen -p `readlink -f ./$module/` -l C `for i in ../mala/module-defs/$module*.def; do readlink -f $i; done`
done

# arch-tag: a97e24d2-abd2-46a1-a052-35564df3a032
