/*
    action.c - MaLa actions implementation

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#include <stdio.h>

#include "acogc.h"
#include "mala_types.h"
#include "strings.h"
#include "stringlist.h"
#include "engine.h"
#include "action.h"
#include "llist.h"


void
mala_action_acogc_initize (void* o)
{
  TRACE(mala_acogc);
  MalaAction self = (MalaAction)o;
  self->name = NULL;
  llist_init (&self->childs);
  llist_init (&self->cldnode);
  llist_init (&self->stknode);
  self->data = NULL;
  self->data_alloc_type = ACOGC_UNSET;
}

void
mala_action_acogc_finalize (void* o)
{
  TRACE(mala_acogc);
  MalaAction self = (MalaAction)o;
  llist_unlink (&self->childs);
  llist_unlink (&self->cldnode);
  llist_unlink (&self->stknode);
  if (self->data_alloc_type == ACOGC_ALLOC)
    acogc_free (&self);
}

acogc_mark_result
mala_action_acogc_mark (void* o)
{
  TRACE(mala_acogc);
  MalaAction self = (MalaAction)o;

  TODO("profile marking order maybe lastmark name instead of program");

  TODO("iterative marker");
  LLIST_FOREACH (&self->childs, node)
    acogc_object_mark (LLIST_TO_STRUCTP (node, mala_action, cldnode));

  LLIST_FOREACH (&self->stknode, node)
    acogc_object_mark (LLIST_TO_STRUCTP (node, mala_action, stknode));

  if (self->data_alloc_type == ACOGC_GC)
    acogc_object_mark (self->data);

  acogc_object_lastmark (self->name);
  //acogc_object_mark (self->name);

  return ACOGC_KEEP;
}

MalaAction
mala_action_new (MalaString name,
                 MalaParserFunc parser,
                 void * data,
                 acogc_alloc_type data_alloc,
                 MalaEngine engine)
{
  REQUIRE (name);
  REQUIRE (engine);
  REQUIRE (data || data_alloc==ACOGC_UNSET);
  TRACE (mala_action);

  MalaAction self = acogc_factory_alloc (&engine->gcfactory_actions);

  self->name = name;
  self->parser = parser;
  self->data = data;
  self->data_alloc_type = data_alloc;
  llist_init (&self->childs);

  self->parent = NULL;
  if (name->action)
    {
      //push
      llist_insert_after (&self->stknode, &name->action->stknode);
    }
  else
    {
      // first action
      REQUIRE (name->user_alloc_type == ACOGC_UNSET);
      name->user_alloc_type = ACOGC_GC;
    }
  name->action = self;

  return self;
}

void
mala_action_delete (MalaAction self)
{
  REQUIRE (self);
  REQUIRE (self->name);
  REQUIRE (self->name->action);
  TRACE (mala_action, "%s", mala_string_cstr (self->name));

  self->name->action = LLIST_TO_STRUCTP( llist_get_next (&self->stknode), mala_action, stknode);

  FIXME ("delete childs!");

  if (self->name->action == self)
    {
      // was last action, destroy stack
      self->name->action = NULL;
      self->name->user_alloc_type = ACOGC_UNSET;
    }
  else
    llist_unlink (&self->stknode);
}

MalaAction
mala_action_new_actioninit (MalaActionInit init, MalaEngine engine)
{
  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaString, name);
  ACOGC_STACK_PTR (MalaAction, self);
  TRACE (mala_action, "%s", init->name);

  name.ptr = mala_string_new_cstr (init->name, engine->words);

  self.ptr = mala_action_new (name.ptr, init->parser, NULL, ACOGC_UNSET, engine);

  switch (init->type)
    {
    case MALA_DATA_STRINGLIST:
      self.ptr->list = mala_stringlist_new (engine);
      self.ptr->data_alloc_type = ACOGC_GC;
      mala_stringlist_append_literal_cstrs (self.ptr->list, -1, init->data, engine);
      break;
    case MALA_DATA_STRINGPOINTER:
      self.ptr->string = mala_string_new_literal_cstr (init->data, engine->words);
      self.ptr->data_alloc_type = ACOGC_GC;
      break;
      //case MALA_DATA_UNUSED:
    default:
      self.ptr->data = init->data;
      self.ptr->data_alloc_type = init->data ? ACOGC_STATIC : ACOGC_UNSET;
      break;
    }

  if (init->parent)
    {
      MalaString str = mala_stringbucket_find (engine->words, init->parent);
      if (str)
        mala_action_attach_child (str->action, self.ptr);
      else
        mala_action_attach_child (engine->common_string[MALA_STRING_WORLD]->action, self.ptr);
    }
  ACOGC_STACK_LEAVE;
  return self.ptr;
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 5b3b4c3c-09f4-4f2e-a737-1ea6c5df59e9
// end_of_file
*/
