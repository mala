/*
    stringbucket.h - MaLa stringbuckets

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_TYPES_H
#define MALA_TYPES_H

#include <nobug.h>
#include "mala_globals.c"       // only extern declarations when included from here

NOBUG_DECLARE_FLAG(mala);
NOBUG_DECLARE_FLAG(mala_acogc);
NOBUG_DECLARE_FLAG(mala_engine);
NOBUG_DECLARE_FLAG(mala_program);
NOBUG_DECLARE_FLAG(mala_strings);
NOBUG_DECLARE_FLAG(mala_stringlist);
NOBUG_DECLARE_FLAG(mala_stringbucket);
NOBUG_DECLARE_FLAG(mala_action);
NOBUG_DECLARE_FLAG(mala_module);
NOBUG_DECLARE_FLAG(mala_module_std);


/*
 * mala state codes
 */
#define MALA_STATES \
  /* the first codes are the states of the MaLa VM and never returned from the engine*/ \
  MALA_STATE(SUCCESS),          /* final state / all succeeded */ \
  MALA_STATE(LITERAL),          /* don't evaluate argument */ \
  MALA_STATE(MACRO),            /* evaluated some new code */ \
  MALA_STATE(BLOCK),            /* block of code evaluated to a macro */ \
  MALA_STATE(STATEMENT),        /* evaluated to nothing, but had some side-effect */  \
  MALA_STATE(REMOVE),           /* tell a command to remove itself recursively */ \
  MALA_STATE(START),            /* start of the statemachine, nothing evaluated yet */ \
  MALA_STATE(EXCEPTION),        /* some error occured (in MaLa code) */ \
 \
  MALA_STATE(EFAULT),   /* beyond are runtime errors */ \
  MALA_STATE(ENOACTION),        /* no action found, exception falltrough */ \
  MALA_STATE(EACTIONUSED),      /* action has childs or is linked to an engine */ \
  MALA_STATE(ESYNTAX),          /* Syntax error in expansion */ \
  MALA_STATE(EINCOMPLETE),      /* interrupted in interactive mode */ \
 \
  MALA_STATE(EFATAL),   /* beyond are fatal errors */ \
  MALA_STATE(EINIT),            /* initialization didnt complete */ \
  MALA_STATE(EALLOC),           /* memory allocation problem */ \
  MALA_STATE(ENOENV),           /* no environment space left */ \
  MALA_STATE(EPARSER),          /* parser wrong or missing */ \
 \
  MALA_STATE(EINVALID),         /* something is really wrong */ \
  MALA_STATE(STATE_TABLE_SIZEOF)

#define MALA_STATE(s) MALA_##s
enum mala_state_enum
{
  MALA_STATES
};
#undef MALA_STATE

typedef enum mala_state_enum mala_state;


enum mala_trace_enum
  {
    MALA_NOTRACE,       // traceing off
    MALA_TRACESOME,     // tracing only important things
    MALA_TRACE,         // normal traceing
    MALA_TRACEALL       // trace details
  };

typedef enum mala_trace_enum mala_trace;

enum mala_action_datatype_enum
  {
    MALA_DATA_UNUSED,
    MALA_DATA_STRINGLIST,
    MALA_DATA_STRINGPOINTER,
    MALA_DATA_EXTERNAL
  };

typedef enum mala_action_datatype_enum mala_action_datatype;



/*
 * datastructure and handle declarations
 */
#define MALA_TYPEDEFS(name,Handle)                      \
  typedef struct mala_##name##_struct mala_##name;      \
  typedef const mala_##name * const_Mala##Handle;       \
  typedef mala_##name * Mala##Handle;                   \
  typedef mala_##name ** Mala##Handle##_ref;            \
  typedef const mala_##name ** const_Mala##Handle##_ref

MALA_TYPEDEFS(actioninit,ActionInit);
MALA_TYPEDEFS(action,Action);
MALA_TYPEDEFS(string,String);
MALA_TYPEDEFS(stringbucket,StringBucket);
MALA_TYPEDEFS(stringbucketnode,StringBucketNode);
MALA_TYPEDEFS(engine,Engine);
MALA_TYPEDEFS(program,Program);
MALA_TYPEDEFS(actiondesc,ActionDesc);
MALA_TYPEDEFS(stringlist,StringList);


typedef mala_state (*MalaParserFunc) (MalaProgram prg);

#define MALA_MUTATOR_BEGIN if (mala_program_state_get (prg) != MALA_REMOVE)
#define MALA_MUTATOR_END

#define MALA_PARSER_DEFINE(name) mala_state mala_##name##_parser (MalaProgram prg)


#endif /* MALA_TYPES_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: ee20274c-3a62-4022-a312-59572f1ffdb9
// end_of_file
*/
