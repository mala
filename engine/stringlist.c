/*
    stringlist.c - MaLa stringlists

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/


#include "stringlist.h"

acogc_mark_result
mala_stringlist_acogc_mark (void* o)
{
  TRACE (mala_acogc,"start");
  MalaStringList self = (MalaStringList) o;

  acogc_object_mark (self->string);
  acogc_object_lastmark (LLIST_TO_STRUCTP (llist_get_next (&self->node), mala_stringlist, node));
  //acogc_object_mark (LLIST_TO_STRUCTP (llist_get_next (&self->node), mala_stringlist, node));

  TRACE (mala_acogc, "finished");
  return ACOGC_KEEP;
}

void
mala_stringlist_acogc_initize (void * o)
{
  TRACE (mala_acogc);
  MalaStringList self = (MalaStringList) o;
  llist_init (&self->node);
  self->string = NULL;
}

void
mala_stringlist_acogc_finalize (void * o)
{
  TRACE (mala_acogc);
  MalaStringList self = (MalaStringList) o;
  llist_unlink (&self->node);
}

MalaStringList
mala_stringlist_new (MalaEngine engine)
{
  return acogc_factory_alloc (&engine->gcfactory_stringlists);
}

void
mala_stringlist_append_cstrs (MalaStringList self, int nelem, char** cstrs, MalaEngine engine)
{
  TRACE (mala_stringlist);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, node);

  for (; nelem != 0 && *cstrs; --nelem , ++cstrs)
    {
      node.ptr = mala_stringlist_node_new_cstr (*cstrs, engine);
      mala_stringlist_insert_tail (self, node.ptr);
    }

  ACOGC_STACK_LEAVE;
}

void
mala_stringlist_append_literal_cstrs (MalaStringList self, int nelem, char** cstrs, MalaEngine engine)
{
  TRACE (mala_stringlist);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, node);

  for (; nelem != 0 && *cstrs; --nelem , ++cstrs)
    {
      node.ptr = mala_stringlist_node_new_literal_cstr (*cstrs, engine);
      mala_stringlist_insert_tail (self, node.ptr);
    }

  ACOGC_STACK_LEAVE;
}

MalaStringList
mala_stringlist_copy (MalaStringList source, MalaEngine engine)
{
  TRACE (mala_stringlist);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, ret);
  ACOGC_STACK_PTR (MalaStringList, elem);

  ret.ptr = mala_stringlist_new (engine);

  LLIST_FOREACH (&source->node, node)
    {
      elem.ptr = mala_stringlist_node_new (mala_stringlist_string_from_llist(node), engine);
      mala_stringlist_insert_tail (ret.ptr, elem.ptr);
    }

  ACOGC_STACK_LEAVE;
  return ret.ptr;
}

MalaStringList
mala_stringlist_node_new_literal_cstr (const char * cstr, MalaEngine engine)
{
  TRACE (mala_stringlist);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, node);
  node.ptr = mala_stringlist_new (engine);
  node.ptr->string = mala_string_new_literal_cstr (cstr, engine->words);
  ACOGC_STACK_LEAVE;
  return node.ptr;
}

MalaStringList
mala_stringlist_node_new_cstr (const char * cstr, MalaEngine engine)
{
  TRACE (mala_stringlist);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, node);
  node.ptr = mala_stringlist_new (engine);
  node.ptr->string = mala_string_new_cstr (cstr, engine->words);
  ACOGC_STACK_LEAVE;
  return node.ptr;
}

MalaStringList
mala_stringlist_node_new (MalaString str, MalaEngine engine)
{
  TRACE (mala_stringlist);

  MalaStringList node = acogc_factory_alloc (&engine->gcfactory_stringlists);
  node->string = str;
  return node;
}

void
mala_stringlist_dump (MalaStringList self,
                      FILE * f,
                      const char* head,
                      const char* before,
                      const char* fmt,
                      const char* after,
                      const char* foot)
{
  unsigned lineno = 0;

  fprintf (f, head);

  LLIST_FOREACH (&self->node, n)
    {
      if (lineno++)
        fprintf (f, after, lineno);
      fprintf (f, before, lineno);
      fprintf (f, fmt, mala_stringlist_cstr_from_llist (n));
    }
  fprintf (f, foot);
}

void
nobug_mala_stringlist_dump (const_MalaStringList self,
                            const int depth,
                            const char* file,
                            const int line,
                            const char* func)
{
  if (self && depth)
    {
      LLIST_FOREACH (&self->node, n)
        {
          DUMP_LOG ("%s", mala_stringlist_cstr_from_llist (n));
        }
    }
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 75b1fbb2-0328-450d-8708-daf943e7bbf6
// end_of_file
*/
