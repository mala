/*
    engine.h - MaLa core engine

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_ENGINE_H
#define MALA_ENGINE_H

#include "mala_types.h"
#include "stringbucket.h"
#include "stringlist.h"
//#include "action.h"

/*
   Build a table of common strings, this strings are
   a) get never garbage collected (except at end)
   b) are fast accessible with their symbolic index

   note that "--WORLD" must be the first one
*/

#define MALA_COMMON_STRINGS                                                             \
{                                                                                       \
    MALA_COMMON_STRING (WORLD, "--WORLD"),                                              \
    MALA_COMMON_STRING (COMMANDLINE, "--COMMANDLINE"),                                  \
    MALA_COMMON_STRING (PROGRAM, "--PROGRAM"),                                          \
    MALA_COMMON_STRING (ARGUMENTS, "--ARGUMENTS"),                                      \
    MALA_COMMON_STRING (FALSE, "--FALSE"),                                              \
    MALA_COMMON_STRING (TRUE, "--TRUE"),                                                \
    MALA_COMMON_STRING (EMPTY, ""),                                                     \
    MALA_COMMON_STRING (ZERO, "0"),                                                     \
    MALA_COMMON_STRING (PASS, "--PASS"),                                                \
    MALA_COMMON_STRING (EXCLAMATIONMARK, "!"),                                          \
    MALA_COMMON_STRING (ASSIGN, "="),                                                   \
    MALA_COMMON_STRING (BACKQUOTE, "`"),                                                \
    MALA_COMMON_STRING (NOT, "--NOT"),                                                  \
    MALA_COMMON_STRING (NO, "--NO"),                                                    \
    MALA_COMMON_STRING (LITERAL, "--LITERAL"),                                          \
    MALA_COMMON_STRING (ENVSUBST, "--ENVSUBST"),                                        \
    MALA_COMMON_STRING (SETENV, "--SETENV"),                                            \
    MALA_COMMON_STRING (SECTION, "--SECTION"),                                          \
    MALA_COMMON_STRING (BEFORE_HERE, "--BEFORE-HERE"),                                  \
    MALA_COMMON_STRING (BEGIN, "--BEGIN"),                                              \
    MALA_COMMON_STRING (END, "--END"),                                                  \
    MALA_COMMON_STRING (DEL, "--DEL"),                                                  \
    MALA_COMMON_STRING (EVAL, "--EVAL"),                                                \
    MALA_COMMON_STRING (STOP, "--STOP"),                                                \
    MALA_COMMON_STRING (TRY, "--TRY"),                                                  \
    MALA_COMMON_STRING (ERROR_MISSING_ARGUMENT, "--ERROR-MISSING-ARGUMENT"),            \
    MALA_COMMON_STRING (ERROR_MISSING_END, "--ERROR-MISSING-END"),                      \
    MALA_COMMON_STRING (ERROR_END_WITHOUT_BEGIN, "--ERROR-END-WITHOUT-BEGIN"),          \
    MALA_COMMON_STRING (ERROR_MACRO_SYNTAX, "--ERROR-MACRO-SYNTAX"),                    \
    MALA_COMMON_STRING (ERROR_NESTING_LIMIT_EXCEED, "--ERROR-NESTING-LIMIT-EXCEED"),    \
    MALA_COMMON_STRING (ERROR_ACTION_NOT_DEFINED, "--ERROR-ACTION-NOT-DEFINED"),        \
    MALA_COMMON_STRING (ERROR_NOT_A_PREDICATE, "--ERROR-NOT-A-PREDICATE"),              \
    MALA_COMMON_STRING (ERROR_WRONG_TYPE, "--ERROR-WRONG-TYPE"),                        \
    MALA_COMMON_STRING (ERROR_OUT_OF_RANGE, "--ERROR-OUT-OF-RANGE"),                    \
    MALA_COMMON_STRING (UNHANDLED_ERROR, "--UNHANDLED-ERROR"),                          \
    MALA_COMMON_STRING (MAX, "")                                                        \
}


#define MALA_COMMON_STRING(E,s) MALA_STRING_##E
enum mala_common_strings_enum MALA_COMMON_STRINGS;
#undef MALA_COMMON_STRING

typedef enum mala_common_strings_enum mala_common_string;

const char * mala_engine_states [MALA_STATE_TABLE_SIZEOF+1];

//#ifdef MALA_PRIVATE
struct mala_engine_struct
{
  /*
   * every tokenized word is collected in 'words'
   * actions can be bound to these words through the strings user variable
   */
  acogc_root gcroot;

  acogc_factory gcfactory_buckets;
  acogc_factory gcfactory_strings;
  acogc_factory gcfactory_stringbucketnodes;
  acogc_factory gcfactory_stringlists;
  acogc_factory gcfactory_actions;
  acogc_factory gcfactory_programs;

  MalaStringBucket words;

  mala_trace engine_trace;      // TODO

  /* --BEGIN ... --END blocks are translated to --BLOCK-xxxxxxxx macros,
     where xxxxxxxx is a monotonic incrementing counter, its value is stored here */
  unsigned blockcnt;

  mala_state state;

  MalaString common_string[MALA_STRING_MAX];
};


/*
 * Mala engine
 */
void
mala_engine_init (MalaEngine self);

MalaEngine
mala_engine_new ();

#if 0
MalaEngine
mala_engine_new_main (int (*initfunc) (MalaEngine),
                      int argc, char ** argv,
                      mala_actioninit * actions_init);
#endif

void
mala_engine_erase (MalaEngine self);

void
mala_engine_free (MalaEngine self);

mala_state
mala_engine_actions_register (MalaEngine, MalaActionInit);


void
mala_engine_action_new_cstr (MalaEngine self,
                             const char* name,
                             MalaParserFunc parser,
                             void *data,
                             acogc_alloc_type data_alloc,
                             MalaAction parent);


void
mala_engine_run_cstr (MalaEngine self,
                      const char* cmd,
                      MalaStringList_ref result);

mala_state
mala_engine_state_get (MalaEngine self);

const char *
mala_engine_state_str (MalaEngine self);

void
mala_engine_clearstate (MalaEngine self);

MalaString
mala_engine_tmpname (MalaEngine self, const char* templ);

static inline MalaAction
mala_engine_action_get_cstr (MalaEngine engine, const char * cname)
{
  MalaString s = mala_stringbucket_find (engine->words, cname);
  return  s ? s->action : NULL;
}

#endif /*MALA_ENGINE_H*/
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 47d718f6-e397-400c-93a6-bc71f475c0c7
// end_of_file
*/
