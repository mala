/*
    strings.c - MaLa string handling

  Copyright (C) 2004, 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

/*
 * TODO: handle empty strings
 */
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "strings.h"
#include "action.h"

acogc_mark_result
mala_string_acogc_mark (void * p)
{
  MalaString self = p;
  TRACE (mala_acogc, "%s", self->str);

  if (self->user_alloc_type == ACOGC_GC)
    acogc_object_lastmark (self->user);
  //acogc_object_mark (self->user);

  return ACOGC_KEEP;
}

void
mala_string_acogc_initize (void * p)
{
  TRACE (mala_acogc);
  MalaString self = p;
  self->user_alloc_type = ACOGC_UNSET;
  self->user = NULL;
}

void
mala_string_acogc_finalize (void * p)
{
  TRACE (mala_acogc);
  MalaString self = p;

  if (self->str_alloc_type == ACOGC_ALLOC)
    acogc_free (&self->str);

  if (self->user_alloc_type == ACOGC_ALLOC)
    acogc_free (&self->user);
}

//

MalaString
mala_string_new_cstr (const char* cstr, MalaStringBucket bucket)
{
  return mala_stringbucket_search (bucket, cstr);
}

MalaString
mala_string_new_literal_cstr (const char* cstr, MalaStringBucket bucket)
{
  return mala_stringbucket_reference (bucket, cstr);
}

MalaString
mala_string_new_attach_cstr (const char* cstr, MalaStringBucket bucket)
{
  return mala_stringbucket_attach (bucket, cstr);
}

MalaString
mala_string_new_2cstr (const char* cstr1, const char* cstr2, MalaStringBucket bucket)
{
  size_t len1;
  char * str = NULL;

  REQUIRE (cstr1, "cstr is NULL");
  REQUIRE (cstr2, "cstr is NULL");
  TRACE (mala_strings);

  acogc_alloc (&str,(len1 = strlen (cstr1)) + strlen (cstr2) + 1, bucket->gcroot);

  strcpy (str, cstr1);
  strcpy (str + len1, cstr2);

  return mala_string_new_attach_cstr (str, bucket);
}

MalaString
mala_string_new_cstr_n (const char* cstr, const size_t n, MalaStringBucket bucket)
{
  size_t len;
  char * str = NULL;

  REQUIRE (cstr, "cstr is NULL");
  TRACE (mala_strings);

  len = strlen (cstr);
  len = (len > n ? n : len);
  acogc_alloc (&str, len + 1, bucket->gcroot);
  strncpy (str, cstr, len);
  str[len] = '\0';

  return mala_string_new_attach_cstr (str, bucket);
}

MalaString
mala_string_new_n (MalaString self, const size_t n, MalaStringBucket bucket)
{
  return mala_string_new_cstr_n (mala_string_cstr (self), n, bucket);
}

MalaString
mala_string_new_prefix (const char* prefix, MalaString str, MalaStringBucket bucket)
{
  return mala_string_new_2cstr (prefix, mala_string_cstr (str), bucket);
}

MalaString
mala_string_new_print (MalaStringBucket bucket, const char* fmt,...)
{
  int n;
  char *p = NULL;
  va_list ap;
  TRACE (mala_strings);

  acogc_alloc (&p, 32, bucket->gcroot);

  va_start(ap, fmt);
  n = vsnprintf (p, 32, fmt, ap);
  va_end(ap);

  ASSERT (n >= 0, "Format error in %s", fmt);

  if (n != 31)
    acogc_alloc (&p, n+1, bucket->gcroot);

  if (n > 31)
    {
      va_start(ap, fmt);
      n = vsnprintf (p, n+1, fmt, ap);
      va_end(ap);
    }

  return mala_string_new_attach_cstr (p, bucket);
}


int
mala_string_scan (const_MalaString str, const char* fmt,...)
{
  int r;
  va_list ap;
  TRACE (mala_strings);

  va_start(ap, fmt);
  r = vsscanf(mala_string_cstr (str), fmt, ap);
  va_end (ap);
  return r;
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 2f6ba215-7cf7-46ba-a09e-e20b4c889234
// end_of_file
*/
