/*
    mala_globals.c - Truely global variables (config vars)

  Copyright (C) 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

/*
  Here are rarely needed fine-tuning configuration variables, most of them take
  only effect when they are changed before a mala engine is initialized.
  Some are const and only for compiletime configuration.
  I try to keep reasonable defaults here, usually there should be no reason to change them.
*/

#ifdef MALA_TYPES_H
#define MALA_GLOBAL(type, name, value) extern type mala_global_##name
#define MALA_GLOBAL_CONST(type, name, value) extern const type mala_global_##name
#else
#define MALA_GLOBAL(type, name, value) type mala_global_##name = value
#define MALA_GLOBAL_CONST(type, name, value) const type mala_global_##name = value
#endif

/*
 * Garbagge Collector configuration
 */
//described as:
// how many objects, allocation frequency, performance impact

// few objects, static, noncritical
MALA_GLOBAL (unsigned, bucketfactory_low, 50);
MALA_GLOBAL (unsigned, bucketfactory_high, 70);

// many objects, dynamic, noncritical
MALA_GLOBAL (unsigned, stringsfactory_low, 25);
MALA_GLOBAL (unsigned, stringsfactory_high, 50);

// many objects, dynamic, noncritical
MALA_GLOBAL (unsigned, splayfactory_low, 25);
MALA_GLOBAL (unsigned, splayfactory_high, 50);

// many objects, very dynamic, critical
MALA_GLOBAL (unsigned, stringlistfactory_low, 50);
MALA_GLOBAL (unsigned, stringlistfactory_high, 90);

// some objects, mostly static, noncritical
MALA_GLOBAL (unsigned, actionfactory_low, 10);
MALA_GLOBAL (unsigned, actionfactory_high, 25);

// few objects, dynamic, noncritical
MALA_GLOBAL (unsigned, programfactory_low, 25);
MALA_GLOBAL (unsigned, programfactory_high, 75);

/*
 * probabilistic splay tree configuration
 */
MALA_GLOBAL_CONST (unsigned, stringbucket_splay_zigzig, 3);
MALA_GLOBAL_CONST (unsigned, stringbucket_splay_zigzag, 0);
MALA_GLOBAL_CONST (unsigned, stringbucket_splay_zig, 6);

/*
 * type identifiers for the acogc typesystem
 */
MALA_GLOBAL_CONST (char*, type_stringbucket, "mala_stringbucket");
MALA_GLOBAL_CONST (char*, type_string, "mala_string");
MALA_GLOBAL_CONST (char*, type_stringbucketnode, "mala_stringbucketnode");
MALA_GLOBAL_CONST (char*, type_stringlist, "mala_stringlist");
MALA_GLOBAL_CONST (char*, type_action, "mala_action");
MALA_GLOBAL_CONST (char*, type_program, "mala_program");


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 504325c1-cb9d-44b2-a407-a45e928d8792
// end_of_file
*/
