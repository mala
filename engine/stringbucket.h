/*
    stringbucket.h - MaLa stringbuckets

  Copyright (C) 2004, 2005, 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STRINGBUCKET_H
#define MALA_STRINGBUCKET_H

#include "acogc.h"
#include "mala_types.h"
#include "strings.h"

typedef int (*mala_string_cmp_fn) (const void*, const void*);

#define MALA_STRING_FWD (mala_string_cmp_fn) mala_string_compare
#define MALA_STRING_REV (mala_string_cmp_fn) mala_string_compare_rev

/*
  strings are looked up in a weak-splay tree, that is a splay tree
  which contains weak-pointers to the strings,
  when a splay tree node to a expired string is encountered it will be instantly removed

  Note that the acogc allows caching and reinstantiating of swept strings
*/
struct mala_stringbucketnode_struct
{
  acogc_weakref weak_string;
  MalaStringBucketNode left;
  MalaStringBucketNode right;
  MalaStringBucketNode parent;  //makes splaying more efficent/easier
};


struct mala_stringbucket_struct
{
  MalaStringBucketNode tree;
  AcogcRoot gcroot;
  AcogcFactory gcfactory_strings;
  AcogcFactory gcfactory_splaynodes;
  mala_string_cmp_fn cmpfn;
};


/*
 *      StringBucket
 */

typedef
enum
  {
    MALA_STRINGBUCKET_FIND,             // try to find string
    MALA_STRINGBUCKET_SEARCH,           // search string, create it if not already existing
    MALA_STRINGBUCKET_ATTACH,           // attach string which must be allocated by the caller,
                                        // freed by stringbucket when already in the tree or at garbage collection
    MALA_STRINGBUCKET_REFERENCE         // reference string, will not be freed, reference will not be used if string already exists
  } mala_stringbucket_mode;


#ifdef EBUG_ACOGC
void
mala_stringbucketnode_dump (const char* name, MalaStringBucketNode_ref n, unsigned depth);
#else
#define mala_stringbucketnode_dump(s,n,d)
#endif

void
mala_stringbucketnode_simpleremove (MalaStringBucketNode_ref n, MalaStringBucket bucket);

acogc_mark_result
mala_stringbucketnode_acogc_mark (void * p);

void
mala_stringbucketnode_acogc_initize (void * p);

void
mala_stringbucketnode_acogc_finalize (void * p);

acogc_mark_result
mala_stringbucket_acogc_mark (void * bucket);

void
mala_stringbucket_acogc_initize (void * p);

MalaStringBucketNode
mala_stringbucket_node_find (MalaStringBucket bucket,
                             const char * what,
                             mala_stringbucket_mode mode);

void
mala_stringbucketnode_splay (MalaStringBucketNode n, MalaStringBucket bucket);

MalaStringBucket
mala_stringbucket_new (mala_string_cmp_fn cmpfn,
                       AcogcFactory bucketfactory,
                       AcogcFactory gcfactory_strings,
                       AcogcFactory gcfactory_splaynodes);

void
mala_stringbucket_init (MalaStringBucket bucket,
                        mala_string_cmp_fn cmpfn,
                        AcogcRoot gcroot,
                        AcogcFactory gcfactory_strings,
                        AcogcFactory gcfactory_splaynodes);

static inline MalaString
mala_stringbucket_find (MalaStringBucket bucket, const char * what)
{
  MalaStringBucketNode ret = mala_stringbucket_node_find (bucket, what, MALA_STRINGBUCKET_FIND);
  return (MalaString) ret ? acogc_weakref_reinstget (&ret->weak_string) : NULL;
}

static inline MalaString
mala_stringbucket_search (MalaStringBucket bucket, const char * what)
{
  return (MalaString) acogc_weakref_reinstget
    (&mala_stringbucket_node_find (bucket, what, MALA_STRINGBUCKET_SEARCH)->weak_string);
}

static inline MalaString
mala_stringbucket_attach (MalaStringBucket bucket, const char * what)
{
  return (MalaString) acogc_weakref_reinstget
    (&mala_stringbucket_node_find (bucket, what, MALA_STRINGBUCKET_ATTACH)->weak_string);
}

static inline MalaString
mala_stringbucket_reference (MalaStringBucket bucket, const char * what)
{
  return (MalaString) acogc_weakref_reinstget
    (&mala_stringbucket_node_find (bucket, what, MALA_STRINGBUCKET_REFERENCE)->weak_string);
}

#endif /* MALA_STRINGBUCKET_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 09e98f6d-4992-4014-a18f-a5e532bdeb27
// end_of_file
*/

