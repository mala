/*
  strings.h - MaLa string handling

  Copyright (C) 2004, 2005, 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STRINGS_H
#define MALA_STRINGS_H

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "mala_types.h"
#include "acogc.h"
#include "action.h"
#include "stringbucket.h"
#include "llist.h"


struct mala_string_struct
{
  const char * str;
  size_t len;
  union
  {
    MalaAction action;
    void * user;
    int integer;
    MalaString string;
  };
  // TODO acogc_alloc_type
  short str_alloc_type:4;
  short user_alloc_type:4;
};

/* gc support */
acogc_mark_result
mala_string_acogc_mark (void*);

void
mala_string_acogc_initize (void*);

void
mala_string_acogc_finalize (void*);

/* constructors */
MalaString
mala_string_new_cstr (const char* cstr, MalaStringBucket bucket);

MalaString
mala_string_new_literal_cstr (const char* cstr, MalaStringBucket bucket);

MalaString
mala_string_new_attach_cstr (const char* cstr, MalaStringBucket bucket);

MalaString
mala_string_new_cstr_n (const char* cstr, const size_t n, MalaStringBucket bucket);

MalaString
mala_string_new_2cstr (const char* cstr1, const char* cstr2, MalaStringBucket bucket);

MalaString
mala_string_new_print (MalaStringBucket bucket, const char* fmt,...);

MalaString
mala_string_new_n (MalaString src, const size_t n, MalaStringBucket bucket);

MalaString
mala_string_new_prefix (const char* prefix, MalaString str, MalaStringBucket bucket);

int
mala_string_scan (const_MalaString str,const char* fmt,...);


static inline const char*
mala_string_cstr (const_MalaString s);

static inline size_t
mala_string_length (const_MalaString s);

static inline int
mala_string_compare (const_MalaString a, const_MalaString b);

static inline int
mala_string_compare_nocase (const_MalaString a, const_MalaString b);

static inline int
mala_string_prefix_nocase (const_MalaString a, const char* prefix);


static inline MalaAction
mala_string_action (const_MalaString self)
{
  return self->action;
}


static inline const char*
mala_string_action_cstr (const_MalaString self)
{
  REQUIRE (self);

  if (self->action && acogc_object_typecheck (self->action, mala_global_type_string))
    return self->action->string->str;
  else
    return NULL;
}


static inline const char*
mala_string_cstr (const_MalaString s)
{
  return s ? s->str : NULL;
}


static inline size_t
mala_string_length (const_MalaString s)
{
  return s->len;
}


static inline int
mala_string_compare (const_MalaString a, const_MalaString b)
{
  return a == b ? 0 : strcmp (mala_string_cstr (a), mala_string_cstr (b));
}


static inline int
mala_string_compare_nocase (const_MalaString a, const_MalaString b)
{
  return a == b ? 0 : strcasecmp (mala_string_cstr (a), mala_string_cstr (b));
}

static inline int
mala_string_prefix_nocase (const_MalaString a, const char* prefix)
{
  return ! strncasecmp(mala_string_cstr (a), prefix, strlen(prefix));
}


#endif /* MALA_STRINGS */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: b43b7350-ecb6-4aa8-94d3-930da822e3ea
// end_of_file
*/

