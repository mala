/*
    program.c - MaLa Program container

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include "mala_types.h"
#include "program.h"
#include "engine.h"
#include "action.h"
#include "stringlist.h"

/* genreate a string array for the states */
#define MALA_STATE(s) #s
const char * mala_program_states [] = {MALA_STATES};
#undef MALA_STATE


void
mala_program_acogc_initize (void* o)
{
  TRACE (mala_acogc);
  MalaProgram self = (MalaProgram)o;
  self->program = NULL;
}

acogc_mark_result
mala_program_acogc_mark (void* o)
{
  TRACE (mala_acogc);
  MalaProgram self = (MalaProgram)o;
  acogc_object_lastmark (self->program);
  //acogc_object_mark (self->program);
  return ACOGC_KEEP;
}

MalaProgram
mala_program_new_cstr (const char* cmd, MalaEngine engine)
{
  TRACE (mala_program);
  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaProgram, self);
  ACOGC_STACK_PTR (MalaStringList, elem);

  self.ptr = acogc_factory_alloc (&engine->gcfactory_programs);

  self.ptr->program = mala_stringlist_new (engine);

  elem.ptr = mala_stringlist_new (engine);
  elem.ptr->string = mala_string_new_cstr (cmd, engine->words);
  llist_insert_tail (&self.ptr->program->node, &elem.ptr->node);

  self.ptr->pptr = self.ptr->program;

  self.ptr->nesting_limit = 5000;
  self.ptr->nesting_level = -1;
  self.ptr->engine = engine;
  self.ptr->negated = 0;
  self.ptr->state = MALA_START;

  ACOGC_STACK_LEAVE;
  return self.ptr;
}


void
mala_program_run (MalaProgram self, mala_state dest)
{
  TRACE (mala_program);

  mala_state state = MALA_START;

  if (++self->nesting_level >= self->nesting_limit)
    state = mala_program_commonexception (self, MALA_STRING_ERROR_NESTING_LIMIT_EXCEED, mala_program_pptr (self));

  if (self->state == MALA_REMOVE)
    dest = MALA_REMOVE;
  else if (dest == MALA_REMOVE)
    self->state = MALA_REMOVE;

  while (mala_program_pptr (self) != self->program &&
         state > dest && state < MALA_EXCEPTION)
    {
      state = mala_program_step (self);
      if (state == MALA_LITERAL)
        {
          if (dest != MALA_REMOVE)
            mala_stringlist_fwd (&self->pptr);
          else
            mala_program_pptr (self)->string = self->engine->common_string[MALA_STRING_PASS];
        }
    }

  TODO("set state to MALA_FINISH at the end of fully evaluated program");

  --self->nesting_level;
  if (dest != MALA_REMOVE)
    self->state = state;
}

mala_state
mala_program_eval_arg (MalaProgram self, int n, mala_state final, MalaStringList_ref node)
{
  if (self->state > MALA_EFAULT)
    return self->state;

  mala_state nstate;
  MalaStringList opptr = self->pptr;

  MalaStringList pptr = (MalaStringList) llist_get_nth_stop (&self->pptr->node, n,
                                                             llist_get_prev (&self->program->node));

  TRACE (mala_program, "eval arg %d '%s' to %s", n,
         pptr?mala_stringlist_cstr (pptr):"(missing)",
         mala_engine_states[final]);

  if (pptr)
    {
      mala_state ostate = self->state;
      self->pptr = pptr;

      if (node)
        *node = pptr;

      mala_program_run (self, final);

      if (node)
        *node = mala_stringlist_next (*node);

      nstate = self->state;
      self->state = ostate;
    }
  else
    {
      ERROR (mala_program, "Missing Argument");
      nstate = mala_program_commonexception (self, MALA_STRING_ERROR_MISSING_ARGUMENT, self->program);
      if (node)
        *node = NULL;
    }

  self->pptr = opptr;
  return nstate;
}

mala_state
mala_program_eval_arg_fmt (MalaProgram self, int n, const char* fmt, void* data, MalaStringList_ref node)
{
  MalaStringList nde;
  mala_state state;

  state = mala_program_eval_arg (self, n, MALA_LITERAL, &nde);
  if (node)
    *node = nde;

  int r;

  r = mala_string_scan (mala_stringlist_string (nde), fmt, data);
  if (r != 1)
    return mala_program_commonexception (self, MALA_STRING_ERROR_WRONG_TYPE, nde);

  return state;
}

mala_state
mala_program_step (MalaProgram self)
{
  TODO(" handle --LITERAL");
  mala_state state;

  MalaStringList pptr = mala_program_pptr (self);

  TRACE (mala_program, "%s", mala_stringlist_cstr (pptr));

  if (pptr->string && pptr->string->action)
    {
      MalaAction action = mala_program_pptr_action (self);
      state = action->parser (self);
    }
  else if (self->state != MALA_EXCEPTION)
    state = MALA_LITERAL;

  return state;
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: fd8544bd-4144-44fe-8a9e-635aa0d3c393
// end_of_file
*/
