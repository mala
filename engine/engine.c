/*
    engine.c - MaLa core engine

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include "mala_types.h"
#include "engine.h"
#include "action.h"
#include "program.h"
#include "stringlist.h"
#include "std/std.h"


// TODO alle acogc_marker optimiern so das weniger doppelt gemarked wird in den FOREACH schleifen (result von acogc_object_mark nutzen)


#define MALA_COMMON_STRING(e,S) S
static const char * mala_common_strings [] = MALA_COMMON_STRINGS;
#undef MALA_COMMON_STRING

#define MALA_STATE(s) #s
const char * mala_engine_states [] = {MALA_STATES};
#undef MALA_STATE

/* nobug support */

NOBUG_DEFINE_FLAG(mala);
NOBUG_DEFINE_FLAG(mala_acogc);
NOBUG_DEFINE_FLAG(mala_engine);
NOBUG_DEFINE_FLAG(mala_program);
NOBUG_DEFINE_FLAG(mala_strings);
NOBUG_DEFINE_FLAG(mala_stringlist);
NOBUG_DEFINE_FLAG(mala_stringbucket);
NOBUG_DEFINE_FLAG(mala_action);
NOBUG_DEFINE_FLAG(mala_module);
NOBUG_DEFINE_FLAG(mala_module_std);


/*
 * function definitions
 */

MalaEngine
mala_engine_new ()
{
  MalaEngine self = NULL;

  acogc_alloc (&self, sizeof(mala_engine), NULL);

  mala_engine_init (self);

  return self;
}

void
mala_engine_init (MalaEngine self)
{
  int i;

  acogc_nobug_init();
  TRACE(NOBUG_ON);

  NOBUG_INIT_FLAG(mala);
  NOBUG_INIT_FLAG(mala_acogc);
  NOBUG_INIT_FLAG(mala_engine);
  NOBUG_INIT_FLAG(mala_program);
  NOBUG_INIT_FLAG(mala_strings);
  NOBUG_INIT_FLAG(mala_stringlist);
  NOBUG_INIT_FLAG(mala_stringbucket);
  NOBUG_INIT_FLAG(mala_action);
  NOBUG_INIT_FLAG(mala_module);
  NOBUG_INIT_FLAG(mala_module_std);

  TRACE(mala_engine);

  self->engine_trace = MALA_NOTRACE;
  self->blockcnt = 0;

  self->state = MALA_SUCCESS;

  acogc_root_init (&self->gcroot);

  acogc_factory_init (&self->gcfactory_buckets,
                      &self->gcroot,
                      sizeof(mala_stringbucket),
                      mala_global_bucketfactory_low,
                      mala_global_bucketfactory_high,
                      mala_stringbucket_acogc_mark,
                      mala_stringbucket_acogc_initize,
                      NULL,
                      mala_global_type_stringbucket);
  

  acogc_factory_init (&self->gcfactory_strings,
                      &self->gcroot,
                      sizeof (mala_string),
                      mala_global_stringsfactory_low,
                      mala_global_stringsfactory_high,
                      mala_string_acogc_mark,
                      mala_string_acogc_initize,
                      mala_string_acogc_finalize,
                      mala_global_type_string);

  acogc_factory_init (&self->gcfactory_stringbucketnodes,
                      &self->gcroot,
                      sizeof (mala_stringbucketnode),
                      mala_global_splayfactory_low,
                      mala_global_splayfactory_high,
                      mala_stringbucketnode_acogc_mark,
                      mala_stringbucketnode_acogc_initize,
                      mala_stringbucketnode_acogc_finalize,
                      mala_global_type_stringbucketnode);

  acogc_factory_init (&self->gcfactory_stringlists,
                      &self->gcroot,
                      sizeof (mala_stringlist),
                      mala_global_stringlistfactory_low,
                      mala_global_stringlistfactory_high,
                      mala_stringlist_acogc_mark,
                      mala_stringlist_acogc_initize,
                      mala_stringlist_acogc_finalize,
                      mala_global_type_stringlist);

  acogc_factory_init (&self->gcfactory_actions,
                      &self->gcroot,
                      sizeof (mala_action),
                      mala_global_actionfactory_low,
                      mala_global_actionfactory_high,
                      mala_action_acogc_mark,
                      mala_action_acogc_initize,
                      mala_action_acogc_finalize,
                      mala_global_type_action);

  acogc_factory_init (&self->gcfactory_programs,
                      &self->gcroot,
                      sizeof (mala_program),
                      mala_global_programfactory_low,
                      mala_global_programfactory_high,
                      mala_program_acogc_mark,
                      mala_program_acogc_initize,
                      NULL,
                      mala_global_type_program);

  self->words = mala_stringbucket_new (MALA_STRING_FWD,
                                       &self->gcfactory_buckets,
                                       &self->gcfactory_strings,
                                       &self->gcfactory_stringbucketnodes);
  acogc_addroot (self->words);

  if (getenv ("MALA_TRACE"))
    self->engine_trace = MALA_TRACE;

  for (i = 0; i < MALA_STRING_MAX; ++i)
    {
      self->common_string[i] = mala_string_new_literal_cstr (mala_common_strings[i], self->words);
      acogc_addroot (self->common_string[i]);
    }

  // add --WORLD action
  MalaAction world = mala_action_new (self->common_string[MALA_STRING_WORLD], NULL, NULL, ACOGC_UNSET, self);
  acogc_addroot (world);
  mala_action_attach_child (world, world);
}

void
mala_engine_free (MalaEngine self)
{
  TRACE (mala_engine);
  if (!self)
    return;

  acogc_root_erase (&self->gcroot);

  free (self);
}


mala_state
mala_engine_state_get (MalaEngine self)
{
  return self ? self->state : MALA_EINVALID;
}

const char *
mala_engine_state_str (MalaEngine self)
{
  return mala_engine_states [self ? self->state : MALA_EINVALID];
}

void
mala_engine_clearstate (MalaEngine self)
{
  if (self)
    self->state = MALA_SUCCESS;
}


mala_state
mala_engine_actions_register (MalaEngine self, mala_actioninit * actioninit)
{
  TRACE (mala_engine);
  if (mala_engine_state_get (self) > MALA_EFAULT)
    return mala_engine_state_get (self);

  while (actioninit->name)
    {
      TRACE (mala_engine, "register %s", actioninit->name);
      mala_action_new_actioninit (actioninit, self);
      ++actioninit;
    }

  return MALA_SUCCESS;
}


void
mala_engine_action_new_cstr (MalaEngine self,
                             const char* cname,
                             MalaParserFunc parser,
                             void *data,
                             acogc_alloc_type data_alloc,
                             MalaAction parent)
{
  TRACE (mala_engine);
  ACOGC_STACK_ENTER (&self->gcroot);
  ACOGC_STACK_PTR (MalaString, name);
  ACOGC_STACK_PTR (MalaAction, action);

  name.ptr = mala_string_new_cstr (cname, self->words);
  action.ptr = mala_action_new (name.ptr, parser, data, data_alloc, self);

  if (parent)
    mala_action_attach_child (parent, action.ptr);

  ACOGC_STACK_LEAVE;
}

MalaString
mala_engine_tmpname (MalaEngine self, const char* templ)
{
  MalaString name;
  do
    name = mala_string_new_print (self->words, templ, self->blockcnt);
  while (name->action && (++self->blockcnt,1));
  return name;
}

void
mala_engine_run_cstr (MalaEngine self,
                      const char* cmd,
                      MalaStringList_ref result)
{
  TRACE (mala_engine);

  if (self->state > MALA_EFAULT)
    return;

  ACOGC_STACK_ENTER (&self->gcroot);
  ACOGC_STACK_PTR (MalaProgram, prg);

  prg.ptr = mala_program_new_cstr (cmd, self);

  mala_program_run (prg.ptr, MALA_SUCCESS);

  self->state = prg.ptr->state;

  if (result)
    *result = prg.ptr->program;

  ACOGC_STACK_LEAVE;
}

#if 0
MalaEngine
mala_engine_new_main (int (*initfunc) (MalaEngine),
                      int argc, char **argv,
                      mala_actioninit * actions_init)
{
  /*TODO convinience function*/
}
#endif

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: ece5489f-f703-4033-8cbc-efc1b0562100
// end_of_file
*/
