/*
    program.h - MaLa program descriptor

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_PROGRAM_H
#define MALA_PROGRAM_H

#include <stdio.h>
#include "mala_types.h"
#include "stringlist.h"

struct mala_program_struct
{
  MalaStringList program;       // list containing the actual program
  MalaStringList pptr;          // points to the position *before* the next to be evaluated string
  MalaStringList exception;     // when a exception is pending then this points to the exception
  MalaEngine engine;
  int nesting_limit;
  int nesting_level;
  int negated:1;
  mala_state state;
};


extern const char * mala_program_states [];

void
mala_program_acogc_initize (void* o);

acogc_mark_result
mala_program_acogc_mark (void* o);

MalaProgram
mala_program_new_cstr (const char * cmd, MalaEngine engine);

void
mala_program_run (MalaProgram self, mala_state dest);

mala_state
mala_program_eval_arg (MalaProgram self, int n, mala_state final, MalaStringList_ref node);

mala_state
mala_program_eval_arg_fmt (MalaProgram self, int n, const char* fmt, void* data, MalaStringList_ref node);

static inline int
mala_program_exists_arg (MalaProgram self, int n)
{
  return !!llist_get_nth_stop (&self->pptr->node, n, llist_get_prev (&self->program->node));
}

static inline mala_state
mala_program_commonexception (MalaProgram self, mala_common_string idx, MalaStringList node)
{
  mala_stringlist_insert_before (node,
                                 self->exception = mala_stringlist_node_new (self->engine->common_string[idx],
                                                                             self->engine));
  return self->state = MALA_EXCEPTION;
}

static inline mala_state
mala_program_state_get (MalaProgram self);

mala_state
mala_program_step (MalaProgram self);

static inline void
mala_program_action_done (MalaProgram prg, int args);


static inline MalaAction
mala_program_pptr_action (MalaProgram self);

static inline MalaString
mala_program_pptr_string (MalaProgram self);

static inline const char*
mala_program_pptr_cstr (MalaProgram self);

static inline MalaStringList
mala_program_pptr (MalaProgram self);



/*
 *      inlines
 */

static inline mala_state
mala_program_state_get (MalaProgram self)
{
  return self ? self->state : MALA_EINVALID;
}

void
mala_program_action_done (MalaProgram prg, int args)
{
  while (args--)
    {
      TODO("assert not delete over end of program");
      mala_stringlist_remove (mala_stringlist_next (prg->pptr));
    }
  if (prg->nesting_level)
    mala_stringlist_next (prg->pptr)->string = prg->engine->common_string[MALA_STRING_PASS];
  else
    mala_stringlist_remove (mala_stringlist_next (prg->pptr));
}

static inline MalaAction
mala_program_pptr_action (MalaProgram self)
{
  return mala_stringlist_next (self->pptr)->string->action;
}

static inline MalaString
mala_program_pptr_string (MalaProgram self)
{
  return mala_stringlist_next (self->pptr)->string;
}

static inline const char*
mala_program_pptr_cstr (MalaProgram self)
{
  return mala_string_cstr (mala_stringlist_next (self->pptr)->string);
}

static inline MalaStringList
mala_program_pptr (MalaProgram self)
{
  return mala_stringlist_next (self->pptr);
}

#endif /*MALA_PROGRAM_H*/
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 0c5dbf2d-8ae2-4e00-874d-d47c8251f709
// end_of_file
*/
