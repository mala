/*
    action.h - MaLa action handler

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_ACTION_H
#define MALA_ACTION_H

#include "llist.h"
#include "mala_types.h"
#include "action.h"

struct
mala_actioninit_struct
{
  const char * name;
  MalaParserFunc parser;
  mala_action_datatype type;
  void *data;
  const char * parent;
};

#define MALA_ACTIONS_END {NULL,NULL,MALA_DATA_UNUSED,NULL,NULL}

struct mala_action_struct
{
  MalaString name;
  MalaParserFunc parser;
  MalaAction parent;
  llist childs;
  llist cldnode;
  llist stknode;
  union
  {
    MalaStringList list;
    MalaString string;
    void * data;
  };
  acogc_alloc_type data_alloc_type;
};

/*
typedef enum {
  MALA_NIL_TYPE,
  MALA_DATAPTR_TYPE,
  MALA_STRINGLIST_TYPE,
  MALA_STRING_TYPE
} mala_data_type;
*/

static inline void
mala_action_attach_child (MalaAction parent, MalaAction child)
{
  if (parent)
    llist_insert_tail (&parent->childs, &child->cldnode);
}

MalaAction
mala_action_new_actioninit (MalaActionInit init, MalaEngine engine);

MalaAction
mala_action_new (MalaString name,
                 MalaParserFunc parser,
                 void * data,
                 acogc_alloc_type data_alloc,
                 MalaEngine engine);

void
mala_action_delete (MalaAction self);

void
mala_action_acogc_initize (void*);

void
mala_action_acogc_finalize (void*);

acogc_mark_result
mala_action_acogc_mark (void*);

static inline AcogcFactory
mala_action_data_type (MalaAction self)
{
  if (self->data_alloc_type >= ACOGC_STATIC)
    {
      return acogc_object_type (self->data);
    }
  else
    return NULL;
}

static inline MalaStringList
mala_action_stringlist (MalaAction self)
{
  return self->list;
}

static inline MalaString
mala_action_string (MalaAction self)
{
  return self->string;
}

static inline void *
mala_action_dataptr (MalaAction self)
{
  return self->data;
}



#endif /*MALA_ACTION_H*/
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: d8b425b7-18c1-4dc8-a19b-cdd095873e70
// end_of_file
*/
