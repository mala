/*
  stringbucket.c - MaLa stringbucket handling

  Copyright (C) 2004, 2005, 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/


#include <stdlib.h>
#include <llist.h>
#include <acogc.h>
#include <nobug.h>
#include "stringbucket.h"
#include "strings.h"
#include "action.h"

/* fast simple low quality prng, low bits are random, 2^31-1 cycle */
static inline uint32_t mala_fast_prng ()
{
  static uint32_t rnd=0xbabeface;
  return rnd = rnd<<1 ^ ((rnd >> 30) & 1) ^ ((rnd>>2) & 1);
}

acogc_mark_result
mala_stringbucket_acogc_mark (void * p)
{
  TRACE (mala_acogc);

  MalaStringBucket bucket = p;
  acogc_object_lastmark (bucket->tree);
  //acogc_object_mark (bucket->tree);
  return ACOGC_KEEP;
}

void
mala_stringbucket_acogc_initize (void * p)
{
  TRACE (mala_acogc);

  MalaStringBucket self = p;
  self->tree = NULL;
}

acogc_mark_result
mala_stringbucketnode_acogc_mark (void * p)
{
  TRACE (mala_acogc);

  MalaStringBucketNode node = p;
  acogc_object_mark (node->left);
  acogc_object_lastmark (node->right);
  //acogc_object_mark (node->right);

  // TODO simpleremove if weakref is empty

  return ACOGC_KEEP;
}

void
mala_stringbucketnode_acogc_initize (void * p)
{
  TRACE (mala_acogc);

  MalaStringBucketNode node = p;
  node->left = NULL;
  node->right = NULL;
  acogc_weakref_init (&node->weak_string);
}

void
mala_stringbucketnode_acogc_finalize (void * p)
{
  TRACE (mala_acogc);

  MalaStringBucketNode node = p;
  acogc_weakref_unlink (&node->weak_string);
}

#ifdef EBUG_ACOGC //TODO
static void
mala_stringbucketnode_dump_ (FILE* g, MalaStringBucketNode_ref n, unsigned depth)
{
  if (*n && depth)
    {
      if ((*n)->left)
        {
          fprintf(g, "\t\"%p:%s\":sw -> \"%p:%s\":n;\n",
                  *n,
                  mala_string_cstr ((MalaString)acogc_weakref_get (&(*n)->weak_string)),
                  (*n)->left,
                  mala_string_cstr ((MalaString)acogc_weakref_get (&(*n)->left->weak_string)));
          mala_stringbucketnode_dump_ (g, &(*n)->left, depth-1);
        }

      if ((*n)->right)
        {
          fprintf(g, "\t\"%p:%s\":se -> \"%p:%s\":n;\n",
                  *n,
                  mala_string_cstr ((MalaString)acogc_weakref_get (&(*n)->weak_string)),
                  (*n)->right,
                  mala_string_cstr ((MalaString)acogc_weakref_get (&(*n)->right->weak_string)));
          mala_stringbucketnode_dump_ (g, &(*n)->right, depth-1);
        }
    }
}

void
mala_stringbucketnode_dump (const char* name, MalaStringBucketNode_ref n, unsigned depth)
{
  static int cnt=0;
  char cmd[256];
  sprintf(cmd,"dot -Tps >/var/tmp/dbg_%d.ps; gv /var/tmp/dbg_%d.ps", cnt, cnt);
  FILE * graph = popen(cmd, "w");

  fprintf(graph, "digraph \"%s\" { center=true; size=\"6,6\"; node [color=lightblue2, style=filled];", name);
  ++cnt;

  fprintf(graph, "\t\"root\":s -> \"%p:%s\":n;\n",
          *n,
          mala_string_cstr ((MalaString)acogc_weakref_get (&(*n)->weak_string)));

  mala_stringbucketnode_dump_ (graph, n, depth-1);

  fprintf(graph, "}");

  pclose(graph);
}
#endif


inline static MalaStringBucketNode
mala_stringbucketnode_new (const char * cstr,
                           mala_stringbucket_mode mode,
                           int c,
                           MalaStringBucketNode p,
                           MalaStringBucket bucket)
{
  TRACE (mala_stringbucket);

  ACOGC_STACK_ENTER (bucket->gcroot);
  ACOGC_STACK_PTR (MalaStringBucketNode, n);
  ACOGC_STACK_PTR (MalaString, s);

  n.ptr = acogc_factory_alloc (bucket->gcfactory_splaynodes);

  n.ptr->parent = p;

  s.ptr = acogc_factory_alloc (bucket->gcfactory_strings);

  s.ptr->len = strlen (cstr);
  s.ptr->str_alloc_type = ACOGC_ALLOC;

  switch (mode)
    {
    case MALA_STRINGBUCKET_SEARCH:
      s.ptr->str = NULL;
      acogc_alloc (&s.ptr->str, s.ptr->len + 1, bucket->gcroot);
      strcpy ((char*)s.ptr->str, cstr);
      break;
    case MALA_STRINGBUCKET_REFERENCE:
      s.ptr->str_alloc_type = ACOGC_STATIC;
      //nobreak
    case MALA_STRINGBUCKET_ATTACH:
      s.ptr->str = cstr;
      break;
    case MALA_STRINGBUCKET_FIND:        // not used, just avoid a gcc warning
      break;
    }

  acogc_weakref_link (&n.ptr->weak_string, s.ptr);

  if (!p)
    bucket->tree = n.ptr;
  else
    {
      ASSERT (c, "c is 0");
      if (c < 0)
        p->left = n.ptr;
      else if (c > 0)
        p->right = n.ptr;
    }

  INFO_DBG (mala_stringbucket, "create %p", n.ptr);

  ASSERT (n.ptr->parent? (n.ptr->parent->left == n.ptr || n.ptr->parent->right == n.ptr): bucket->tree==n.ptr, "PARENT ERROR");
  ACOGC_STACK_LEAVE;
  return n.ptr;
}

void
mala_stringbucketnode_simpleremove (MalaStringBucketNode_ref n, MalaStringBucket bucket)
{
  TRACE (mala_stringbucket);

  //mala_stringbucketnode_dump ("remove before", &bucket->tree, 40);
  INFO_DBG (mala_stringbucket, "removing %p",*n);
  ASSERT ((*n)->parent? ((*n)->parent->left == *n || (*n)->parent->right == *n): bucket->tree==*n, "PARENT ERROR");

  MalaStringBucketNode p = (*n)->parent;
  int c;

  if (p && p->left == *n)
    c = -1;
  else
    c = 1;

  if (!(*n)->left)
    // only right leaf
    *n = (*n)->right;
  else if (!(*n)->right)
    // only left leaf
    *n = (*n)->left;
  else
    {
      MalaStringBucketNode i;
      if (mala_fast_prng()&1) /* 50% probability removing left or right wards */
        {
          //left right
          TRACE_DBG (mala_stringbucket, "left right");

          //for (i = (*n)->left; i->right; assert(i->right->parent == i),i = i->right);
          TODO ("reinst asserts");
          for (i = (*n)->left; i->right; i = i->right);

          i->right = (*n)->right;
          if (i->parent != *n)
            {
              i->parent->right = i->left;
              if (i->left)
                i->left->parent = i->parent;
              i->left = (*n)->left;
            }
        }
      else
        {
          // right left
          TRACE_DBG (mala_stringbucket, "right left");

          TODO ("reinst asserts");
          //for (i = (*n)->right; i->left; assert(i->left->parent == i),i = i->left);
          for (i = (*n)->right; i->left; i = i->left);

          i->left = (*n)->left;
          if (i->parent != *n)
            {
              i->parent->left = i->right;
              if (i->right)
                i->right->parent = i->parent;
              i->right = (*n)->right;
            }
        }
      *n = i;
    }

  if (!p)
    bucket->tree = *n;
  else
    {
      if (c < 0)
        p->left = *n;
      else
        p->right = *n;
    }

  if (*n)
    {
      (*n)->parent = p;
      if((*n)->left)
        (*n)->left->parent = *n;
      if((*n)->right)
        (*n)->right->parent = *n;
    }
}


MalaStringBucketNode
mala_stringbucket_node_find (MalaStringBucket bucket,
                             const char * cstr,
                             mala_stringbucket_mode mode)
{
  TRACE (mala_stringbucket);
  int c = 1;
  MalaStringBucketNode p = NULL;
  MalaStringBucketNode n = bucket->tree;

  while (1)
    {
      if (!n)
        {
          if (mode == MALA_STRINGBUCKET_FIND)
            return NULL;

          n = mala_stringbucketnode_new (cstr, mode, c, p, bucket);
          mala_stringbucketnode_splay (n, bucket);
          ASSERT (n->parent? (n->parent->left == n || n->parent->right == n): bucket->tree==n, "PARENT ERROR");
          return n;
        }

      MalaString s = acogc_weakref_get (&n->weak_string);
      if (!s)
        {
          // really freed nodes are removed from the tree
          mala_stringbucketnode_simpleremove (&n, bucket);
          continue;
        }

      c = strcmp (cstr, mala_string_cstr (s));

      p = n;
      if (c < 0)
        {
          n = n->left;
        }
      else if (c > 0)
        {
          n = n->right;
        }
      else
        {
          if (mode == MALA_STRINGBUCKET_ATTACH)
            // string already exists, so we can free the supplied string
            acogc_free (&cstr);

          mala_stringbucketnode_splay (n, bucket);
          ASSERT (n->parent? (n->parent->left == n || n->parent->right == n): bucket->tree==n, "PARENT ERROR");
          return n;
        }
    }
}

void
mala_stringbucketnode_splay (MalaStringBucketNode n, MalaStringBucket bucket)
{
  TRACE (mala_stringbucket);

  while (n->parent)
    {
      MalaStringBucketNode p = n->parent;
      MalaStringBucketNode g = p?p->parent:NULL;

      TRACE_DBG (mala_stringbucket, "splaying n %p, p %p, g %p",n,p,g);

      if (g)
        {
          if (p == g->left)
            {
              // zig..
              if (n == p->left)
                {
                  TRACE_DBG (mala_stringbucket, "zig zig %p",n);
                  if ((mala_fast_prng()&0xF) < mala_global_stringbucket_splay_zigzig)
                    return;

                  g->left = p->right;
                  if (g->left) g->left->parent = g;
                  p->right = g;

                  p->left = n->right;
                  if (p->left) p->left->parent = p;
                  n->right = p;

                  n->parent = g->parent;
                  g->parent = p;
                }
              else
                {
                  TRACE_DBG (mala_stringbucket, "zig zag %p",n);
                  if ((mala_fast_prng()&0xF) < mala_global_stringbucket_splay_zigzag)
                    return;

                  p->right = n->left;
                  if (p->right) p->right->parent = p;
                  n->left = p;

                  g->left = n->right;
                  if (g->left) g->left->parent = g;
                  n->right = g;

                  n->parent = g->parent;
                  g->parent = n;
                }
            }
          else
            {
              // zag..
              if (n == p->left)
                {
                  TRACE_DBG (mala_stringbucket, "zag zig %p", n);
                  if ((mala_fast_prng()&0xF) < mala_global_stringbucket_splay_zigzag)
                    return;

                  p->left = n->right;
                  if (p->left) p->left->parent = p;
                  n->right = p;

                  g->right = n->left;
                  if (g->right) g->right->parent = g;
                  n->left = g;

                  n->parent = g->parent;
                  g->parent = n;
                }
              else
                {
                  TRACE_DBG (mala_stringbucket, "zag zag %p", n);
                  if ((mala_fast_prng()&0xF) < mala_global_stringbucket_splay_zigzig)
                    return;

                  g->right = p->left;
                  if (g->right) g->right->parent = g;
                  p->left = g;

                  p->right = n->left;
                  if (p->right) p->right->parent = p;
                  n->left = p;

                  n->parent = g->parent;
                  g->parent = p;
                }
            }

          p->parent = n;
          if (n->parent)
            {
              if (n->parent->left == g)
                n->parent->left = n;
              else
                n->parent->right = n;
            }
          else
            {
              bucket->tree = n;
              return;
            }
        }
      else
        {
          if (p)
            {
              if ((mala_fast_prng()&0xF) < mala_global_stringbucket_splay_zig)
                return;

              if (n == p->left)
                {
                  // ..zig
                  TRACE_DBG (mala_stringbucket, "zig %p", n);
                  p->left = n->right;
                  if (p->left) p->left->parent = p;
                  n->right = p;
                }
              else
                {
                  // ..zag
                  TRACE_DBG (mala_stringbucket, "zag %p", n);
                  p->right = n->left;
                  if (p->right) p->right->parent = p;
                  n->left = p;
                }
              p->parent = n;
              n->parent = NULL;
              bucket->tree = n;
            }
          return;       // simple zig and zag are end-cases
        }
    }
}


MalaStringBucket
mala_stringbucket_new (mala_string_cmp_fn cmpfn,
                       AcogcFactory bucketfactory,
                       AcogcFactory gcfactory_strings,
                       AcogcFactory gcfactory_splaynodes)
{
  TRACE (mala_stringbucket);

  ACOGC_STACK_ENTER (bucketfactory->root);
  ACOGC_STACK_PTR (MalaStringBucket, self);

  self.ptr = acogc_factory_alloc (bucketfactory);
  mala_stringbucket_init (self.ptr, cmpfn, bucketfactory->root, gcfactory_strings, gcfactory_splaynodes);

  ACOGC_STACK_LEAVE;
  return self.ptr;
}


void
mala_stringbucket_init (MalaStringBucket bucket,
                        mala_string_cmp_fn cmpfn,
                        AcogcRoot gcroot,
                        AcogcFactory gcfactory_strings,
                        AcogcFactory gcfactory_splaynodes)
{
  TRACE (mala_stringbucket);

  bucket->tree = NULL;
  bucket->cmpfn = cmpfn;
  bucket->gcroot = gcroot;
  bucket->gcfactory_strings = gcfactory_strings,
  bucket->gcfactory_splaynodes = gcfactory_splaynodes;
}


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 84fbd8ab-4e90-4d04-ab70-d9b955dde41c
// end_of_file
*/
