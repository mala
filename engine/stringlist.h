/*
    stringlist.h - MaLa stringlists

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STRINGLIST_H
#define MALA_STRINGLIST_H

#include <stdio.h>
#include "mala_types.h"
#include "engine.h"
#include "strings.h"
#include "llist.h"


#define MALA_STATIC_STRINGLIST_INIT(args...) \
  (const char*[]){args, NULL}


/*
 *      StringList
 */

struct mala_stringlist_struct
{
  llist node;
  MalaString string;
};

/*gc support*/
acogc_mark_result
mala_stringlist_acogc_mark (void* o);

void
mala_stringlist_acogc_initize (void * o);

void
mala_stringlist_acogc_finalize (void * o);


/* constructors */
MalaStringList
mala_stringlist_new (MalaEngine engine);

MalaStringList
mala_stringlist_copy (MalaStringList source, MalaEngine engine);

void
mala_stringlist_append_cstrs (MalaStringList self, int nelem, char** cstrs, MalaEngine engine);

void
mala_stringlist_append_literal_cstrs (MalaStringList self, int nelem, char** cstrs, MalaEngine engine);

MalaStringList
mala_stringlist_node_new_literal_cstr (const char * cstr, MalaEngine engine);

MalaStringList
mala_stringlist_node_new_cstr (const char * cstr, MalaEngine engine);

MalaStringList
mala_stringlist_node_new (MalaString str, MalaEngine engine);

/* declarations */
static inline void
mala_stringlist_insert_after_stringlist (MalaStringList self, MalaStringList src);

static inline MalaString
mala_stringlist_string_from_llist (const_LList self);

static inline const char *
mala_stringlist_cstr_from_llist (const_LList self);

void
mala_stringlist_dump (MalaStringList self, FILE * f,
                      const char* head,
                      const char* before,
                      const char* fmt,
                      const char* after,
                      const char* foot);

void
nobug_mala_stringlist_dump (const_MalaStringList self,
                            const int depth,
                            const char* file,
                            const int line,
                            const char* func);

static inline MalaString
mala_stringlist_string (MalaStringList self);

static inline void
mala_stringlist_string_set (MalaStringList self, MalaString str);

static inline MalaStringList
mala_stringlist_next (MalaStringList self);

static inline MalaStringList
mala_stringlist_prev (MalaStringList self);

static inline void
mala_stringlist_fwd (MalaStringList_ref self);

static inline void
mala_stringlist_rev (MalaStringList_ref self);

static inline void
mala_stringlist_advance (MalaStringList self);

static inline void
mala_stringlist_remove (MalaStringList self);

static inline void
mala_stringlist_insert_tail (MalaStringList self, MalaStringList node);

static inline void
mala_stringlist_insert_head (MalaStringList self, MalaStringList node);

#define mala_stringlist_insert_before mala_stringlist_insert_tail
#define mala_stringlist_insert_after mala_stringlist_insert_head

/* inlines */
static inline void
mala_stringlist_insert_after_stringlist (MalaStringList self, MalaStringList src)
{
  llist_insert_list_after (&src->node, &self->node);
}

static inline MalaString
mala_stringlist_string (MalaStringList self)
{
  return self->string;
}

static inline void
mala_stringlist_string_set (MalaStringList self, MalaString str)
{
  self->string = str;
}

static inline const char*
mala_stringlist_cstr (MalaStringList self)
{
  return mala_string_cstr (self->string);
}

static inline MalaStringList
mala_stringlist_head_get (MalaStringList self)
{
  return LLIST_TO_STRUCTP (llist_get_next (&self->node), mala_stringlist, node);
}

static inline MalaString
mala_stringlist_string_from_llist (const_LList self)
{
  return LLIST_TO_STRUCTP (self, mala_stringlist, node)->string;
}

static inline const char *
mala_stringlist_cstr_from_llist (const_LList self)
{
  return mala_string_cstr (mala_stringlist_string_from_llist (self));
}

static inline void
mala_stringlist_fwd (MalaStringList_ref self)
{
  *self = LLIST_TO_STRUCTP (llist_get_next (&(*self)->node), mala_stringlist, node);
}

static inline void
mala_stringlist_rev (MalaStringList_ref self)
{
  *self = LLIST_TO_STRUCTP (llist_get_prev (&(*self)->node), mala_stringlist, node);
}

static inline void
mala_stringlist_advance (MalaStringList self)
{
  llist_advance (&self->node);
}

static inline void
mala_stringlist_remove (MalaStringList self)
{
  llist_unlink (&self->node);
}

static inline MalaStringList
mala_stringlist_next (MalaStringList self)
{
  return LLIST_TO_STRUCTP (llist_get_next(&self->node), mala_stringlist, node);
}

static inline MalaStringList
mala_stringlist_prev (MalaStringList self)
{
  return LLIST_TO_STRUCTP (llist_get_prev(&self->node), mala_stringlist, node);
}

static inline void
mala_stringlist_insert_tail (MalaStringList self, MalaStringList node)
{
  llist_insert_tail (&self->node, &node->node);
}

static inline void
mala_stringlist_insert_head (MalaStringList self, MalaStringList node)
{
  llist_insert_head (&self->node, &node->node);
}

static inline MalaStringList
mala_stringlist_tail_get (MalaStringList self)
{
  return LLIST_TO_STRUCTP(llist_get_tail (&self->node), mala_stringlist, node);
}

static inline int
mala_stringlist_is_end (MalaStringList self, MalaStringList other)
{
  return self == other;
}

#endif /* MALA_STRINGLIST_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 44b2b6d9-80b8-43fa-938a-a492bd52cea2
// end_of_file
*/
