/*
    test-mala_arithmodule.c - MaLa arithmetic module test suite

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#include <hackerlab/bugs/panic.h>
#include <hackerlab/vu/safe-printfmt.h>
#include <hackerlab/os/stdlib.h>
#include <mala-hackerlab/mala-hackerlab.h>
#include <mala-hackerlab/std/std.h>
#include <mala-hackerlab/arith/arith.h>

int init_mala(MalaEngine eng)
{
  int err;
  if ( err = mala_stdmodule_init(eng))
    return err;
  if ( err = mala_arithmodule_init(eng))
    return err;

  return 0;
}

int
main(int argc, char * argv[])
{
  MalaEngine my_engine;
  my_engine = mala_engine_new_main (init_mala, argc, argv, 0);

  invariant (mala_engine_getstate (my_engine) == MALA_SUCCESS);

  mala_engine_run (my_engine);

  mala_engine_dumpargs (my_engine, stdout, "arg: ", "\n");
  mala_engine_dumpprogram (my_engine, stdout, "prg: ", "\n");

  safe_printfmt (1, "variable: %s\n", getenv ("variable"));
  safe_printfmt (1, "negated: %d\n", mala_engine_is_negated (my_engine));
  safe_printfmt (1, "state: %d\n", mala_engine_getstate (my_engine));
  mala_engine_destroy (my_engine);
  return 0;
}




/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: fc8f81f4-d22d-4894-a553-f8e4a3f3e272
// end_of_file
*/
