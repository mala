/*
 * Copyright (C) 2004, Christian Thaeter <chth@gmx.net>
 *
 * This file is part of the MaLa extension Language.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, contact me.
 */

#include <stdio.h>
#include <stdlib.h>
#include <nobug.h>
#include <acogc.h>

#include "../engine/strings.h"
#include "../engine/stringbucket.h"

#define TEST(t) printf("TEST %s: ",#t);\
 if((t))printf(".. OK\n");\
 else printf(".. FAILED\n")

#define TEST_VOID(t) printf("TEST %s: ",#t);\
 fflush(stdout); t; printf(".. OK\n")

acogc_root gcroot;
acogc_factory bucketfactory;
acogc_factory gcfactory_strings;
acogc_factory gcfactory_splaynodes;

void
construction (MalaStringBucket testbucket)
{
  ACOGC_STACK_ENTER (testbucket->gcroot);
  ACOGC_STACK_PTR (MalaString, a);
  ACOGC_STACK_PTR (MalaString, b);
  ACOGC_STACK_PTR (MalaString, c);
  ACOGC_STACK_PTR (MalaString, d);
  ACOGC_STACK_PTR (MalaString, e);
  ACOGC_STACK_PTR (MalaString, onetwothreefour);

  TEST(a.ptr = mala_string_new_cstr ("bar", testbucket));

  TEST(b.ptr = mala_string_new_cstr ("baz", testbucket));

  TEST(c.ptr = mala_string_new_cstr ("bar", testbucket));

  TEST(a.ptr == c.ptr);

  TEST(mala_string_compare (a.ptr, b.ptr) == -1);
  TEST(mala_string_compare (b.ptr, c.ptr) == 1);

  TEST(a.ptr = mala_string_new_literal_cstr ("foobar", testbucket));

  TEST(strcmp (mala_string_cstr (a.ptr), "foobar") == 0);

  TEST(b.ptr = mala_string_new_attach_cstr (strdup ("foo"), testbucket));
  TEST(strcmp (mala_string_cstr (b.ptr), "foo") == 0);

  TEST(c.ptr = mala_string_new_cstr_n ("foobar", 3, testbucket));
  TEST(c.ptr == b.ptr);

  TEST(d.ptr = mala_string_new_2cstr ("foo", "bar", testbucket));
  TEST(d.ptr == a.ptr);

  TEST(a.ptr = mala_string_new_cstr_n ("foobar", 3, testbucket));

  TEST(a.ptr = mala_string_new_n (a.ptr, 3, testbucket));
  TEST(strcmp(mala_string_cstr (a.ptr), "foo") == 0);

  TEST(b.ptr = mala_string_new_prefix ("bar", a.ptr, testbucket));
  TEST(c.ptr = mala_string_new_literal_cstr ("barfoo", testbucket));
  TEST(c.ptr == b.ptr);

  TEST(onetwothreefour.ptr = mala_string_new_literal_cstr ("1234", testbucket));
  TEST(mala_string_length (onetwothreefour.ptr) == 4);

  TEST(d.ptr = mala_string_new_print (testbucket, "%d", 1234));
  TEST(mala_string_length (d.ptr) == 4);
  TEST(d.ptr == onetwothreefour.ptr);
  TEST(mala_string_compare (d.ptr, onetwothreefour.ptr) == 0);

  TEST(e.ptr = mala_string_new_print (testbucket, "%s", "123456789abcdef longer string"));
  TEST(mala_string_length (e.ptr) == 29);

  ACOGC_STACK_LEAVE;
}

void tools (MalaStringBucket testbucket)
{
  MalaString a;

  int i;
  char abcde[6];
  float f;
  char foobar[7];

  a = mala_string_new_literal_cstr ("1234 abcde 2.25 foobar" , testbucket);

  TEST(4 == mala_string_scan (a, "%d %s %f %s", &i, abcde, &f, foobar));

  TEST(i == 1234);
  TEST(strcmp(abcde, "abcde") == 0);
  TEST(f == 2.25);
  TEST(strcmp(foobar, "foobar") == 0);
}

int main()
{
  acogc_root_init (&gcroot);

  acogc_factory_init (&bucketfactory,
                      &gcroot,
                      sizeof(mala_stringbucket),
                      10,
                      20,
                      mala_stringbucket_acogc_mark,
                      mala_stringbucket_acogc_initize,
                      NULL,
                      mala_global_type_stringbucket);

  acogc_factory_init (&gcfactory_strings,
                      &gcroot,
                      sizeof (mala_string),
                      mala_global_stringsfactory_low,
                      mala_global_stringsfactory_high,
                      mala_string_acogc_mark,
                      mala_string_acogc_initize,
                      mala_string_acogc_finalize,
                      mala_global_type_string);

  acogc_factory_init (&gcfactory_splaynodes,
                      &gcroot,
                      sizeof (mala_stringbucketnode),
                      mala_global_splayfactory_low,
                      mala_global_splayfactory_high,
                      mala_stringbucketnode_acogc_mark,
                      mala_stringbucketnode_acogc_initize,
                      mala_stringbucketnode_acogc_finalize,
                      mala_global_type_stringbucketnode);


  MalaStringBucket testbucket = mala_stringbucket_new (MALA_STRING_FWD, &bucketfactory, &gcfactory_strings, &gcfactory_splaynodes);
  acogc_addroot (testbucket);

  construction (testbucket);
  tools (testbucket);

  acogc_root_erase (&gcroot);
  exit(0);
}

/*
// arch-tag: 99da2844-99b2-4ec3-81ed-32db697656ec
*/
