/*
  bench-strinbucket.c - used to benchmark mala's string lib performance

  Copyright (C) 2006, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#define _GNU_SOURCE

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <nobug.h>

#include "../engine/stringbucket.h"
#include "../engine/strings.h"

//#define GAUSS_DIST 1
//#define GAUSS_DIST 3
//#define GAUSS_DIST 17
#define GAUSS_DIST 47
//#define GAUSS_DIST 253

acogc_root gcroot;
acogc_factory bucketfactory;
acogc_factory gcfactory_strings;
acogc_factory gcfactory_splaynodes;

char* strtable[65536];

static inline uint32_t bench_fast_prng ()
{
  static uint32_t rnd = 0xdeadbabe;
  return rnd = rnd<<1 ^ ((rnd >> 30) & 1) ^ ((rnd>>2) & 1);
}


static inline uint32_t bench_gauss_prng (unsigned g)
{
  static uint32_t rnd = 0xdeadbabe;
  uint32_t s = 0;
  for (unsigned i = 0; i < g; ++i)
    s += (rnd = rnd<<1 ^ ((rnd >> 30) & 1) ^ ((rnd>>2) & 1));
  return s/g;
}


void
run (MalaStringBucket b)
{
  ACOGC_STACK_ENTER (b->gcroot);
  ACOGC_STACK_PTR (MalaString, str);

  ACOGC_WEAK_REFERENCE(last);

  int first = 0;

  /*
  str = mala_string_new_cstr ("foobar", b, NULL);
  acogc_reference_link_weak (&last, str, &str->weakrefs);
  acogc_reference_weak_unlink (&last);
  if (0)
  */
  //for (int i = 0; i<500000; ++i)
  for (int i = 0; i<5000; ++i)
  //for (int i = 0; i<1; ++i)
    {
      //LOG_DBG (NOBUG_INFO, "ITERATION %d", i);

      //uint32_t rg = bench_gauss_prng (GAUSS_DIST)/0xFF;
#if 1
      uint32_t rg = bench_gauss_prng (GAUSS_DIST)/0xFFF;
      //uint32_t rg = bench_gauss_prng (GAUSS_DIST)/0xFFFF;
      rg %= 65535;
      char* cstr = strtable[rg];
#else
      char cstr[50];
      sprintf(cstr,"%d",i%6);
#endif


      str.ptr = mala_string_new_cstr (cstr, b);
      assert(str.ptr);
      //TODO mala_string_user_init (str.ptr);

      ASSERT (strcmp(mala_string_cstr (str.ptr),cstr) == 0, "lookup failed");

      // if (last.ref)
      if (first)
        {
          //TODO mala_string_set_user (str.ptr, acogc_weakref_get (&last));

          acogc_weakref_unlink (&last);
        }
      else
        {
          first = 1;
          //TODO mala_string_set_user (str.ptr , NULL);
          //acogc_addroot (str.ptr);
          //        acogc_object_gcroot_remove (str);
        }
      acogc_weakref_link (&last, str.ptr);

      //mala_stringbucketnode_dump ("splayed", &b->tree, 40);
    }
  //acogc_root_collect(b->gcroot, ACOGC_COLLECT_ALLFREEABLE, NULL);
  //acogc_root_collect(b->gcroot, ACOGC_COLLECT_DONTFREE, NULL);
  //acogc_root_collect(b->gcroot, ACOGC_COLLECT_DONTFREE, NULL);
  //acogc_root_collect(b->gcroot, ACOGC_COLLECT_DONTFREE, NULL);
  mala_stringbucketnode_dump ("finished", &b->tree, 40);
  acogc_weakref_unlink (&last);
  //  acogc_root_collect (&gcroot,NULL,ACOGC_COLLECT_NORMAL);
  ACOGC_STACK_LEAVE;
}


void
cleanup (MalaStringBucket b)
{
  for (int i = 0; i<65536; ++i)
    free(strtable[i]);

  (void) b;
}

int main(int argc, char* argv[])
{
  (void) argc;
  (void) argv;

  //  CALLGRIND_START_INSTRUMENTATION();
  //CALLGRIND_TOGGLE_COLLECT();
  acogc_root_init (&gcroot);

  acogc_factory_init (&bucketfactory,
                      &gcroot,
                      sizeof(mala_stringbucket),
                      10,
                      20,
                      mala_stringbucket_acogc_mark,
                      mala_stringbucket_acogc_initize,
                      NULL,
                      mala_global_type_stringbucket);

  acogc_factory_init (&gcfactory_strings,
                      &gcroot,
                      sizeof (mala_string),
                      mala_global_stringsfactory_low,
                      mala_global_stringsfactory_high,
                      mala_string_acogc_mark,
                      mala_string_acogc_initize,
                      mala_string_acogc_finalize,
                      mala_global_type_string);

  acogc_factory_init (&gcfactory_splaynodes,
                      &gcroot,
                      sizeof (mala_stringbucketnode),
                      mala_global_splayfactory_low,
                      mala_global_splayfactory_high,
                      mala_stringbucketnode_acogc_mark,
                      mala_stringbucketnode_acogc_initize,
                      mala_stringbucketnode_acogc_finalize,
                      mala_global_type_stringbucketnode);

  for (int i = 0; i<65536; ++i)
    asprintf(&strtable[i], "--%X", bench_fast_prng ());


  MalaStringBucket testbucket = acogc_factory_alloc (&bucketfactory);
  acogc_addroot (testbucket);

  mala_stringbucket_init (testbucket, MALA_STRING_FWD, &gcroot, &gcfactory_strings, &gcfactory_splaynodes);


  //  CALLGRIND_TOGGLE_COLLECT();
  INFO (NOBUG_ON, "RUN");
  run (testbucket);
  //CALLGRIND_TOGGLE_COLLECT();

  INFO (NOBUG_ON, "CLEANUP");
  cleanup (testbucket);

  INFO (NOBUG_ON, "ERASE");

  acogc_removeroot (testbucket);
  testbucket->tree = NULL;
  acogc_root_erase (&gcroot);

  return 0;
}
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 171785df-d119-4794-923b-5522e04f9402
// end_of_file
*/
