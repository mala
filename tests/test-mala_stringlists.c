/*
 * test-mala_stringlists.c
 *
 * Copyright (C) 2004, Christian Thaeter <chth@gmx.net>
 *
 * This file is part of the MaLa extension Language.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, contact me.
 */

#include <stdio.h>
#include <stdlib.h>
#include <nobug.h>
#include <acogc.h>

#include "../engine/strings.h"
#include "../engine/stringbucket.h"
#include "../engine/stringlist.h"

#define TEST(t) printf("TEST %s: ",#t);\
 if((t))printf(".. OK\n");\
 else printf(".. FAILED\n")

#define TEST_VOID(t) printf("TEST %s: ",#t);\
 fflush(stdout); t; printf(".. OK\n")


void
runtest (MalaEngine engine)
{
  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, a);
  ACOGC_STACK_PTR (MalaStringList, b);
  ACOGC_STACK_PTR (MalaStringList, c);


  TEST (a.ptr = mala_stringlist_new (engine));
  TEST (b.ptr = mala_stringlist_new (engine));

  char* testarray[] = {"foo", "bar", "baz", NULL};

  mala_stringlist_dump (a.ptr, stdout, "empty list..\n", "  %d: ", "%s\n", "", " ..OK\n\n");

  mala_stringlist_append_cstrs (a.ptr, -1, testarray, engine);

  mala_stringlist_dump (a.ptr, stdout, "append cstrs..\n", "  %d: ","%s\n", "", " ..OK\n\n");

  ACOGC_STACK_LEAVE;
}

int main()
{
  MalaEngine engine = mala_engine_new ();

  runtest (engine);

  mala_engine_free (engine);
  exit(0);
}

/*
// arch-tag: 9c80a752-81ef-47f3-8536-05e15605b639
*/
