/*
    test-mala.c - MaLa test driver

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <nobug.h>
#include <acogc.h>

#include "mala.h"
#include "std.h"

mala_actioninit my_actions[] =
  {
    MALA_PARSER_SUBSTITUTE("--SUBST-TEST","SUBST_SUCCEEDED"),
    MALA_PARSER_EXPAND("--FOOBAR",("foo","bar")),
    MALA_ACTIONS_END
  };

int
main(int argc, char * argv[])
{
  MalaEngine engine;

  engine = mala_engine_new ();

  TRACE (NOBUG_ON);

  ACOGC_STACK_ENTER (&engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, args);
  ACOGC_STACK_PTR (MalaStringList, result);

  mala_module_std_init (engine);

  // TODO mala_engine_new_main
  mala_engine_actions_register (engine, my_actions);

  args.ptr = mala_stringlist_new (engine);
  mala_stringlist_append_cstrs (args.ptr, argc - 1, argv + 1, engine);

  mala_engine_action_new_cstr (engine, "--ARGV0",
                               mala_substitute_parser,
                               argv[0], ACOGC_STATIC,
                               mala_engine_action_get_cstr (engine, "--WORLD"));

  mala_engine_action_new_cstr (engine, "--COMMANDLINE",
                               mala_expand_parser,
                               args.ptr, ACOGC_GC,
                               mala_engine_action_get_cstr (engine, "--WORLD"));

  printf ("stdout: ");
  mala_engine_run_cstr (engine, "--COMMANDLINE", &result.ptr);

  mala_stringlist_dump (result.ptr, stdout, ".\n", "literal: ","%s.\n", "", "");


  printf ("variable: %s\n", getenv ("variable"));
  printf ("state: %s\n", mala_engine_state_str (engine));
  ACOGC_STACK_LEAVE;
  mala_engine_free (engine);
  return 0;
}



/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 610917e5-3918-4cba-be05-bf83efb45223
// end_of_file
*/
