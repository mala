/*
  std_macros.c - MaLa standard module macros

  Copyright (C) 2005, 2006, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mala.h"
#include "std_macros.h"

static void
mala_stringlist_macrocheck (MalaStringList list, int * max_arg, mala_state argt[10]);

static void
mala_string_macrocheck (MalaString string, int * max_arg, mala_state argt[10]);

static MalaString
mala_string_new_macrosubst (MalaString src, MalaString args[10], MalaEngine eng);

static MalaStringList
mala_stringlist_new_macrosubst (MalaStringList src, MalaString args[10], MalaEngine eng);



static
mala_actioninit std_macros[] =
  {
    MALA_PARSER_SIMPLE("--DEL", mala_delete_parser),
    MALA_PARSER_SIMPLE("--BEGIN", mala_begin_parser),

    // MALA_PARSER_BRIEF("--BEGIN", "defines a block or list"),
    //TODO MALA_PARSER_SIGNATURE_TYPE("--BEGIN", ("SEQUENCE","END")),
    //TODO MALA_PARSER_SIGNATURE_USAGE("--BEGIN", ("content of the block", "a --END which closes the block")),
    //TODO MALA_PARSER_RESULT_TYPE("--BEGIN", ("BLOCK")),
    //TODO MALA_PARSER_RESULT_USAGE("--BEGIN", ("a unique name of the block")),
    //TODO MALA_PARSER_HELP("--BEGIN", ("Blocks can have % substitutions, Blocks are garbage collected, TODO")),


    MALA_PARSER_SIMPLE("--END", mala_end_parser),
    // TODO MALA_PARSER_BRIEF("--END", "closes a block"),
    //TODOMALA_PARSER_HELP("--END", ("an --END without a matching --BEGIN raises an error.")),

    MALA_PARSER_SIMPLE("--DEF", mala_def_parser),
    //MALA_PARSER_BRIEF("--DEF", "defines a substitution"),
    //MALA_PARSER_SIGNATURE_TYPE("--DEF", ("WORD", "WORD-OR-BLOCK")),
    //TODO MALA_PARSER_SIGNATURE_USAGE("--DEF", ("word to be substituted",
    //                                    "replaced with this")),
    //TODO MALA_PARSER_HELP("--DEF", ("The second argument can be a block of words enclosed in --BEGIN ... --END with % substitution sequences. See help for --BEGIN for details.")),
    //TODO MALA_PARSER_EXAMPLE("--DEF", "--DEF `foo `bar\t expands foo to bar\n--DEF `--action --BEGIN foo bar --END\t --action will expand to foo bar"),
    //MALA_PARSER_BRIEF("MACROS", "TODO"),
    //MALA_PARSER_HELP("MACROS", ("TODO")),

    /*
    MALA_PARSER("--", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),

    MALA_PARSER("--FOREACH-WORD", mala_foreach_word_parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),
    */




    //MALA_PARSER_BRIEF("--DEL", "deletes a macro"),
    //TODO MALA_PARSER_SIGNATURE_TYPE("--DEL", ("WORD")),
    //TODO MALA_PARSER_SIGNATURE_USAGE("--DEL", ("name of the Macro to delete")),
    //TODO MALA_PARSER_HELP("--DEL", ("Removes the last definition of the most recent definition with the matching name and any childs which where attached to it (see --ATTACH)")),


    MALA_ACTIONS_END
  };


mala_state
mala_begin_parser (MalaProgram prg)
{
  MalaStringList end;
  unsigned depth = 0;

  TRACE (mala_module_std);

  // find matching --END
  MalaStringList begin = mala_program_pptr (prg);
  for (end = begin;
       !mala_stringlist_is_end (prg->program, end);
       mala_stringlist_fwd (&end))
    {
      if (mala_stringlist_string (end) == prg->engine->common_string[MALA_STRING_BEGIN])
        ++depth;
      else if (mala_stringlist_string (end) == prg->engine->common_string[MALA_STRING_END])
        --depth;

      if (!depth)
        break;
    }

  if (depth)
    return mala_program_commonexception (prg, MALA_STRING_ERROR_MISSING_END, prg->program);

  ACOGC_STACK_ENTER (&prg->engine->gcroot);
  ACOGC_STACK_PTR (MalaStringList, list);
  ACOGC_STACK_PTR (MalaString, name);

  list.ptr = mala_stringlist_new (prg->engine);
  unsigned length = 0;

  // move elements into destionation list
  while (mala_stringlist_next (begin) != end)
    {
      mala_stringlist_insert_tail (list.ptr, mala_stringlist_next (begin));
      ++length;
    }

  MalaParserFunc parser;
  int max_arg = -1;
  mala_state argt[10] = {[0 ... 9] = MALA_EXCEPTION};

  const char* templ;
  mala_state ret;
  void * data = NULL;

  if (length == 0)
    {
      // --BEGIN --END becomes --PASS
      TODO ("direct --PASS substitute");
      parser = mala_pass_parser;
      templ = "--BLOCK_%08X";
      ret = MALA_BLOCK;
    }
  else if (length == 1)
    {
      data = mala_stringlist_string (mala_stringlist_head_get (list.ptr));
      mala_string_macrocheck ((MalaString) data, &max_arg, argt);
      if (max_arg == INT_MAX)
        {
          mala_stringlist_insert_after_stringlist (begin, list.ptr);
          ACOGC_STACK_LEAVE;
          return mala_program_commonexception (prg, MALA_STRING_ERROR_MACRO_SYNTAX, end); //TODO?
        }

      if (max_arg == -1)
        {
          // --BEGIN FOO --END becomes FOO
          TODO ("direct name substitute");
          parser = mala_substitute_parser;
          templ = "--BLOCK_%08X";
          ret = MALA_BLOCK;
        }
      else
        {
          parser = mala_macro_parser;
          templ = "--BLOCK_%08X";
          ret = MALA_BLOCK;
        }
    }
  else
    {
      data = list.ptr;
      mala_stringlist_macrocheck (list.ptr, &max_arg, argt);
      if (max_arg == INT_MAX)
        {
          mala_stringlist_insert_after_stringlist (begin, list.ptr);
          ACOGC_STACK_LEAVE;
          return mala_program_commonexception (prg, MALA_STRING_ERROR_MACRO_SYNTAX, mala_program_pptr (prg));
        }

      if (max_arg == -1)
        {
          parser = mala_expand_parser;
          templ = "--BLOCK_%08X";
          ret = MALA_BLOCK;
        }
      else
        {
          parser = mala_macro_parser;
          templ = "--BLOCK_%08X";
          ret = MALA_BLOCK;
        }
    }

  mala_stringlist_remove (mala_program_pptr (prg));

  MALA_MUTATOR_BEGIN
    {
      // insert new --nnn... in program
      name.ptr = mala_engine_tmpname (prg->engine, templ);
      TRACE (mala_module_std, "Block %s", mala_string_cstr (name.ptr) );
      mala_action_new (name.ptr, parser, data, data?ACOGC_GC:ACOGC_UNSET, prg->engine);
      ACOGC_STACK_PTR (MalaStringList, node);
      node.ptr = mala_stringlist_node_new (name.ptr, prg->engine);
      mala_stringlist_insert_after (end, node.ptr);
      mala_stringlist_remove (mala_program_pptr (prg));
    }
  else
    {
      mala_program_pptr (prg)->string = prg->engine->common_string[MALA_STRING_PASS];
    }
  MALA_MUTATOR_END;

  ACOGC_STACK_LEAVE;
  return ret;
}

mala_state
mala_substitute_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  MALA_MUTATOR_BEGIN
    {
      REQUIRE (acogc_object_type (mala_program_pptr_action (prg)->data)
               == &prg->engine->gcfactory_strings);

      mala_program_pptr (prg)->string = mala_action_string (mala_program_pptr_action (prg));
    }
  MALA_MUTATOR_END;

  return MALA_MACRO;
}

mala_state
mala_expand_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  MALA_MUTATOR_BEGIN
    {
      MalaAction act = mala_program_pptr_action (prg);
      REQUIRE (acogc_object_type (mala_action_dataptr (act))
               == &prg->engine->gcfactory_stringlists);

      MalaStringList pos = mala_program_pptr (prg);
      MalaStringList itr;

      for (itr = mala_stringlist_tail_get (mala_action_stringlist (act));
           !mala_stringlist_is_end (mala_action_stringlist (act), itr);
           mala_stringlist_rev (&itr))
        {
          MalaStringList node = mala_stringlist_node_new (mala_stringlist_string (itr), prg->engine);
          mala_stringlist_insert_after (pos, node);
        }
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 0);
  return MALA_MACRO;
}


mala_state
mala_macro_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  /* How much to eveluate the argument (for now, likely extended in future):
     MALA_EXCEPTION is used as pseudo tag for unused arguments/initialization
     MALA_START doesn't evaluate the argument
     MALA_LITERAL will evaluate fully
     .. other destination states are reserved for future
  */
  int max_arg = -1;
  mala_state argt[10] = {[0 ... 9] = MALA_EXCEPTION};
  MalaAction act = mala_program_pptr_action (prg);

  // scan for arguments
  AcogcFactory type = mala_action_data_type (act);
  if (type == &prg->engine->gcfactory_strings)
    {
      mala_string_macrocheck (mala_action_string (act), &max_arg, argt);
    }
  else if (type == &prg->engine->gcfactory_stringlists)
    {
      mala_stringlist_macrocheck (mala_action_stringlist (act), &max_arg, argt);
    }
  else
    NOTREACHED;

  if (max_arg == INT_MAX)
    return mala_program_commonexception (prg, MALA_STRING_ERROR_MACRO_SYNTAX, mala_program_pptr (prg));

  if (max_arg == -1)
    return mala_expand_parser (prg);

  // evaluate args
  for (int i=1; i <= max_arg; ++i)
    {
      if (argt[i] <= MALA_START)
        {
          mala_state state = mala_program_eval_arg (prg, i, argt[i], NULL);
          if (state > MALA_START)
            return state;

          TODO("use dst in arg_eval");
        }
    }

  MALA_MUTATOR_BEGIN
    {
      // construct result list
      MalaString args[10];
      MalaStringList p = mala_program_pptr (prg);

      args[0] = mala_stringlist_string (p);
      for (int i=1; i <= max_arg; ++i)
        {
          mala_stringlist_fwd (&p);
          args[i] = mala_stringlist_string (p);
        }

      ACOGC_STACK_ENTER (&prg->engine->gcroot);
      // constructing list
      if (type == &prg->engine->gcfactory_strings)
        {
          ACOGC_STACK_PTR (MalaString, string);
          string.ptr = mala_string_new_macrosubst (mala_action_string (act),
                                                   args, prg->engine);

          MalaStringList n = mala_stringlist_node_new (string.ptr, prg->engine);
          mala_stringlist_insert_after (p, n);
        }
      else if (type == &prg->engine->gcfactory_stringlists)
        {
          ACOGC_STACK_PTR (MalaStringList, list);
          list.ptr = mala_stringlist_new_macrosubst (mala_action_stringlist (act),
                                                     args, prg->engine);
          mala_stringlist_insert_after_stringlist (p, list.ptr);
        }
      else
        NOTREACHED;

      ACOGC_STACK_LEAVE;
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, max_arg);
  return MALA_MACRO;
}

mala_state
mala_end_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  return mala_program_commonexception (prg, MALA_STRING_ERROR_END_WITHOUT_BEGIN, mala_program_pptr (prg));
}

mala_state
mala_def_parser (MalaProgram prg)
{
  MalaStringList dst;
  MalaString name;
  MalaString expand;

  TRACE (mala_module_std);
  // evaluate both args
  mala_state state = mala_program_eval_arg (prg, 1, MALA_LITERAL, &dst);
  if (state > MALA_START)
    return state;

  name = mala_stringlist_string (dst);

  state = mala_program_eval_arg (prg, 2, MALA_BLOCK, &dst);
  if (state > MALA_START)
    return state;

  expand = mala_stringlist_string (dst);

  TRACE (mala_module_std, "def %s as %s", mala_string_cstr (name), mala_string_cstr (expand));

  MALA_MUTATOR_BEGIN
    {
      TODO("optimize direct expansions");
      int max_arg = -1;
      mala_state argt[10] = {[0 ... 9] = MALA_EXCEPTION};
      mala_string_macrocheck (expand, &max_arg, argt);

      if (max_arg == INT_MAX)
        return mala_program_commonexception (prg, MALA_STRING_ERROR_MACRO_SYNTAX, dst);
      else if (max_arg == -1)
        // register as simple substitute
        mala_action_new (name, mala_substitute_parser, expand, ACOGC_GC, prg->engine);
      else
        // register as macro
        mala_action_new (name, mala_macro_parser, expand, ACOGC_GC, prg->engine);
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 2);
  return MALA_STATEMENT;
}


mala_state
mala_delete_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  MalaStringList dst;

  mala_state state = mala_program_eval_arg (prg, 1, MALA_LITERAL, &dst);
  if (state > MALA_START)
    return state;

  MALA_MUTATOR_BEGIN
    {
      MalaAction act = mala_string_action (mala_stringlist_string (dst));
      if (act)
        mala_action_delete (mala_string_action (mala_stringlist_string (dst)));
      else
        return mala_program_commonexception (prg, MALA_STRING_ERROR_ACTION_NOT_DEFINED, dst);
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 1);
  return MALA_STATEMENT;
}


static void
mala_stringlist_macrocheck (MalaStringList list, int * max_arg, mala_state argt[10])
{
  LLIST_FOREACH (&list->node, itr)
    {
      MalaStringList l = LLIST_TO_STRUCTP(itr, mala_stringlist, node);
      mala_string_macrocheck (mala_stringlist_string (l), max_arg, argt);
      if (*max_arg == INT_MAX)
        return;
    }
}

static void
mala_string_macrocheck (MalaString string, int * max_arg, mala_state argt[10])
{
  const char * c;
  for (c = mala_string_cstr (string); *c; ++c)
    {
      if (*c == '%')
        {
          ++c;
          if (*c == '%')
            {
              if (*max_arg == -1)
                *max_arg = 0;
              continue;
            }
          if (*c == '^')
            {
              ++c;
              if (*c >= '0' && *c <= '9')
                {
                  if (argt[*c - '0'] < MALA_EXCEPTION && argt[*c - '0'] != MALA_START)
                    goto esyntax;
                  if (*c > (char) *max_arg + '0')
                    *max_arg = *c - '0';
                  argt[*c - '0'] = MALA_START;
                }
              else
                goto esyntax;
            }
          else if (*c >= '0' && *c <= '9')
            {
              if (argt[*c - '0'] != MALA_EXCEPTION && argt[*c - '0'] != MALA_LITERAL)
                goto esyntax;
              if (*c > (char) *max_arg + '0')
                *max_arg = *c - '0';
              argt[*c - '0'] = MALA_LITERAL;
            }
          else
            goto esyntax;
        }
    }
  return;

 esyntax:
  *max_arg = INT_MAX;
  return;
}


static MalaString
mala_string_new_macrosubst (MalaString src, MalaString args[10], MalaEngine eng)
{
  // 1st pass get size
  size_t size = mala_string_length (src);
  for (const char *c = mala_string_cstr (src); *c; ++c)
    {
      if (*c == '%')
        {
          ++c;
          --size;
          if (*c == '%')
            continue;
          if (*c == '^')
            {
              ++c;
              --size;
            }
          --size;
          TODO ("Handle %%^0");
          size += mala_string_length (args[*c - '0']);
        }
    }

  // 2nd pass create string
  char* cstr = NULL;
  acogc_alloc (&cstr, size+1, &eng->gcroot);

  char* p = cstr;

  for (const char *c = mala_string_cstr (src); *c; ++c)
    {
      if (*c == '%')
        {
          ++c;
          if (*c == '%')
            {
              *p++ = *c;
              continue;
            }
          if (*c == '^')
            ++c;

          TODO ("Handle %%^0");

          strcpy (p, mala_string_cstr (args[*c - '0']));
          p += mala_string_length (args[*c - '0']);
        }
      else
        *p++ = *c;
    }
  *p = '\0';

  return mala_string_new_attach_cstr (cstr, eng->words);
}

static MalaStringList
mala_stringlist_new_macrosubst (MalaStringList src, MalaString args[10], MalaEngine eng)
{
  ACOGC_STACK_ENTER (&eng->gcroot);
  ACOGC_STACK_PTR (MalaStringList, ret);
  ACOGC_STACK_PTR (MalaString, str);

  ret.ptr = mala_stringlist_new (eng);

  LLIST_FOREACH (&src->node, node)
    {
      str.ptr = mala_string_new_macrosubst (mala_stringlist_string_from_llist(node), args, eng);
      MalaStringList n = mala_stringlist_node_new (str.ptr, eng);
      mala_stringlist_insert_tail (ret.ptr, n);
    }

  ACOGC_STACK_LEAVE;
  return ret.ptr;
}






/*
  realloc a string to at least needed size
  return the amount really reserved or 0 on error
*/
#if 0 // TODO acogc reserve!
static size_t
reserve_string (char ** s, size_t actual, size_t needed)
{
  size_t n;
  char * r;

  for (n = actual>64?actual:64; n <= needed; n += (n>>1)); /*n = n * 1.5*/

  r = realloc (*s, n);
  if (!r)
    {
      /* that was to much, try conservatively */
      r = realloc (*s, n = needed);
      if (!r)
        return 0;
    }
  *s = r;
  return n;
}
#endif

int
mala_module_std_macros_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_macros);
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 4722e09b-a020-4206-9ecc-9442505826c5
// end_of_file
*/
