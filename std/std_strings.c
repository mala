/*
  std_strings.c - MaLa standard module stringss

  Copyright (C) 2006, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mala.h"
#include "std_strings.h"


static
mala_actioninit std_strings[] =
  {
    // TODO to strings module
    MALA_PARSER_MACRO("--CAT",("%1%2")),
    // MALA_PARSER_BRIEF("--CAT", "concatenates two words"),
    // MALA_PARSER_HELP("--CAT", ("TODO")),
    // MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    // MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    // MALA_PARSER_RESULT_TYPE("--", ("")),
    // MALA_PARSER_RESULT_USAGE("--", ("")),


    MALA_ACTIONS_END
  };

int
mala_module_std_strings_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_strings);
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
*/
