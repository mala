/*
  std_statements.h - MaLa imperative alike statements

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_STATEMENTS_H
#define MALA_STD_STATEMENTS_H

#include "std.h"

MALA_PARSER_DEFINE(literal);
MALA_PARSER_DEFINE(skip);
MALA_PARSER_DEFINE(pass);
MALA_PARSER_DEFINE(gc);
MALA_PARSER_DEFINE(remove);
MALA_PARSER_DEFINE(no);
MALA_PARSER_DEFINE(nono);
MALA_PARSER_DEFINE(ifelse);
MALA_PARSER_DEFINE(sleep);
MALA_PARSER_DEFINE(stop);
MALA_PARSER_DEFINE(eval);
MALA_PARSER_DEFINE(throw);
MALA_PARSER_DEFINE(try);
MALA_PARSER_DEFINE(replace);
//MALA_PARSER_DEFINE(defined);
//MALA_PARSER_DEFINE(trace);
//MALA_PARSER_DEFINE(gctrace);
//MALA_PARSER_DEFINE(exception);

//MALA_PARSER_DEFINE(pass);

int
mala_module_std_statements_init (MalaEngine self);

#endif /* MALA_STD_STATEMENTS_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 739b75b9-0297-4d15-9efb-8d0cfe5ac3f1
// end_of_file
*/
