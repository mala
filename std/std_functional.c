/*
  std_parsers.c - MaLa standard module parsers

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mala.h"
#include "std_functional.h"

static size_t
reserve_string (char ** s, size_t actual, size_t needed);

static void
mala_macrocheck_list (MalaStringList list, int * max_arg, int argt[10]);

static void
mala_macrocheck_string (MalaString string, int * max_arg, int argt[10]);


#if 0
    MALA_PARSER("--MAP", mala_map_parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--MAP", "TODO applies an action to each element of a nested list"),
    MALA_PARSER_HELP("--MAP", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--FOREACH-WORD", ("DEFINED-ACTION", "MACRO-LIST")),
    MALA_PARSER_SIGNATURE_USAGE("--FOREACH-WORD", ("Action to be applied",
                                                   "list of lists")),
    MALA_PARSER_RESULT_TYPE("--MAP", ("SEQUENCE")),
    MALA_PARSER_RESULT_USAGE("--MAP", ("Resulting Sequence")),


    MALA_PARSER("--LENGTH", mala_length_parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--LENGTH", "returns the length of a sequence"),
    MALA_PARSER_HELP("--LENGTH", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--LENGTH", ("SEQUENCE")),
    MALA_PARSER_SIGNATURE_USAGE("--LENGTH", ("list of lists")),
    MALA_PARSER_RESULT_TYPE("--LENGTH", ("INTEGER")),
    MALA_PARSER_RESULT_USAGE("--LENGTH", ("Length of the Sequence")),
#endif

int
mala_substitute_parser (MalaEngine eng,
                        MalaStringListNode_ref pptr,
                        void * data)
{
  (void) eng;

  MALA_SIDEEFFECT_BEGIN
    mala_stringlistnode_substitute_string (*pptr, (MalaString) data);
  MALA_SIDEEFFECT_END;

  return MALA_CONTINUE;
}

int
mala_expand_parser (MalaEngine eng,
                    MalaStringListNode_ref pptr,
                    void * data)
{
  MalaStringListNode itr;
  MalaStringListNode end;

  MALA_SIDEEFFECT_BEGIN
    {
      end = mala_stringlistnode_next (*pptr);

      for (itr = mala_stringlist_tail ((MalaStringList) data);
           !mala_stringlist_is_end ((MalaStringList) data, itr);
           mala_stringlistnode_rev (&itr))
        {
          if (!mala_stringlist_after_new (&eng->program, *pptr,
                                          mala_stringlistnode_string (itr)))
            goto ealloc_node;
        }
    }
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 0);
  return MALA_CONTINUE;

 ealloc_node:
  /* remove already added things */
  for (itr = mala_stringlistnode_next (*pptr);
       itr != end;
       mala_stringlist_elem_delete_fwd (&eng->program, &itr));

  return MALA_EALLOC;
}


int
mala_macro_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  MalaStringListNode itr;
  MalaStringListNode last = NULL;
  MalaStringList list;
  const char* c;
  int i;
  int max_arg = 0;
  int argt[10] = {0};
  char *p;
  MalaString args[10];
  MalaString str = NULL;

  // scan how much args are needed
  mala_macrocheck_list ((MalaStringList) data, &max_arg, argt);

  // evaluate args, fill arg-array
  args[0] = mala_stringlistnode_string (*pptr);

  for (i = 1; i <= max_arg; ++i)
    {
      if (!(last = mala_engine_arg_eval (eng, pptr, i, argt[i] == 1 ? -1 : 0, NULL, NULL))
          || eng->state > MALA_EFAULT)
        return eng->state;

      args[i] = mala_stringlistnode_string (last);
    }

  MALA_SIDEEFFECT_BEGIN
    {
      // build list with substitutions
      list = mala_stringlist_new ();
      if (!list)
        goto ealloc_list;

      for (itr = mala_stringlist_head ((MalaStringList) data);
           !mala_stringlist_is_end ((MalaStringList) data, itr);
           mala_stringlistnode_fwd (&itr))
        {
          size_t size = 64;
          if (!(p = malloc (size)))
            goto ealloc_cstr;

          for (c = mala_string_cstr (mala_stringlistnode_string (itr)), i = 0; *c; ++c,++i)
            {
              if (*c == '%')
                {
                  ++c;
                  if (*c == '%')
                    {
                      // %% -> %
                      goto normal_char;
                    }
                  else if (*c >= '0' && *c <= '9')
                    {
                      // %0..%9
                      if (size <= i+mala_string_length (args[*c-'0'])
                          && !(size = reserve_string (&p,
                                                      size,
                                                      1 + i + mala_string_length (args[*c-'0']))))
                        goto ealloc_reserve;
                      strcpy(p+i, mala_string_cstr (args[*c-'0']));
                      i += (mala_string_length (args[*c-'0']) - 1);
                    }
                  else if (*c == '-')
                    {
                      ++c;
                      if (*c >= '0' && *c <= '9')
                        {
                          // %0..%9
                          if (size <= i+mala_string_length (args[*c-'0'])
                              && !(size = reserve_string (&p,
                                                          size,
                                                          1 + i +
                                                          mala_string_length (args[*c-'0']))))
                            goto ealloc_reserve;
                          strcpy(p+i, mala_string_cstr (args[*c-'0']));
                          i += (mala_string_length (args[*c-'0']) - 1);
                        }
                    }
                }
              else
                {
                normal_char:
                  // normal char
                  if (size <= (size_t) i && !(size = reserve_string (&p, size, (size_t) i+1)))
                    goto ealloc_reserve;
                  p[i] = *c;
                }
            }

          p[i] = '\0';

          char* ptmp;
          if (!(ptmp = realloc (p, size)))
            goto ealloc_realloc;

          str = mala_string_new_cstr_attach (ptmp, &eng->words);
          if (!str)
            goto ealloc_string;

          if (!mala_stringlist_tail_new (list, str))
            goto ealloc_node;

          mala_string_free (str);
        }


      // insert list
      for (itr = mala_stringlist_tail (list);
           !mala_stringlist_is_end (list, itr);
           mala_stringlistnode_rev (&itr))
        {
          if (!mala_stringlist_after_new (&eng->program, last,
                                          mala_stringlistnode_string (itr)))
            goto ealloc_program;
        }

      // cleanup
      mala_stringlist_free (list);

    }
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, max_arg);
  return MALA_CONTINUE;

 ealloc_program:
 ealloc_node:
  mala_string_free (str);
 ealloc_string:
 ealloc_realloc:
 ealloc_reserve:
  free (p);
 ealloc_cstr:
  mala_stringlist_free (list);
 ealloc_list:
  return MALA_EALLOC;
}

int
mala_defined_parser (MalaEngine eng,
                     MalaStringListNode_ref pptr,
                     void * data)
{
  MalaStringListNode result;
  MalaActionDesc desc;

  (void) data;

  result = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
  if (!result)
    return eng->state;

  MALA_SIDEEFFECT_BEGIN
    {
      desc = (MalaActionDesc) mala_stringlistnode_user_get (result);

      mala_engine_command_done (eng, pptr, 1);

      if (desc && mala_actiondesc_top (desc))
        {
          return MALA_SUCCESS;
        }
      else
        {
          return MALA_FAILURE;
        }
    }
  MALA_SIDEEFFECT_END;

  return MALA_REMOVE;
}

#if 0
int
mala_map_parser (MalaEngine eng,
                 MalaStringListNode_ref pptr,
                 void * data)
{
  MalaStringListNode function;
  MalaStringListNode list_of_lists;

  (void) data;

  result = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
  if (!result)
    return eng->state;

  MALA_SIDEEFFECT_BEGIN
    {
      desc = (MalaActionDesc) mala_stringlistnode_user_get (result);

      mala_engine_command_done (eng, pptr, 1);

      if (desc && mala_actiondesc_top (desc))
        {
          return MALA_SUCCESS;
        }
      else
        {
          return MALA_FAILURE;
        }
    }
  MALA_SIDEEFFECT_END;

  return MALA_REMOVE;
}
#endif

int
mala_if_parser (MalaEngine eng,
                MalaStringListNode_ref pptr,
                void * data)
{
  MalaStringListNode result;
  (void) data;
  /*TODO negated, blocks, parse long double 0.0 as false*/
  /* what with syntax errors like   --IF foo --ELSE ..  (missing 'then' part)*/
  int state;

  result = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
  if (!result)
    return eng->state;

  MALA_SIDEEFFECT_BEGIN
    {
      if (eng->state == MALA_LITERAL)
        {
          if (mala_engine_string_istrue (eng, mala_stringlistnode_string (result)))
            eng->state = MALA_SUCCESS;
          else
            eng->state = MALA_FAILURE;
        }

      state = eng->state;

      if (eng->state == MALA_FAILURE)
          eng->state = MALA_REMOVE;

      result = mala_engine_arg_eval (eng, pptr, 2, -1, NULL, NULL);
      if (!result)
        return eng->state;

      if (eng->state == MALA_REMOVE)
        eng->state = MALA_FAILURE;
    }
  MALA_SIDEEFFECT_END;

  if (eng->state == MALA_LITERAL)
    mala_engine_command_done (eng, pptr, 1);
  else
    mala_engine_command_done (eng, pptr, 2);
  return state;
}

int
mala_else_parser (MalaEngine eng,
                  MalaStringListNode_ref pptr,
                  void * data)
{
  MalaStringListNode result;
  (void) data;
  /*TODO blocks*/

  MALA_SIDEEFFECT_BEGIN
    {
      if (eng->state == MALA_SUCCESS)
          eng->state = MALA_REMOVE;

      result = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
      if (!result)
        return eng->state;

      if (eng->state == MALA_REMOVE)
        eng->state = MALA_SUCCESS;
    }
  MALA_SIDEEFFECT_END;

  if (eng->state == MALA_LITERAL)
    mala_engine_command_done (eng, pptr, 0);
  else
    mala_engine_command_done (eng, pptr, 0);
  return eng->state;
}

int
mala_pass_parser (MalaEngine eng,
                  MalaStringListNode_ref pptr,
                  void * data)
{
  (void) data;

  mala_stringlist_elem_delete_fwd (&eng->program, pptr);

  return eng->state;
}

int
mala_trace_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  (void) data;

  MALA_SIDEEFFECT_BEGIN
    eng->trace = !eng->negated;
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 0);
  return MALA_SUCCESS;
}

/*TODO to predicates*/
static int
mala_predicate_greaterorequal_double (double * a, double * b)
{
  return *a >= *b;
};

int
mala_sleep_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  double duration;
  double zero = 0.0;
  struct timespec req;

  (void) data;

  if (mala_engine_arg_eval_fmt (eng, pptr, 1, "%lf", &duration,
                                (MalaPredicate) mala_predicate_greaterorequal_double, &zero
                                ) >= MALA_EXCEPTION)
    return eng->state;

  MALA_SIDEEFFECT_BEGIN
    {
      req.tv_sec = (time_t) duration;
      req.tv_nsec = (long) 1000000000 * (duration - ((double) req.tv_sec));

      (void) nanosleep (&req, NULL);
    }
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 1);
  return MALA_SUCCESS;
}

int
mala_literal_parser (MalaEngine eng,
                     MalaStringListNode_ref pptr,
                     void * data)
{
  (void) data;

  MALA_SIDEEFFECT_BEGIN
    {
      if (mala_stringlist_is_tail (&eng->program, *pptr))
        return mala_engine_exception (eng, pptr, *pptr,
                                      eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);
    }
  MALA_SIDEEFFECT_END;

  mala_stringlist_elem_delete_fwd (&eng->program, pptr);
  return MALA_LITERAL;
}

int
mala_not_parser (MalaEngine eng,
                 MalaStringListNode_ref pptr,
                 void * data)
{
  (void) data;
  /*TODO evaluate %1*/
  MALA_SIDEEFFECT_BEGIN
    {
      if (mala_stringlist_is_tail (&eng->program, *pptr))
        return mala_engine_exception (eng, pptr, *pptr,
                                      eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);
    } /*TODO remove unfinished*/
  MALA_SIDEEFFECT_END;

  mala_stringlist_elem_delete_fwd (&eng->program, pptr);
  eng->negated = !eng->negated;
  return MALA_SUCCESS; /*TODO*/
}

int
mala_begin_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  MalaStringListNode itr;
  MalaStringListNode end;
  MalaStringList list;
  unsigned depth = 1;
  MalaString name = NULL;

  (void) data;

  if (mala_stringlist_is_tail (&eng->program, *pptr))
    return mala_engine_exception (eng, pptr, *pptr,
                                  eng->common_string[MALA_STRING_ERROR_MISSING_END]);

  // find matching --END
  for (end = mala_stringlistnode_next (*pptr);
       !mala_stringlist_is_end (&eng->program, end);
       mala_stringlistnode_fwd (&end))
    {
      if (mala_string_same(mala_stringlistnode_string (end),
                           eng->common_string[MALA_STRING_BEGIN]))
        ++depth;
      else if (mala_string_same(mala_stringlistnode_string (end),
                                eng->common_string[MALA_STRING_END]))
        --depth;
      if (!depth)
        break;
    }

  if (depth)
    return mala_engine_exception (eng, pptr, mala_stringlistnode_prev (end),
                                  eng->common_string[MALA_STRING_ERROR_MISSING_END]);

  MALA_SIDEEFFECT_BEGIN
    {
      list = mala_stringlist_new ();
      if (!list)
        return MALA_EALLOC;

      // copy the block content to list
      for (itr = mala_stringlistnode_next (*pptr); itr != end; mala_stringlistnode_fwd (&itr))
        if (!mala_stringlist_tail_new (list, mala_stringlistnode_string (itr)))
          goto ealloc_node;

      // allocate new block name
      do {
        mala_string_free (name);
        name = mala_string_new_print (&eng->words, "--BLOCK_%08X", ++eng->blockcnt);
        if (!name)
          goto ealloc_name;
      } while (mala_actiondesc_top ((MalaActionDesc)mala_string_user_get (name)));

      if (MALA_SUCCESS != mala_engine_add_action (eng, name, list,
                                                  mala_block_parser,
                                                  (MalaDataFactory)mala_stringlist_factory,
                                                  NULL))
        goto ealloc_action;

      // insert new --BLOCK_... in program
      if (!mala_stringlist_after_new (&eng->program, end, name))
        goto ealloc_node;
      mala_string_free (name);
    }
  else
    {
      if (!mala_stringlist_after_new (&eng->program,
                                      end,
                                      eng->common_string[MALA_STRING_PASS]))
        goto ealloc_pass;
    }
  MALA_SIDEEFFECT_END;

  // and remove definition
  while (*pptr != end)
    mala_stringlist_elem_delete_fwd (&eng->program, pptr);

  mala_stringlist_elem_delete_fwd (&eng->program, pptr);

  return MALA_CONTINUE;

 ealloc_action:
  mala_string_free (name);
 ealloc_name:
 ealloc_node:
  mala_stringlist_free (list);
 ealloc_pass:
  return MALA_EALLOC;
}

int
mala_block_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  MalaStringListNode itr;
  MalaStringListNode end;

  MALA_SIDEEFFECT_BEGIN
    {
      end = mala_stringlistnode_next (*pptr);

      for (itr = mala_stringlist_tail ((MalaStringList) data);
           !mala_stringlist_is_end ((MalaStringList) data, itr);
           mala_stringlistnode_rev (&itr))
        {
          if (!mala_stringlist_after_new (&eng->program, *pptr,
                                          mala_stringlistnode_string (itr)))
            goto ealloc_node;
        }
    }
  MALA_SIDEEFFECT_END;

  mala_actiondesc_pop_delete (mala_stringlistnode_user_get (*pptr));
  mala_engine_command_done (eng, pptr, 0);
  return MALA_CONTINUE;

 ealloc_node:
  /* remove already added things */
  for (itr = mala_stringlistnode_next (*pptr);
       itr != end;
       mala_stringlist_elem_delete_fwd (&eng->program, &itr));

  return MALA_EALLOC;
}

int
mala_macrodelete_parser (MalaEngine eng,
                         MalaStringListNode_ref pptr,
                         void * data)
{
  (void) data;

  mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);

  MALA_SIDEEFFECT_BEGIN
    {
      if (mala_stringlist_is_tail (&eng->program, *pptr))
        return mala_engine_exception (eng, pptr, *pptr,
                                      eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);

      mala_actiondesc_pop_delete (mala_stringlistnode_user_get (mala_stringlistnode_next (*pptr)));
    }
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 1);
  return MALA_SUCCESS;
}

int
mala_exception_parser (MalaEngine eng,
                       MalaStringListNode_ref pptr,
                       void * data)
{
  // TODO needs better semantics --EXCEPTION n error  -> --ERROR-error 1 .. n --HERE

  (void) data;

  mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);

  //TODO  MALA_SIDEEFFECT_BEGIN
  //  {
  if (mala_stringlist_is_tail (&eng->program, *pptr))
    return mala_engine_exception (eng, pptr, *pptr,
                                  eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);

  if (eng->state > MALA_EFAULT)
    return eng->state;

  MalaString ex = mala_string_new_print (&eng->words, "--ERROR-%s",
                                         mala_stringlistnode_cstr (mala_stringlistnode_next (*pptr)));

  mala_stringlist_elem_delete_fwd (&eng->program, pptr);
  int state = mala_engine_exception (eng, pptr, *pptr, ex);
  mala_stringlist_elem_delete (&eng->program, mala_stringlistnode_next (*pptr));

  mala_string_free (ex);
  //  MALA_SIDEEFFECT_END;

  return state;
}

int
mala_end_parser (MalaEngine eng,
                 MalaStringListNode_ref pptr,
                 void * data)
{
  (void) data;

  return mala_engine_exception (eng, pptr, *pptr,
                                eng->common_string[MALA_STRING_ERROR_END_WITHOUT_BEGIN]);
}

static void
mala_macrocheck_list (MalaStringList list, int * max_arg, int argt[10])
{
  MalaStringListNode itr;
  for (itr = mala_stringlist_head (list);
       !mala_stringlist_is_end (list, itr);
       mala_stringlistnode_fwd (&itr))
    {
      mala_macrocheck_string (mala_stringlistnode_string (itr), max_arg, argt);
      if (*max_arg == -1)
        return;
    }
}

static void
mala_macrocheck_string (MalaString string, int * max_arg, int argt[10])
{
  const char * c;
  for (c = mala_string_cstr (string); *c; ++c)
    {
      // "foo%1%-2"
      if (*c == '%')
        {
          // %
          ++c;
          if (*c == '%')
            // %%
            continue;
          if (*c == '-')
            {
              // %-
              ++c;
              if (*c >= '0' && *c <= '9')
                {
                  // %-0 .. %-9
                  if (argt[*c - '0'] == 1)
                    goto esyntax;
                  if (*c > (char) *max_arg + '0')
                    *max_arg = *c - '0';
                  argt[*c - '0'] = -1;
                }
              else
                goto esyntax;
            }
          else if (*c >= '0' && *c <= '9')
            {
              // %0 .. %9
              if (argt[*c - '0'] == -1)
                goto esyntax;
              if (*c > (char) *max_arg + '0')
                *max_arg = *c - '0';
              argt[*c - '0'] = 1;
            }
          else
            goto esyntax;
        }
    }
  return;

 esyntax:
  *max_arg = -1;
  return;
}

int
mala_macrodef_parser (MalaEngine eng,
                      MalaStringListNode_ref pptr,
                      void * data)
{
  int i;
  MalaStringListNode itr;
  MalaStringListNode arg[2];
  MalaString name;
  MalaAction act;

  (void) data;

  // evaluate both args
  if (!mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL))
    return eng->state;
  if (!mala_engine_arg_eval (eng, pptr, 2, -1, NULL, mala_block_parser))
    return eng->state;

  MALA_SIDEEFFECT_BEGIN
    {
      // test if 2 arguments left and assign them to arg[], else error
      for (i = 0, itr = *pptr; i<2; ++i, mala_stringlistnode_fwd(&itr))
        {
          if (mala_stringlist_is_tail (&eng->program, itr))
            return mala_engine_exception (eng, pptr, itr,
                                          eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);
          arg[i] = mala_stringlistnode_next (itr);
        }

      // if name is a block then error
      name = mala_stringlistnode_string (arg[0]);
      act = mala_actiondesc_top ((MalaActionDesc) mala_string_user_get (name));
      if (act && act->parser == mala_block_parser)
        return mala_engine_exception (eng, pptr, arg[0],
                                      eng->common_string[MALA_STRING_ERROR_BLOCK_NOT_ALLOWED]);

      //expansion check and optimize block
      act = mala_actiondesc_top ((MalaActionDesc) mala_stringlistnode_user_get (arg[1]));
      if (act && act->factory == (MalaDataFactory) mala_stringlist_factory)
        {
          int max_arg = 0;
          int argt[10] = {0};
          mala_macrocheck_list ((MalaStringList) act->data, &max_arg, argt);
          // convert block to expansion
          if (max_arg > 0)
            {
              //macro
              MalaActionDesc desc;
              desc = mala_actiondesc_ensure (name);
              if (!desc)
                return MALA_EALLOC;

              act = mala_actiondesc_pop ((MalaActionDesc) mala_stringlistnode_user_get (arg[1]));
              act->name = name;
              act->parser = mala_macro_parser;
              mala_actiondesc_push_action (desc, act);
            }
          else if (mala_stringlist_is_single ((MalaStringList) act->data))
            {
              //substitute
              MalaString subst;
              subst = mala_stringlist_head_string_copy ((MalaStringList) act->data);

              if (MALA_SUCCESS != mala_engine_add_action (eng, name, subst,
                                                          mala_substitute_parser,
                                                          (MalaDataFactory) mala_string_factory,
                                                          NULL))
                return MALA_EALLOC;

              mala_action_free(act);
            }
          else if (max_arg == 0)
            {
              //expand
              MalaActionDesc desc;
              desc = mala_actiondesc_ensure (name);
              if (!desc)
                return MALA_EALLOC;

              act = mala_actiondesc_pop ((MalaActionDesc) mala_stringlistnode_user_get (arg[1]));
              act->name = name;
              act->parser = mala_expand_parser;
              mala_actiondesc_push_action (desc, act);
            }
          else
            return mala_engine_exception (eng, pptr, arg[1],
                                          eng->common_string[MALA_STRING_ERROR_PARAMETER_SYNTAX]);
        }
      else //if (act && act->factory == (MalaDataFactory)mala_string_factory)
        {
          // single word
          int max_arg = 0;
          int argt[10] = {0};
          mala_macrocheck_string (mala_stringlistnode_string (arg[1]), &max_arg, argt);
          if (max_arg == 0)
            {
              // substitute
              if (MALA_SUCCESS != mala_engine_add_action (eng, name,
                                                          mala_stringlistnode_string_copy (arg[1]),
                                                          mala_substitute_parser,
                                                          (MalaDataFactory) mala_string_factory,
                                                          NULL))
                return MALA_EALLOC;
            }
          else if (max_arg > 0)
            {
              // macro
              MalaStringList list;

              list = mala_stringlist_new ();
              if (!list)
                return MALA_EALLOC;

              mala_stringlist_tail_new (list, mala_stringlistnode_string (arg[1]));

              if (MALA_SUCCESS != mala_engine_add_action (eng, name, list,
                                                          mala_macro_parser,
                                                          (MalaDataFactory) mala_stringlist_factory,
                                                          NULL))
                return MALA_EALLOC;
            }
          else // syntax error
            return mala_engine_exception (eng, pptr, arg[1],
                                          eng->common_string[MALA_STRING_ERROR_PARAMETER_SYNTAX]);
        }
    }
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 2);
  return MALA_SUCCESS;
}

int
mala_foreach_word_parser (MalaEngine eng,
                          MalaStringListNode_ref pptr,
                          void * data)
{
  MalaAction act;
  MalaStringListNode first;
  MalaStringListNode second;
  MalaStringListNode last;
  MalaStringListNode itr;
  (void) data;

  /*TODO bugs, rename to --APPLY fixit etc*/

  first = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
  if (!first)
    return eng->state;
  act = mala_actiondesc_top ((MalaActionDesc) mala_stringlistnode_user_get (first));
  // TODO allow blocks as first arg (define macro and delete it at later)
  if (act && act->parser == mala_block_parser)
    return mala_engine_exception (eng, pptr, first,
                                  eng->common_string[MALA_STRING_ERROR_BLOCK_NOT_ALLOWED]);

  second = mala_engine_arg_eval (eng, pptr, 2, -1, (MalaDataFactory) mala_stringlist_factory, NULL);
  if (!second)
    return eng->state;

  last = mala_stringlistnode_next (second);

  act = mala_actiondesc_top ((MalaActionDesc) mala_stringlistnode_user_get (second));

  // expand second
  if (eng->state != MALA_LITERAL)
    for (itr = mala_stringlist_tail ((MalaStringList) act->data);
         !mala_stringlist_is_end ((MalaStringList) act->data, itr);
         mala_stringlistnode_rev (&itr))
      {
        if (!mala_stringlist_after_new (&eng->program,
                                        second,
                                        mala_stringlistnode_string (itr)))
          goto ealloc_node;
        if (!mala_stringlist_after_new (&eng->program,
                                        second,
                                        mala_stringlistnode_string (first)))
          goto ealloc_node;
      }
  else
    {
      if (!mala_stringlist_after_new (&eng->program,
                                      second,
                                      mala_stringlistnode_string (second)))
        goto ealloc_node;
      if (!mala_stringlist_after_new (&eng->program,
                                      second,
                                      mala_stringlistnode_string (first)))
        goto ealloc_node;
    }


  // was a block? delete it
  if (act && act->parser == mala_block_parser)
    mala_actiondesc_pop_delete (mala_stringlistnode_user_get (second));

  mala_engine_command_done (eng, pptr, 2);
  return MALA_SUCCESS;

 ealloc_node:
  for (itr = mala_stringlistnode_next (second);
       itr != last;
       mala_stringlistnode_fwd (&itr))
    {
      mala_stringlist_elem_delete (&eng->program, itr);
    }

  return MALA_EALLOC; // TODO exception instead (needs pools, no allocation possible further)
}




/*
  realloc a string to at least needed size
  return the amount really reserved or 0 on error
*/
static size_t
reserve_string (char ** s, size_t actual, size_t needed)
{
  size_t n;
  char * r;

  for (n = actual>64?actual:64; n <= needed; n += (n>>1)); /*n = n * 1.5*/

  r = realloc (*s, n);
  if (!r)
    {
      /* that was to much, try conservatively */
      r = realloc (*s, n = needed);
      if (!r)
        return 0;
    }
  *s = r;
  return n;
}


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 195047fd-d18b-4af6-a627-43e5655162ed
// end_of_file
*/
