/*
  std_macros.h - MaLa standard module parser declarations

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_MACROS_H
#define MALA_STD_MACROS_H

#include "std.h"

//MALA_PARSER_DEFINE(foreach_word);
//MALA_PARSER_DEFINE(map);
//MALA_PARSER_DEFINE(length);

MALA_PARSER_DEFINE(substitute);
MALA_PARSER_DEFINE(expand);
MALA_PARSER_DEFINE(macro);
MALA_PARSER_DEFINE(begin);
//MALA_PARSER_DEFINE(block);
MALA_PARSER_DEFINE(delete);
MALA_PARSER_DEFINE(end);
MALA_PARSER_DEFINE(def);

int
mala_module_std_macros_init (MalaEngine self);

#endif /* MALA_STD_MACROS_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: a18bab46-9847-4627-9987-09776cd199e4
// end_of_file
*/
