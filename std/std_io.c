/*
    std_io.c - MaLa standard IO module parsers

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "mala.h"
#include "std_io.h"
#include "std.h"

static
mala_actioninit std_io[] =
  {

    MALA_PARSER_SIMPLE("--PRINT", mala_print_parser),
    //    MALA_PARSER_BRIEF("--PRINT", "prints to stdout"),
//    MALA_PARSER_HELP("--PRINT", ("TODO")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),
    //MALA_PARSER_SIGNATURE_TYPE("--PRINT", ("WORD-OR-BLOCK")),
    //MALA_PARSER_RESULT_TYPE("--PRINT", ("VOID")),

    MALA_PARSER_SIMPLE("--NL", mala_newline_parser),
    //MALA_PARSER_BRIEF("--NL", "prints a newline"),
//    MALA_PARSER_HELP("--NL", ("TODO")),
    //MALA_PARSER_RESULT_USAGE("--NL", ("")),


    MALA_PARSER_MACRO("--PRINTL",("--PRINT", "--LITERAL", "%1", "--NL")),
    //MALA_PARSER_BRIEF("--PRINTL", "prints a textline to stdout"),
//    MALA_PARSER_HELP("--PRINTL", ("TODO")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),
    //MALA_PARSER_SIGNATURE_TYPE("--PRINTL", ("WORD-OR-BLOCK")),
    //MALA_PARSER_RESULT_TYPE("--PRINTL", ("VOID")),


    MALA_PARSER_SIMPLE("--PRINTWRAPED",mala_printwraped_parser),
    //MALA_PARSER_BRIEF("--PRINTWRAPED", "prints formatted text"),
    //TODO MALA_PARSER_HELP("--PRINTWRAPED", ("Wraps some text to fit nicely on the screen. Tabulators become aligned. The last tabulator delimited field of a line will be printed word-wraped and indented.")),
    //TODO MALA_PARSER_SIGNATURE_USAGE("--PRINTWRAPED",
    //                            ("Columns available on the screen",
    //                             "Lines available on the screen",
    //                             "Indent of the block in characters",
    //                             "Tabwidth to be used",
    //                             "Text to be printed")),
//    //MALA_PARSER_RESULT_USAGE("--PRINTWRAPED", ("TODO")),
    //TODO MALA_PARSER_SIGNATURE_TYPE("--PRINTWRAPED",
    //                         ("COLUMNS",
    //                          "LINES",
    //                          "INDENT",
    //                          "TABWIDTH",
    //                          "TEXT")),

    MALA_ACTIONS_END
  };

struct mala_printwraped_context;

mala_state
mala_newline_parser (MalaProgram prg)
{
  MALA_MUTATOR_BEGIN
    putchar('\n');
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 0);
  return MALA_STATEMENT;
}

mala_state
mala_print_parser (MalaProgram prg)
{
  mala_state state = mala_program_eval_arg (prg, 1, MALA_LITERAL, NULL);
  if (state > MALA_START)
    return state;

  TODO("use dst in arg_eval");

  PLANNED("print blocks ?");

  MALA_MUTATOR_BEGIN
    printf("%s",mala_string_cstr (mala_stringlist_string (mala_stringlist_next (mala_program_pptr (prg)))));
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 1);
  return MALA_STATEMENT;
}


static void
mala_estimate_tabwidth (struct mala_printwraped_context* pwc);

static void
mala_insert_space (struct mala_printwraped_context* pwc, int stop);

static void
mala_ltab (struct mala_printwraped_context* pwc);

static void
mala_rtab (struct mala_printwraped_context* pwc);

static void
mala_wrap_words (struct mala_printwraped_context* pwc);

static void
mala_print_wraped (struct mala_printwraped_context* pwc);

static void
mala_put_word (struct mala_printwraped_context* pwc);


struct mala_printwraped_context
{
  // constant values
  int columns;
  int lines;
  int indent;
  int tabwidth;
  const char * text;

  // states
  int pos;
  const char * out_itr;
  const char * parse_itr;
  int tabstops[20];
  int ltab_itr;
  int wrap_indent;
  int wrap_indent_locked;
  const char * lastspace;
};


void
nobug_mala_printwraped_context_dump (const struct mala_printwraped_context* self,
                                     const int depth,
                                     const char* file,
                                     const int line,
                                     const char* func)
{
  if (self && depth)
    {
      DUMP_LOG ("columns %d", self->columns);
      DUMP_LOG ("lines %d", self->lines);
      DUMP_LOG ("indent %d", self->indent);
      DUMP_LOG ("tabwidth %d", self->tabwidth);
      for (int current_tab = 0; current_tab < 19; ++current_tab)
        DUMP_LOG_IF (self->tabstops[current_tab], "tab %d %d", current_tab, self->tabstops[current_tab]);
    }
}


static void
mala_estimate_tabwidth (struct mala_printwraped_context* pwc)
{
  int current_tab;
  int ltab_cnt;

 again:
  TRACE (mala_module_std);
  for (current_tab = 19; current_tab; --current_tab)
    pwc->tabstops[current_tab] = 0;

  pwc->tabstops[0] = pwc->indent;
  current_tab = 0;
  ltab_cnt = 0;

  for (pwc->parse_itr = pwc->text, pwc->pos = pwc->indent;
       *pwc->parse_itr;
       ++pwc->parse_itr)
    {
      switch (*pwc->parse_itr)
        {
        case '\t':
          if (*(pwc->parse_itr+1) != ' ')
            {
              // left bound tab
              if (ltab_cnt < 19)
                {
                  ++ltab_cnt;

                  if (pwc->tabstops[ltab_cnt] < pwc->pos)
                    {
                      pwc->tabstops[ltab_cnt] = pwc->indent +
                        (pwc->pos - 1 + pwc->tabwidth) -
                        ((pwc->pos - 1 + pwc->tabwidth) % pwc->tabwidth);
                    }

                  pwc->pos = pwc->tabstops[ltab_cnt];

                  if (pwc->pos + pwc->tabwidth >= pwc->columns && pwc->tabwidth > 2)
                    {
                      --pwc->tabwidth;
                      TRACE(mala_module_std, "tab decreased to %d",pwc->tabwidth);
                      goto again;
                    }
                }
            }
          break;
        case '\n':
          // end of line
          ltab_cnt = 0;
          pwc->pos = pwc->indent;
          break;
        default:
          // normal character
          ++pwc->pos;
          break;
        }
    }
}

static void
mala_insert_space (struct mala_printwraped_context* pwc, int stop)
{
  while (pwc->pos < stop)
    {
      ++pwc->pos;
      putchar (' ');
    }
}

static void
mala_ltab (struct mala_printwraped_context* pwc)
{
  TRACE (mala_module_std);
  while(*pwc->lastspace == ' ')
    ++pwc->lastspace;
  ++pwc->lastspace;

  if (pwc->ltab_itr < 19)
    {
      ++pwc->ltab_itr;
      pwc->wrap_indent = pwc->tabstops[pwc->ltab_itr] -
        (pwc->parse_itr - pwc->lastspace) - 1;
    }

  pwc->wrap_indent_locked = 0;
  ++pwc->parse_itr;
  ++pwc->out_itr;
}

static void
mala_rtab (struct mala_printwraped_context* pwc)
{
  TRACE (mala_module_std);
  const char * tmp_itr;

  if (pwc->pos < pwc->columns)
    {
      putchar (' ');
      ++pwc->pos;
    }

  ++pwc->parse_itr;
  while (isblank (*pwc->parse_itr))
    ++pwc->parse_itr;

  pwc->lastspace = pwc->parse_itr;
 
  tmp_itr = pwc->parse_itr;
  while (*tmp_itr != '\0' && *tmp_itr != '\n')
    ++tmp_itr;

  int length = tmp_itr - pwc->parse_itr;

  pwc->wrap_indent = pwc->columns - length;
  pwc->wrap_indent_locked = 0;
}

static void
mala_put_word (struct mala_printwraped_context* pwc)
{
  const char* stop;
  int length;
  int left;
  TRACE (mala_module_std);

  do
    {
      stop = pwc->out_itr;
      length = stop - pwc->lastspace;
      left = pwc->columns - pwc->pos;

      if (pwc->pos + length > pwc->columns)
        {
          while(*pwc->lastspace == ' ')
            ++pwc->lastspace;

          if (length > pwc->columns - pwc->wrap_indent)
            {
              stop = pwc->lastspace + pwc->columns - pwc->wrap_indent - 1;
            }

          if (isalpha (*(pwc->lastspace - 1)))
            putchar ('-');

          putchar ('\n');
          pwc->pos = 0;
        }

      if (pwc->pos < pwc->wrap_indent)
        mala_insert_space (pwc, pwc->wrap_indent);

      if (pwc->wrap_indent >= pwc->columns - 1)
        pwc->wrap_indent = pwc->columns - 2;

      while (pwc->lastspace < stop)
        {
          if (isalpha (*pwc->lastspace))
            pwc->wrap_indent_locked = 1;
          else if (!pwc->wrap_indent_locked)
            ++pwc->wrap_indent;

          putchar (*pwc->lastspace);
          ++pwc->pos;
          ++pwc->lastspace;
        }
    }
  while (stop < pwc->out_itr);
}

static void
mala_wrap_words (struct mala_printwraped_context* pwc)
{
  /* prints all text until the next \t \n or \0 linewraped */
  TRACE (mala_module_std);
  for (; pwc->out_itr < pwc->parse_itr; ++pwc->out_itr)
    {
      if (*pwc->out_itr == ' ')
        {
          mala_put_word (pwc);
        }
    }
  mala_put_word (pwc);
}

static void
mala_print_wraped (struct mala_printwraped_context* pwc)
{
  /*
    - a line consists of many tab delimited fields with one optional right aligned field at the
      end introduced with tab-space "\t "
    - the last normal tab field gets word wraped and aligned the first alphabetic character of this
      field. Too long words become (simple) hyphenated.
    - tabs are aligned every 'tabwidth' characters, when the terminal is too small, tabwidth is
      gradually decremented until the data fits more nicely until tabwidth = 2.
    - care is taken that even if the output isn't displayable (too less columns) at least something
      ugly will be presented, never failing.

  Example:
  "foo \tbar\n\n\tfoo\t- foo explanation which gets word wraped because this text is long\n\tbaz\t- baz explanation\t [note]"

  becomes something like:

  foo     bar

          foo     - foo explanation which gets word wraped
                    because this text is long
          baz     - baz explanation                   [note]

  */
  TRACE (mala_module_std);

  for (pwc->lastspace = pwc->out_itr = pwc->parse_itr = pwc->text,
         pwc->pos = 0;
       *pwc->parse_itr;
       ++pwc->parse_itr)
    {
      switch (*pwc->parse_itr)
        {
        case '\t':
          mala_wrap_words (pwc);
          if (*(pwc->parse_itr+1) != ' ')
            mala_ltab (pwc);
          else
            mala_rtab (pwc);
          break;
        case '\n':
          mala_wrap_words (pwc);
          putchar ('\n');
          ++pwc->lastspace;
          pwc->wrap_indent = pwc->indent;
          pwc->wrap_indent_locked = 0;
          pwc->ltab_itr = 0;
          pwc->pos = 0;
          break;
        }
    }

  mala_wrap_words (pwc);
  putchar ('\n');
}


mala_state
mala_printwraped_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  struct mala_printwraped_context pwc;

  mala_state state;

  /* Columns */
  state = mala_program_eval_arg_fmt (prg, 1, "%d", &pwc.columns, NULL);
  if (state > MALA_START)
    return state;
  if (pwc.columns < 2)
    pwc.columns = 2;

  /* Lines _ignored for now_ */
  PLANNED("paged output");
  state = mala_program_eval_arg_fmt (prg, 2, "%d", &pwc.lines, NULL);
  if (state > MALA_START)
    return state;
  if (pwc.lines < 2)
    pwc.lines = 2;

  /* Indent */
  state = mala_program_eval_arg_fmt (prg, 3, "%d", &pwc.indent, NULL);
  if (state > MALA_START)
    return state;
  if (pwc.indent >= pwc.columns - 2)
    /* ugly fallback */
    pwc.indent = 1;

  /* Tabwidth */
  state = mala_program_eval_arg_fmt (prg, 4, "%d", &pwc.tabwidth, NULL);
  if (state > MALA_START)
    return state;
  if (pwc.tabwidth < 2)
    pwc.tabwidth = 2;

  // Text
  MalaStringList textnode;
  state = mala_program_eval_arg (prg, 5, MALA_LITERAL, &textnode);
  if (state > MALA_START)
    return state;

  pwc.text = mala_stringlist_cstr (textnode);

  MALA_MUTATOR_BEGIN
    {
      pwc.ltab_itr = 0;
      pwc.wrap_indent_locked = 0;
      pwc.wrap_indent = pwc.indent;

      mala_estimate_tabwidth (&pwc);
      mala_print_wraped (&pwc);
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 5);
  return MALA_STATEMENT;
}


int
mala_module_std_io_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_io);
}


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 3d2a15ac-145d-42d7-98a5-14dc9b7d2003
// end_of_file
*/
