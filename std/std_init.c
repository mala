/*
    std_init.c - MaLa standard module initialization table

  Copyright (C) 2005, 2006, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>

#include "mala.h"
#include "std.h"

static
mala_actioninit std_actions[] =
  {
    /*
    MALA_PARSER("--", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),


    MALA_PARSER("--CHILDOF", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),

    MALA_PARSER("--FOREACH-WORD", mala_foreach_word_parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),
    */

    //MALA_SUBSTITUTE_PARSER("--MALA-VERSION","MaLa 0.2 ("__DATE__" "__TIME__")"),
    //MALA_PARSER_BRIEF("--MALA-VERSION", "Holds the version"),

    //MALA_SUBSTITUTE_PARSER("--DEFAULT-PARSER","--LITERAL"),
    //MALA_PARSER_BRIEF("--DEFAULT-PARSER", "is used for any word which has no associated action"),
    // MALA_PARSER_HELP("--DEFAULT-PARSER", ("TODO")),
    // MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    // MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    // MALA_PARSER_RESULT_TYPE("--", ("")),
    // MALA_PARSER_RESULT_USAGE("--", ("")),

    //MALA_TRUE_PARSER("--FLAG-NUMSPLIT-EXPANSION"),
    // MALA_PARSER_BRIEF("--FLAG-NUMSPLIT-EXPANSION", "TODO"),
    // MALA_PARSER_HELP("--FLAG-NUMSPLIT-EXPANSION", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-LITERAL-EXPANSION"),
    //MALA_PARSER_BRIEF("--FLAG-LITERAL-EXPANSION",
    //                  "words beginning with a single backquote will be "
    //                  "preprocessed as '--LITERAL' 'theword'"),
    // MALA_PARSER_HELP("--FLAG-LITERAL-EXPANSION", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-EXCLAM-EXPANSION"),
    //MALA_PARSER_BRIEF("--FLAG-EXCLAM-EXPANSION",
    //                  "a single ! will be preprocessed as --NOT"),
    // MALA_PARSER_HELP("--FLAG-EXCLAM-EXPANSION", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-NO-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-ASSIGN-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-ENVVAR-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-CHAR-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-UNDERSCORE-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-SETENV-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-BRACKET-EXPANSION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),

    //MALA_TRUE_PARSER("--FLAG-ASSIGN-CONTRACTION"),
    // MALA_PARSER_BRIEF("--", ""),
    // MALA_PARSER_HELP("--", ("TODO")),



    // TODO remove this
    //MALA_EXPAND_PARSER("TESTLIST",("TEST","LIST")),


    MALA_ACTIONS_END
  };

int
mala_module_std_init (MalaEngine self)
{
  (void)(       // prevent 'warning: value computed is not used'
    mala_engine_actions_register (self, std_actions) == MALA_SUCCESS &&
    mala_module_std_io_init (self) == MALA_SUCCESS &&
    mala_module_std_strings_init (self) == MALA_SUCCESS &&
    mala_module_std_statements_init (self) == MALA_SUCCESS &&
    mala_module_std_pp_init (self) == MALA_SUCCESS &&
    mala_module_std_macros_init (self) == MALA_SUCCESS
    );

  return mala_engine_state_get (self);
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: e6d43941-2533-4f28-863e-2dd3128e58bb
// end_of_file
*/
