/*
  std_pp.h - MaLa standard preprocessor declarations

  Copyright (C) 2007, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_PP_H
#define MALA_STD_PP_H

#include "std.h"

MALA_PARSER_DEFINE(ppbackquote);
MALA_PARSER_DEFINE(ppquoted);
MALA_PARSER_DEFINE(ppdquoted);
MALA_PARSER_DEFINE(ppnumsplit);
MALA_PARSER_DEFINE(ppexclam);
MALA_PARSER_DEFINE(ppno);
MALA_PARSER_DEFINE(ppassign);
MALA_PARSER_DEFINE(ppchar);
MALA_PARSER_DEFINE(ppplus);
MALA_PARSER_DEFINE(ppunderscore);
MALA_PARSER_DEFINE(ppsection);
MALA_PARSER_DEFINE(ppassignconcat);
MALA_PARSER_DEFINE(ppenvsubst);
MALA_PARSER_DEFINE(ppsetenv);
MALA_PARSER_DEFINE(ppbrace);
MALA_PARSER_DEFINE(pplist);
MALA_PARSER_DEFINE(ppcommand);
MALA_PARSER_DEFINE(pptokenize);
MALA_PARSER_DEFINE(ppdashdash);

int
mala_module_std_pp_init (MalaEngine self);

#endif /* MALA_STD_PP_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
*/
