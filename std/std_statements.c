/*
  std_statements.c - MaLa statement like parsers

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mala.h"
#include "std_statements.h"


static
mala_actioninit std_statements[] =
  {
    MALA_PARSER_SIMPLE("--LITERAL", mala_literal_parser),
    //MALA_PARSER_BRIEF("--LITERAL", "Treats the next word literally"),
//    MALA_PARSER_HELP("--LITERAL", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),


    MALA_PARSER_SIMPLE("--SKIP", mala_skip_parser),
    //MALA_PARSER_BRIEF("--SKIP", "Treats the next n words literally"),
//    MALA_PARSER_HELP("--LITERAL", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),


    MALA_PARSER_SIMPLE("--PASS", mala_pass_parser),
    //MALA_PARSER_BRIEF("--PASS","does nothing"),
    //TODO MALA_PARSER_HELP("--PASS",("--HELP-ACTION","blah")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),

    MALA_PARSER_SIMPLE("--GC", mala_gc_parser),

    MALA_PARSER_SIMPLE("--REMOVE", mala_remove_parser),

    MALA_PARSER_SIMPLE("--NO", mala_no_parser),

    MALA_PARSER_SIMPLE("--NONO", mala_nono_parser),

    MALA_PARSER_SIMPLE("--IF-ELSE", mala_ifelse_parser),

    MALA_PARSER_MACRO("--IF", ("--IF-ELSE", "%1", "%2", "--BEGIN", "--PASS", "--END")),

    MALA_PARSER_SIMPLE("--STOP", mala_stop_parser),

    MALA_PARSER_SIMPLE("--EVAL", mala_eval_parser),

    MALA_PARSER_SIMPLE("--TRY", mala_try_parser),

    MALA_PARSER_SIMPLE("--REPLACE", mala_replace_parser),


    /*
    MALA_PARSER("--", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
//    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),

    MALA_PARSER("--CHILDOF", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
//    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_TYPE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),
    */



    //MALA_PARSER("--EXCEPTION", mala_exception_parser, NULL, NULL, NULL),
    //MALA_PARSER_BRIEF("--EXCEPTION", "defines a new macro"),
//    MALA_PARSER_HELP("--EXCEPTION", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),

//TODO    MALA_EXPAND_PARSER("--IFDEF", ("--IF", "--DEFINED")),
    //MALA_PARSER_BRIEF("--IFDEF", "tests if a word is defined as macro"),
//    MALA_PARSER_HELP("--NOT", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),

    //MALA_PARSER("--DEFINED", mala_defined_parser, NULL, NULL, NULL),
    //MALA_PARSER_BRIEF("--DEFINED", "tests if a word is defined as macro"),
//    MALA_PARSER_HELP("--NOT", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),

    //MALA_PARSER_BRIEF("--IF", "executes some code if a condition succeeded"),
//    MALA_PARSER_HELP("--NOT", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),


    //MALA_PARSER("--NOT", mala_not_parser, NULL, NULL, NULL),
    //MALA_PARSER_BRIEF("--NOT", "negates logic"),
//    MALA_PARSER_HELP("--NOT", ("TODO")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),



    MALA_PARSER_SIMPLE("--SLEEP",mala_sleep_parser),
    //MALA_PARSER_BRIEF("--SLEEP", "delays execution for some time"),
    //TODO MALA_PARSER_HELP("--SLEEP", ("The program will be suspended for some time, the time might be specified as fraction of seconds\t example: 9.5")),
    //TODO MALA_PARSER_SIGNATURE_USAGE("--SLEEP",("Seconds to sleep")),
//    //MALA_PARSER_RESULT_USAGE("--PRINTWRAPED", ("TODO")),
    //MALA_PARSER_SIGNATURE_TYPE("--SLEEP", ("DURATION")),

    //MALA_PARSER("--TRACE",mala_trace_parser, NULL, NULL, NULL),
    //MALA_PARSER_BRIEF("--TRACE", "controls the trace mode of the MaLa VM"),
    //MALA_PARSER_SIGNATURE_USAGE("--SLEEP",("Seconds to sleep")),
//    //MALA_PARSER_RESULT_USAGE("--PRINTWRAPED", ("TODO")),
    //MALA_PARSER_SIGNATURE_TYPE("--SLEEP", ("DURATION")),

    //MALA_PARSER("--GCTRACE",mala_gctrace_parser, NULL, NULL, NULL),
    //MALA_PARSER_BRIEF("--GCTRACE", "controls the trace mode of the MaLa garbagge collector"),
    //MALA_PARSER_SIGNATURE_USAGE("--SLEEP",("Seconds to sleep")),
//    //MALA_PARSER_RESULT_USAGE("--PRINTWRAPED", ("TODO")),
    //MALA_PARSER_SIGNATURE_TYPE("--SLEEP", ("DURATION")),

    //    MALA_PARSER("--EVAL",mala_eval_parser, NULL, NULL, NULL),
    //    MALA_PARSER_BRIEF("--EVAL",""),
    //    MALA_PARSER_HELP("--EVAL",("blah")),
//    MALA_PARSER_SIGNATURE_TYPE("--", ("")),
//    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
//    MALA_PARSER_RESULT_TYPE("--", ("")),
//    MALA_PARSER_RESULT_USAGE("--", ("")),


    MALA_ACTIONS_END
  };


mala_state
mala_literal_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  if (!mala_program_exists_arg (prg, 1))
    return mala_program_commonexception (prg, MALA_STRING_ERROR_MISSING_ARGUMENT, mala_program_pptr (prg));

  mala_stringlist_remove (mala_program_pptr (prg));
  return MALA_LITERAL;
}


mala_state
mala_stop_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  mala_stringlist_remove (mala_program_pptr (prg));
  return MALA_SUCCESS;
}


mala_state
mala_skip_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MalaStringList node;
  int n;

  mala_state state = mala_program_eval_arg_fmt (prg, 1, "%d", &n, &node);
  if (state > MALA_START)
    return state;
  if (n < 0)
    return mala_program_commonexception (prg, MALA_STRING_ERROR_OUT_OF_RANGE, node);

  MalaStringList pptr;
  if (n)
    {
      pptr = (MalaStringList) llist_get_nth_stop (&prg->pptr->node, n+2,
                                                  &prg->program->node);
      if (!pptr)
        return mala_program_commonexception (prg, MALA_STRING_ERROR_MISSING_ARGUMENT, prg->program);
    }

  mala_program_action_done (prg, 1);
  if (n)
    prg->pptr = pptr;
  return MALA_STATEMENT;
}


mala_state
mala_pass_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  mala_stringlist_remove (mala_program_pptr (prg));
  return MALA_STATEMENT;
}


mala_state
mala_gc_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  acogc_root_collect (&prg->engine->gcroot, ACOGC_COLLECT_NORMAL);
  mala_program_action_done (prg, 0);
  return MALA_STATEMENT;
}


mala_state
mala_remove_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  mala_state state = mala_program_eval_arg (prg, 1, MALA_REMOVE, NULL);
  if (state > MALA_START)
    return state;
  
  mala_program_action_done (prg, 0);
  return MALA_STATEMENT;
}


mala_state
mala_no_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  MALA_MUTATOR_BEGIN
    {
      ++prg->negated;
    }
  MALA_MUTATOR_END;
  
  mala_program_action_done (prg, 0);
  return MALA_STATEMENT;
}


mala_state
mala_nono_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  MALA_MUTATOR_BEGIN
    {
      prg->negated = 0;
    }
  MALA_MUTATOR_END;
  
  mala_program_action_done (prg, 0);
  return MALA_STATEMENT;
}


mala_state
mala_ifelse_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MalaStringList test;

  mala_state state = mala_program_eval_arg (prg, 1, MALA_LITERAL, &test);

  if (state > MALA_START)
    return state;

  if (state == MALA_LITERAL &&
      mala_stringlist_string (test) == prg->engine->common_string[MALA_STRING_TRUE])
    {
      MalaStringList code;

      mala_program_eval_arg (prg, 2, MALA_BLOCK, &code);
      mala_program_eval_arg (prg, 3, MALA_REMOVE, NULL);
      mala_stringlist_advance (code);
    }
  else if (state == MALA_LITERAL &&
           mala_stringlist_string (test) == prg->engine->common_string[MALA_STRING_FALSE])
    {
      mala_program_eval_arg (prg, 2, MALA_REMOVE, NULL);
    }
  else if (prg->state == MALA_REMOVE)
    {
      mala_program_eval_arg (prg, 2, MALA_BLOCK, NULL);
      mala_program_eval_arg (prg, 3, MALA_BLOCK, NULL);

      mala_program_action_done (prg, 3);
      return MALA_STATEMENT;
    }
  else
    return mala_program_commonexception (prg, MALA_STRING_ERROR_NOT_A_PREDICATE, mala_program_pptr (prg));

  mala_program_action_done (prg, 2);
  return MALA_STATEMENT;
}


mala_state
mala_sleep_parser (MalaProgram prg)
{
  double duration;
  struct timespec req;

  mala_state state = mala_program_eval_arg_fmt (prg, 1, "%lf", &duration, NULL);
  if (state > MALA_START)
    return state;

  MALA_MUTATOR_BEGIN
    {
      req.tv_sec = (time_t) duration;
      req.tv_nsec = (long) 1000000000 * (duration - ((double) req.tv_sec));

      (void) nanosleep (&req, NULL);
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 1);
  return MALA_STATEMENT;
}


mala_state
mala_throw_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  (void) prg;
  UNIMPLEMENTED("check this");
  return MALA_EXCEPTION;
}


mala_state
mala_eval_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  mala_state state = mala_program_eval_arg (prg, 1, MALA_SUCCESS, NULL);
  if (state > MALA_START)
    return state;

  mala_program_action_done (prg, 0);
  return MALA_START;
}


mala_state
mala_try_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  mala_state state = mala_program_eval_arg (prg, 1, MALA_LITERAL, NULL);
  TODO ("state >EFAULT");

  MALA_MUTATOR_BEGIN
    {
      if (state == MALA_EXCEPTION)
        {
          ACOGC_STACK_ENTER (&prg->engine->gcroot);
          ACOGC_STACK_PTR(MalaString, s);
          ACOGC_STACK_PTR(MalaStringList, n);

          REQUIRE (prg->exception);

          MalaStringList pptr = mala_program_pptr (prg);

          s.ptr = mala_string_new_print (prg->engine->words, "--CATCH-%s", mala_stringlist_cstr (prg->exception));

          if (mala_string_action (s.ptr))
            {
              n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_STOP], prg->engine);
              mala_stringlist_insert_after (pptr, n.ptr);

              n.ptr = mala_stringlist_node_new (s.ptr, prg->engine);
              mala_stringlist_insert_after (pptr, n.ptr);

              n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_EVAL], prg->engine);
              mala_stringlist_insert_after (pptr, n.ptr);
              prg->exception = NULL;
              state = MALA_START;
            }
          else
            {
              n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_UNHANDLED_ERROR], prg->engine);
              mala_stringlist_insert_after (pptr, n.ptr);

              state = MALA_ENOACTION;
            }

          ACOGC_STACK_LEAVE;
        }
    }
  MALA_MUTATOR_END;
  
  mala_stringlist_remove (mala_program_pptr (prg));
  return state;
}


mala_state
mala_replace_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  mala_state state;
  MalaStringList search;
  MalaStringList replace;

  state = mala_program_eval_arg (prg, 1, MALA_LITERAL, &search);
  if (state > MALA_START)
    return state;
  
  state = mala_program_eval_arg (prg, 2, MALA_LITERAL, &replace);
  if (state > MALA_START)
    return state;

  MALA_MUTATOR_BEGIN
    {
      MalaStringList itr;
      for (itr = mala_stringlist_next (replace);
           itr != prg->program && mala_stringlist_string (itr) != mala_stringlist_string (search);
           mala_stringlist_fwd (&itr));

      if (itr != prg->program)
          mala_stringlist_string_set (itr, mala_stringlist_string (replace));
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 2);
  return MALA_STATEMENT;
}


#if 0
int
mala_defined_parser (MalaEngine eng,
                     MalaStringListNode_ref pptr,
                     void * data)
{
  MalaStringListNode result;

  (void) data;

  result = mala_engine_arg_eval (eng, pptr, 1, MALA_LITERAL);
  if (!result)
    return eng->state;


  mala_engine_command_done (eng, pptr, 1);

  //  if (desc && 0 /*TODO mala_actiondesc_top (desc)*/)
  //  return MALA_SUCCESS;
  //else
    return MALA_FAILURE;
}


int
mala_trace_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  (void) data;

  /*TODO trace levels*/
  MALA_SIDEEFFECT_BEGIN
    eng->engine_trace = eng->negated ? MALA_NOTRACE:MALA_TRACE;
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 0);
  return MALA_CONTINUE;
}

#if 0
int
mala_gctrace_parser (MalaEngine eng,
                   MalaStringListNode_ref pptr,
                   void * data)
{
  (void) data;

  /*TODO trace levels*/
  MALA_SIDEEFFECT_BEGIN
    *eng->gc_trace = eng->negated ? MALA_NOTRACE:MALA_TRACE;
  MALA_SIDEEFFECT_END;

  mala_engine_command_done (eng, pptr, 0);
  return MALA_CONTINUE;
}
#endif

/*TODO to predicates*/
static int
mala_predicate_greaterorequal_double (double * a, double * b)
{
  return *a >= *b;
};


int
mala_not_parser (MalaEngine eng,
                 MalaStringListNode_ref pptr,
                 void * data)
{
  MalaStringListNode result;

  (void) data;

  MALA_SIDEEFFECT_BEGIN
      eng->negated = !eng->negated;
  MALA_SIDEEFFECT_END;

  result = mala_engine_arg_eval (eng, pptr, 1, MALA_LITERAL);
  if (!result)
    return eng->state;

  mala_engine_command_done (eng, pptr, 1);
  return eng->state;
}

int
mala_exception_parser (MalaEngine eng,
                       MalaStringListNode_ref pptr,
                       void * data)
{
  // TODO needs better semantics --EXCEPTION n error  -> --ERROR-error 1 .. n --HERE

  (void) data;

  mala_engine_arg_eval (eng, pptr, 1, MALA_LITERAL);

  //TODO  MALA_SIDEEFFECT_BEGIN
  //  {
#if 0 // TODO --PROGRAM
  if (mala_stringlist_is_tail (&eng->program, *pptr))
    return mala_engine_exception (eng, pptr, *pptr,
                                  eng->common_string[MALA_STRING_ERROR_MISSING_ARGUMENT]);
#endif
  if (eng->state > MALA_EFAULT)
    return eng->state;

  MalaString ex = mala_string_new_print (&eng->words, NULL, //TODO refs
                                         "--ERROR-%s",
                                         mala_stringlistnode_cstr (mala_stringlistnode_next (*pptr)));

  // TODO  mala_stringlist_elem_delete_fwd (&eng->program, pptr);
  int state = mala_engine_exception (eng, pptr, *pptr, ex);
  // TODO mala_stringlist_elem_delete (&eng->program, mala_stringlistnode_next (*pptr));

  // TODO review mala_string_free (ex);
  //  MALA_SIDEEFFECT_END;

  return state;
}

#if 0
int
mala_foreach_word_parser (MalaEngine eng,
                          MalaStringListNode_ref pptr,
                          void * data)
{
  MalaAction act;
  MalaStringListNode first;
  MalaStringListNode second;
  MalaStringListNode last;
  MalaStringListNode itr;
  (void) data;

  /*TODO bugs, rename to --APPLY fixit etc*/

  first = mala_engine_arg_eval (eng, pptr, 1, -1, NULL, NULL);
  if (!first)
    return eng->state;
  act = mala_actiondesc_top ((MalaActionDesc) mala_stringlistnode_user_get (first));
  // TODO allow blocks as first arg (define macro and delete it at later)
  if (act && act->parser == mala_block_parser)
    return mala_engine_exception (eng, pptr, first,
                                  eng->common_string[MALA_STRING_ERROR_BLOCK_NOT_ALLOWED]);

  second = mala_engine_arg_eval (eng, pptr, 2, -1, (MalaDataFactory) mala_stringlist_factory, NULL);
  if (!second)
    return eng->state;

  last = mala_stringlistnode_next (second);

  act = mala_actiondesc_top ((MalaActionDesc) mala_stringlistnode_user_get (second));

  // expand second
  if (eng->state != MALA_LITERAL)
    for (itr = mala_stringlist_tail ((MalaStringList) act->data);
         !mala_stringlist_is_end ((MalaStringList) act->data, itr);
         mala_stringlistnode_rev (&itr))
      {
#if 0 // TODO --PROGRAM
        if (!mala_stringlist_after_new (&eng->program,
                                        second,
                                        mala_stringlistnode_string (itr)))
          goto ealloc_node;
        if (!mala_stringlist_after_new (&eng->program,
                                        second,
                                        mala_stringlistnode_string (first)))
          goto ealloc_node;
#endif
      }
  else
    {
#if 0
      if (!mala_stringlist_after_new (&eng->program,
                                      second,
                                      mala_stringlistnode_string (second)))
        goto ealloc_node;
      if (!mala_stringlist_after_new (&eng->program,
                                      second,
                                      mala_stringlistnode_string (first)))
        goto ealloc_node;
#endif
    }


  // was a block? delete it
  if (act && act->parser == mala_block_parser)
    mala_actiondesc_pop_delete (mala_stringlistnode_user_get (second));

  mala_engine_command_done (eng, pptr, 2);
  return MALA_SUCCESS;

 ealloc_node:
  for (itr = mala_stringlistnode_next (second);
       itr != last;
       mala_stringlistnode_fwd (&itr))
    {
      //      mala_stringlist_elem_delete (&eng->program, itr);
    }

  return MALA_EALLOC; // TODO exception instead (needs pools, no allocation possible further)
}




/*
  realloc a string to at least needed size
  return the amount really reserved or 0 on error
*/
static size_t
reserve_string (char ** s, size_t actual, size_t needed)
{
  size_t n;
  char * r;

  for (n = actual>64?actual:64; n <= needed; n += (n>>1)); /*n = n * 1.5*/

  r = realloc (*s, n);
  if (!r)
    {
      /* that was to much, try conservatively */
      r = realloc (*s, n = needed);
      if (!r)
        return 0;
    }
  *s = r;
  return n;
}
#endif


#endif


int
mala_module_std_statements_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_statements);
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 7e2dc784-1527-458a-8881-e87d3fb22cef
// end_of_file
*/
