/*
    std_pp.c - MaLa standard preprocessor

  Copyright (C) 2007, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <ctype.h>
#include "mala.h"
#include "std_pp.h"

static
mala_actioninit std_pp[] =
  {

    MALA_PARSER_SUBSTITUTE("--PREPROCESS", "--PREPROCESS-ARGV"),

    MALA_PARSER_EXPAND("--PREPROCESS-ARGV",
                       ("--DEF", "--LITERAL", "--ERROR-MISSING-ARGUMENT",
                        "--BEGIN","--DEL", "--LITERAL", "--ERROR-MISSING-ARGUMENT", "--END",
                        "--PP-ARGV")),

    MALA_PARSER_SUBSTITUTE("--PP-ARGV", "--PP-BACKQUOTE-ARGV"),

    MALA_PARSER_MACRO("--PP-ARGV-RESTART",("--LITERAL","%^1", "--PP-ARGV")),

    MALA_PARSER_SIMPLE("--PP-BACKQUOTE-ARGV", mala_ppbackquote_parser),
    MALA_PARSER_SUBSTITUTE("--PP-BACKQUOTE-ARGV-START", "--PP-ARGV"),
    MALA_PARSER_SUBSTITUTE("--PP-BACKQUOTE-ARGV-CONTINUE", "--PP-NUMSPLIT-ARGV"),

    MALA_PARSER_SIMPLE("--PP-NUMSPLIT-ARGV", mala_ppnumsplit_parser),
    MALA_PARSER_SUBSTITUTE("--PP-NUMSPLIT-ARGV-CONTINUE", "--PP-EXCLAM-ARGV"),
    //MALA_PARSER_SUBSTITUTE("--PP-NUMSPLIT-ARGV-CONTINUE", "--PP-EXCLAM-ARGV"), // TODO state to bypass EXLAM (optimization)

    MALA_PARSER_SIMPLE("--PP-EXCLAM-ARGV", mala_ppexclam_parser),
    MALA_PARSER_SUBSTITUTE("--PP-EXCLAM-ARGV-START", "--PP-ARGV-RESTART"),
    MALA_PARSER_SUBSTITUTE("--PP-EXCLAM-ARGV-CONTINUE", "--PP-ARGV-RESTART"),


    /*
    MALA_PARSER_SIMPLE("--PP-QUOTED", mala_ppquoted_parser),
    MALA_PARSER_SIMPLE("--PP-DQUOTED", mala_ppdquoted_parser),
    MALA_PARSER_SIMPLE("--PP-NUMSPLIT", mala_ppnumsplit_parser),
    MALA_PARSER_SIMPLE("--PP-EXCLAM", mala_ppexclam_parser),
    MALA_PARSER_SIMPLE("--PP-NO", mala_ppno_parser),
    MALA_PARSER_SIMPLE("--PP-ASSIGN", mala_ppassign_parser),
    MALA_PARSER_SIMPLE("--PP-CHAR", mala_ppchar_parser),
    MALA_PARSER_SIMPLE("--PP-PLUS", mala_ppplus_parser),
    MALA_PARSER_SIMPLE("--PP_UNDERSCORE", mala_ppunderscore_parser),
    MALA_PARSER_SIMPLE("--PP-SECTION", mala_ppsection_parser),
    MALA_PARSER_SIMPLE("--PP-ASSIGNCONCAT", mala_ppassignconcat_parser),
    MALA_PARSER_SIMPLE("--PP-ENVSUBST", mala_ppenvsubst_parser),
    MALA_PARSER_SIMPLE("--PP-SETENV", mala_ppsetenv_parser),
    MALA_PARSER_SIMPLE("--PP-BRACE", mala_ppbrace_parser),
    MALA_PARSER_SIMPLE("--PP-SIMPLE", mala_pplist_parser),
    MALA_PARSER_SIMPLE("--PP-COMMAND", mala_ppcommand_parser),
    MALA_PARSER_SIMPLE("--PP-TOKENIZE", mala_pptokenize_parser),
    MALA_PARSER_SIMPLE("--PP-DASHDASH", mala_ppdashdash_parser),
    */
    MALA_ACTIONS_END
  };


/*

HOWTO
envsubst in literal?

note: want a --PP-STOP?

 --PREPROCESS           should be defined to the proper preprocessor

 --PREPROCESS-ARGV-MINIMAL


 --PREPROCESS-ARGV      preprocessor for commandline options
   --PP-BACKQUOTE --PP-NUMSPLIT --PP-EXCLAM --PP-NO --PP-ASSIGN --PP-CHAR --PP-PLUS --PP-UNDERSCORE --PP-SECTION --PP-ASSIGN-CONCAT --PP-ENVSUBST --PP-SETENV

 --PREPROCESS-CONF      preprocessor for config files
   --PP-QUOTED --PP-DQUOTED --PP-NUMSPLIT --PP-EXCLAM --PP-NO --PP-ASSIGN --PP-CHAR --PP-PLUS --PP-UNDERSCORE --PP-SECTION --PP-ASSIGN-CONCAT --PP-ENVSUBST --PP-SETENV --PP-BRACE --PP-LIST --PP-COMMAND

 --PREPROCESS-CMD       preprocessor for commandline
   --PP-TOKENINZE --PP-LITERAL --PP-NUMSPLIT --PP-EXCLAM --PP-NO --PP-ASSIGN --PP-CHAR --PP-PLUS --PP-UNDERSCORE --PP-SECTION --PP-ASSIGN-CONCAT --PP-ENVSUBST --PP-SETENV --PP-BRACE --PP-LIST --PP-COMMAND

*/


/*
Preprocessing applies different parsers to works which might in turn modify them. When finished
with a word it may iterate to the next word and restart the preprocessing.

This is done as state-machine where each parser uses
 %0-NEXT to restart on the next word
or
 %0-CONTINUE to


parser                          1)              2)
--PP-BACKQUOTE-ARG              --PP-ARGV       --PP-NUMSPLIT-ARGV
--PP-NUMSPLIT-ARGV              --PP-ARGV       --PP-EXCLAM-ARGV
--PP-EXCLAM-ARGV                --PP-ARGV       --PP-NO-ARGV
--PP-NO-ARGV                    --PP-ARGV       --PP-ASSIGN-ARGV
--PP-ASSIGN-ARGV                --PP-ARGV       --PP-CHAR-ARGV
--PP-CHAR-ARGV                  --PP-ARGV       --PP-PLUS-ARGV
--PP-PLUS-ARGV                  --PP-ARGV       --PP-UNDERSCORE-ARGV
--PP-UNDERSCORE-ARGV            --PP-ARGV

*/


/*
--PP-BACKQUOTE
      literal_expansion `foo becomes --LITERAL foo, where ` is the default of --BACKQUOTE-EXPANSION-CHAR
      backquote_expansion ``foo becomes `foo without literal expansion (escaping of literal expansion)
*/
mala_state
mala_ppbackquote_parser (MalaProgram prg)
{
  int args = 0;
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
      MalaStringList str = mala_stringlist_next(mala_stringlist_next(prg->pptr));

      if (str != prg->program)
        {
          ACOGC_STACK_ENTER (&prg->engine->gcroot);
          ACOGC_STACK_PTR(MalaString, s);
          ACOGC_STACK_PTR(MalaStringList, n);

          if (mala_stringlist_cstr (str)[0] == '`' && mala_stringlist_cstr (str)[1])
            {
              if (mala_stringlist_cstr (str)[1] != '`')
                {
                  s.ptr = mala_string_new_print (prg->engine->words, "%s-START",mala_program_pptr_cstr(prg));
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  n.ptr = mala_stringlist_node_new_cstr (mala_stringlist_cstr (str) + 1, prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_LITERAL], prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  s.ptr = mala_string_new_literal_cstr ("2", prg->engine->words);
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  s.ptr = mala_string_new_literal_cstr ("--SKIP", prg->engine->words);
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  args = 1;
                }
              else
                {
                  n.ptr = mala_stringlist_node_new_cstr (mala_stringlist_cstr (str) + 1, prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  args = 1;
                }
            }
          else
            {
              s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
              n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
              mala_stringlist_insert_after (mala_stringlist_next(prg->pptr), n.ptr);
              args = 0;
            }

          ACOGC_STACK_LEAVE;
        }
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, args);
  return MALA_STATEMENT;
}


/*
--PP-NUMSPLIT
      numsplit_expansion -a10 becomes -a 10
*/
mala_state
mala_ppnumsplit_parser (MalaProgram prg)
{
  int args = 0;
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
      MalaStringList str = mala_stringlist_next(mala_stringlist_next(prg->pptr));
      if (str != prg->program)
        {
          ACOGC_STACK_ENTER (&prg->engine->gcroot);
          ACOGC_STACK_PTR(MalaString, s);
          ACOGC_STACK_PTR(MalaStringList, n);
          /* starts with -[^-0-9] */
          const char* begin = mala_stringlist_cstr (str);

          if (begin[0] == '-' && begin[1] && begin[1] != '-' && !isdigit(begin[1]))
            {
              const char* i; 
              for (i = begin + 2;
                   *i && !isdigit(*i);
                   ++i);

              if (*i)
                {
                  size_t len1 = i - begin;

                  s.ptr = mala_string_new_cstr_n (begin + len1,
                                                  mala_string_length (mala_stringlist_string (str)) - len1,
                                                  prg->engine->words);
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  s.ptr = mala_string_new_cstr_n (begin,
                                                  len1,
                                                  prg->engine->words);
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
                  n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
                  mala_stringlist_insert_after (str, n.ptr);

                  args = 1;
                }
            }
          else
            {
              s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
              n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
              mala_stringlist_insert_before (str, n.ptr);
              args = 0;
            }
          ACOGC_STACK_LEAVE;
        }
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, args);
  return MALA_STATEMENT;
}


/*
--PP-EXCLAM
      exclam_expansion ! becomes --NO
*/
mala_state
mala_ppexclam_parser (MalaProgram prg)
{
  int args = 0;
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
      MalaStringList str = mala_stringlist_next(mala_stringlist_next(prg->pptr));
      if (str != prg->program)
        {
          ACOGC_STACK_ENTER (&prg->engine->gcroot);
          ACOGC_STACK_PTR(MalaString, s);
          ACOGC_STACK_PTR(MalaStringList, n);

          if (mala_stringlist_string(str) == prg->engine->common_string[MALA_STRING_EXCLAMATIONMARK])
            {
              s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
              n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
              mala_stringlist_insert_after (str, n.ptr);

              n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_NO], prg->engine);
              mala_stringlist_insert_after (str, n.ptr);

              n.ptr = mala_stringlist_node_new (prg->engine->common_string[MALA_STRING_LITERAL], prg->engine);
              mala_stringlist_insert_after (str, n.ptr);
              args = 1;
            }
          else
            {
              s.ptr = mala_string_new_print (prg->engine->words, "%s-CONTINUE",mala_program_pptr_cstr(prg));
              n.ptr = mala_stringlist_node_new (s.ptr , prg->engine);
              mala_stringlist_insert_before (str, n.ptr);
              args = 0;
            }
          ACOGC_STACK_LEAVE;
        }
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, args);
  return MALA_STATEMENT;
}











/*
--PP-QUOTED
      literal_expansion 'foo' becomes --LITERAL foo, where '..' are the default of --QUOTED-EXPANSION-CHAR
      backquote_expansion ``foo becomes `foo without literal expansion (escaping of literal expansion)
*/
mala_state
mala_ppquoted_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  mala_program_action_done (prg, 0);
  return MALA_SUCCESS;
}


/*
--PP-DQUOTED
      literal_expansion "foo" becomes --LITERAL foo, where ".." are the default of --DQUOTED-EXPANSION-CHAR
      backquote_expansion ``foo becomes `foo without literal expansion (escaping of literal expansion)
*/
mala_state
mala_ppdquoted_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-NO
      no_expansion --NO-foo or --no-foo becomes --NO --foo
*/
mala_state
mala_ppno_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-ASSIGN
      assign_expansion --foo=bar becomes --foo bar
*/
mala_state
mala_ppassign_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-CHAR
      char_expansion -abc+def123 becomes -a -b -c +d +e +f 123
*/
mala_state
mala_ppchar_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-PLUS
      plus_expansion +a becomes --NO -a
*/
mala_state
mala_ppplus_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP_UNDERSCORE
      underscore_expansion --foo_bar becomes --foo-bar
*/
mala_state
mala_ppunderscore_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-SECTION
      bracket_expansion [foo bar] becomes --SECTION foo bar
*/
mala_state
mala_ppsection_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-ASSIGN-CONCAT
      assign_concat foo = bar becomes foo=bar for setenv_expansion
*/
mala_state
mala_ppassignconcat_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-ENVSUBST
      envvar_expansion foo$bar or foo${bar} becomes --ENVSUBST foo${bar}

  wordexp()
*/
mala_state
mala_ppenvsubst_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-SETENV
      setenv_expansion foo=bar becomes --SETENV foo bar
*/
mala_state
mala_ppsetenv_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-BRACE
      brace_expansion { becomes --BEGIN and } becomes --END where { is the default value of --OPENING-BRACE and } is the default value of --CLOSING-BRACE
*/
mala_state
mala_ppbrace_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-LIST
      list_expansion foo,bar,baz becomes --BEGIN foo bar baz --END where , is the default value of --LIST-SEPARATOR
*/

mala_state
mala_pplist_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-COMMAND
      comand_expansion foo becomes --foo when --foo is a defined action
*/
mala_state
mala_ppcommand_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-TOKENIZE
      tokenizes a string with respect of quotes
*/
mala_state
mala_pptokenize_parser (MalaProgram prg)
{
  TRACE (mala_module_std);

  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}


/*
--PP-DASHDASH
      quits preprocessor on --
*/
mala_state
mala_ppdashdash_parser (MalaProgram prg)
{
  TRACE (mala_module_std);
  //--PP-DASHDASH for '--' doesnt add --PP-MARK, returns MALA_SUCCESS (or something other which breaks preprocessing instantly)
  MALA_MUTATOR_BEGIN
    {
    }
  MALA_MUTATOR_END;

  return MALA_STATEMENT;
}



int
mala_module_std_pp_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_pp);
}


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
*/
