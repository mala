/*
  std.h - MaLa standard parser module

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_H
#define MALA_STD_H

// TODO dont include here
#include "std_macros.h"
#include "std_statements.h"
#include "std_io.h"
#include "std_strings.h"
#include "std_pp.h"

#define MALA_PARSER(n,p,t,d,m) {n, (MalaParserFunc)p, t, d, m}

#define MALA_PARSER_SIMPLE(name, parser) \
  MALA_PARSER(name, parser, MALA_DATA_UNUSED, NULL, "--WORLD")
#define MALA_PARSER_SIMPLE_P(name, parser, parent) \
  MALA_PARSER(name, parser, MALA_DATA_UNUSED, NULL, parent)

#define MALA_PARSER_LIST(name, parser, list) \
  MALA_PARSER(name, parser, MALA_DATA_STRINGLIST, MALA_STATIC_STRINGLIST_INIT list, "--WORLD")
#define MALA_PARSER_LIST_P(name, parser, list, parent) \
  MALA_PARSER(name, parser, MALA_DATA_STRINGLIST, MALA_STATIC_STRINGLIST_INIT list, parent)

#define MALA_PARSER_SUBSTITUTE(name,subst) \
  MALA_PARSER(name, mala_substitute_parser, MALA_DATA_STRINGPOINTER, subst, "--WORLD")
#define MALA_PARSER_SUBSTITUTE_P(name,subst,parent) \
  MALA_PARSER(name, mala_substitute_parser, MALA_DATA_STRINGPOINTER, subst, parent)

#define MALA_PARSER_EXPAND(name, expand) \
  MALA_PARSER(name, mala_expand_parser, MALA_DATA_STRINGLIST, \
              MALA_STATIC_STRINGLIST_INIT expand, "--WORLD")
#define MALA_PARSER_EXPAND_P(name, expand, parent) \
  MALA_PARSER(name, mala_expand_parser, MALA_DATA_STRINGLIST, \
              MALA_STATIC_STRINGLIST_INIT expand, parent)

#define MALA_PARSER_MACRO(name, macro) \
  MALA_PARSER(name, mala_macro_parser, MALA_DATA_STRINGLIST, \
              MALA_STATIC_STRINGLIST_INIT macro, "--WORLD")
#define MALA_PARSER_MACRO_P(name, macro, parent) \
  MALA_PARSER(name, mala_macro_parser, MALA_DATA_STRINGLIST, \
              MALA_STATIC_STRINGLIST_INIT macro, parent)

#define MALA_PARSER_BRIEF(name, brief) \
  MALA_PARSER_SUBSTITUTE_P("--BRIEF_" name, brief, name)

#define MALA_PARSER_HELP(name, args) \
  MALA_PARSER_EXPAND_P("--HELP_" name, args, name)

#if 0
#define MALA_PARSER_EXAMPLE(name, example) \
  MALA_PARSER_SUBSTITUTE_P("--EXAMPLE_" name, example, name)

#define MALA_PARSER_TYPE(name, explain)            \
  MALA_PARSER_SUBSTITUTE("--TYPE_" name, explain)

#define MALA_PARSER_RESULT_TYPE(name, args) \
  MALA_PARSER_EXPAND_P("--RESULT-TYPE_" name, args, name)

#define MALA_PARSER_RESULT_USAGE(name, args) \
  MALA_PARSER_EXPAND_P("--RESULT-USAGE_" name, args, name)

#define MALA_PARSER_SIGNATURE_TYPE(name, args) \
  MALA_PARSER_EXPAND_P("--SIGNATURE-TYPE_" name, args, name)

#define MALA_PARSER_SIGNATURE_USAGE(name, args) \
  MALA_PARSER_EXPAND_P("--SIGNATURE-USAGE_" name, args, name)
#endif 

#define MALA_PARSER_TRUE(name) \
  MALA_PARSER_SUBSTITUTE(name,"--TRUE")
#define MALA_PARSER_FALSE(name) \
  MALA_PARSER_SUBSTITUTE(name,"--FALSE")

int
mala_module_std_init (MalaEngine self);

#endif /* MALA_STD_H */
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: df02af8a-69d7-4d51-8acc-aa544cb3acbe
// end_of_file
*/

