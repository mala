/*
  std_strings.h - MaLa standard module strings

  Copyright (C) 2006, Christian Thaeter <ct@pipapo.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_STRINGS_H
#define MALA_STD_STRINGS_H

#include "std.h"

int
mala_module_std_strings_init (MalaEngine self);

#endif /* MALA_STD_STRINGS_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
*/
