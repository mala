/*
    std_init.c - MaLa standard module initialization table

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>

#include "mala.h"
#include "std.h"

static
mala_actioninit std_help[] =
  {
    /*
    MALA_PARSER("--", mala__parser, NULL, NULL, NULL),
    MALA_PARSER_BRIEF("--", ""),
    MALA_PARSER_HELP("--", ("TODO")),
    MALA_PARSER_SIGNATURE("--", ("")),
    MALA_PARSER_RESULT("--", ("")),

    */
    MALA_MACRO_PARSER("--TYPE", ("--TYPE_%-1")),
    MALA_PARSER_BRIEF("--TYPE", "Explains a type"),
    MALA_PARSER_HELP("--TYPE", ("Mala has a lazy type system, by default everything is treated asd string.  Type adding type information is optional, some extra tools will actually use it to do some useful things like command completion and so on.")),
    MALA_PARSER_SIGNATURE_TYPE("--TYPE", ("TYPE")),
    MALA_PARSER_RESULT_TYPE("--TYPE", ("VOID")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("Name of the type to explain")),
    MALA_PARSER_RESULT_USAGE("--", ("")),


#if 0
    MALA_MACRO_PARSER("--SIGNATURE", ("--SIGNATURE_%-1")),
    MALA_PARSER_BRIEF("--SIGNATURE", "Lists the options of a command"),
    MALA_PARSER_HELP("--SIGNATURE", ("TODO")),
    MALA_PARSER_SIGNATURE("--SIGNATURE", ("DEFINED-ACTION", "")),
    MALA_PARSER_RESULT("--SIGNATURE", ("VOID")),

    MALA_MACRO_PARSER("--RESULT", ("--RESULT_%-1")),
    MALA_PARSER_BRIEF("--RESULT", "explains the result of a command"),
    MALA_PARSER_HELP("--RESULT", ("TODO")),
    MALA_PARSER_SIGNATURE("--RESULT", ("DEFINED-ACTION")),
    MALA_PARSER_RESULT("--RESULT", ("VOID")),
#endif

    MALA_MACRO_PARSER("--BRIEF", ("--BRIEF-ACTION", "--BRIEF_%-1")),
    MALA_PARSER_BRIEF("--BRIEF", "gives a short note about a command"),
    MALA_PARSER_HELP("--BRIEF", ("TODO")),
    MALA_PARSER_SIGNATURE_TYPE("--BRIEF", ("DEFINED-ACTION")),
    MALA_PARSER_RESULT_TYPE("--BRIEF", ("VOID")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),

    /*
applies an action to each word of a macro/expansion
usage: --FOREACH-WORD DEFINED-ACTION MACRO
parameters:
  DEFINED-ACTION   - Action to be applied
  MACRO            - Macro consists of words on which the action should be appied
Result [SEQUENCE]:
  Expansion which applies the Action to the macro
    */

    MALA_MACRO_PARSER("--PRINT_SPACE", ("--PRINT", "--LITERAL", "%-1",
                                        "--PRINT", " ")),

    MALA_MACRO_PARSER("--HELP", ("--IFDEF", "--BRIEF_%-1",
                                 "--BEGIN",
                                 "--PRINTL", "--BRIEF_%-1",
                                 "--END",
                                 "--ELSE"
                                 "--BEGIN",
                                 "--PRINTL", "%-1",
                                 "--END",

                                 "--IFDEF", "--SIGNATURE_TYPE_%-1",
                                 "--BEGIN",
                                 "--PRINT", "usage: %-1 ",
                                 "--FOREACH-WORD","--LITERAL","--PRINT_SPACE",
                                 "--SIGNATURE-TYPE_%-1",
                                 "--NL",
                                 "--END",

                                 "--IFDEF", "--SIGNATURE_USAGE_%-1",
                                 "--BEGIN",
                                 "--NL",
                                 "--PRINTL","parameters:",
                                 //                                 "--FOREACH-WORD","--LITERAL","--PRINT_SPACE",
                                 //"--SIGNATURE-USAGE_%-1",
                                 "--END",

                                 "--IFDEF", "--RESULT-TYPE_%-1",
                                 "--BEGIN",
                                 "--PRINT","result [","--PRINT","--RESULT-TYPE_%-1",
                                 "--PRINTL","]:",
                                 "--END",

                                 "--IFDEF", "--RESULT-USAGE_%-1",
                                 "--BEGIN",
                                 "--PRINT","  ","--PRINTL","--RESULT-USAGE_%-1",
                                 "--PRINTL", "--HELP_%-1"
                                 "--END"
                                 )),
    MALA_PARSER_BRIEF("--HELP", "gives help about some command"),
    MALA_PARSER_HELP("--HELP", ("Any command in Mala might be associated with some buildin help, use --HELP --somecommand to show this help")),
    MALA_PARSER_SIGNATURE_TYPE("--HELP", ("DEFINED-ACTION")),
    MALA_PARSER_RESULT_TYPE("--HELP", ("VOID")),
    MALA_PARSER_SIGNATURE_USAGE("--", ("")),
    MALA_PARSER_RESULT_USAGE("--", ("")),

    MALA_ACTIONS_END
  };

int
mala_module_std_help_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_help);
}


/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 5d28d2a3-4ceb-4d09-ab30-2912d0710fc8
// end_of_file
*/
