/*
  std_io.h - MaLa standard IO parser declarations

  Copyright (C) 2004, 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_STD_IO_H
#define MALA_STD_IO_H

#include "std.h"

MALA_PARSER_DEFINE(newline);
MALA_PARSER_DEFINE(print);
MALA_PARSER_DEFINE(printwraped);

int
mala_module_std_io_init (MalaEngine self);

#endif /* MALA_STD_IO_H */

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 0a7bc468-391e-4679-bb8d-93afa982ee49
// end_of_file
*/
