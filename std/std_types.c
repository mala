/*
    std_init.c - MaLa standard module initialization table

  Copyright (C) 2005, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#include <stdio.h>
#include <string.h>

#include "mala.h"
#include "std_types.h"

static
mala_actioninit std_types[] =
  {
    MALA_PARSER_TYPE("WORD", "Just any word"),
    MALA_PARSER_TYPE("MACRO", "Any Action which will expand to one or more words"),

    MALA_PARSER_TYPE("ACTION", "Name of an action"),
    MALA_PARSER_TYPE("SEQUENCE", "a Mala program sequence"),
    MALA_PARSER_TYPE("WORD-OR-BLOCK","Single word or block delimited by --BEGIN and --END"),
    MALA_PARSER_TYPE("DEFINED-ACTION","Name of an already defined action"),
    MALA_PARSER_TYPE("VOID","nothing"),
    MALA_PARSER_TYPE("TYPE","Some Datatype"),

    MALA_PARSER_TYPE("BLOCK","TODO"),

    MALA_PARSER_TYPE("WORDS","a sequence of words"),
    MALA_PARSER_TYPE("END","--END"),

    MALA_PARSER_TYPE("ERROR","Will raise an error"),

    MALA_PARSER_TYPE("FILE-EXISTING","File which already exists"),
    MALA_PARSER_TYPE("FILE-NEW","File which does not exist"),
    MALA_PARSER_TYPE("FILE","Some File"),

    MALA_PARSER_TYPE("DIR-EXISTING","Directory wich already exists"),
    MALA_PARSER_TYPE("DIR-NEW","Directory which does not exist"),
    MALA_PARSER_TYPE("DIR","Directory"),

    MALA_PARSER_TYPE("INTEGER","an integer Number"),
    MALA_PARSER_TYPE("STRING","a String"),

    MALA_ACTIONS_END
  };

int
mala_module_std_types_init (MalaEngine self)
{
  return mala_engine_actions_register (self, std_types);
}

/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 0069b6ab-8173-4cb0-98d3-92c621bdeac5
// end_of_file
*/
