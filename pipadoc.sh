#!/bin/bash
#
# pipadoc - C++ Documentation extractor
#
# Copyright (C) 2002, Pipapo Project, Christian Thaeter
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, contact to any of the Pipapo maintainers.
#

pipadoc_version=0.1

COM='//'
DOC='_'
SUB='='


function usage
{
    cat <<EOF

  Extract Documentation-Text from C/C++ files

usage:   pipadoc files...
         files are C/C++ sourcefiles with embedded Documentation strings
         after '//_' or '//__' comments. This Doc-Comments can
         optionally be followed by a tag. '//_' comments are
         stable-sorted in alphabetic order on a per-file basis.
         '//__' comments are globally stable-sorted in alphabetic
         order.
         '//_' and '//__' comments without a tag are sorted as
         '//_5' resp. '//__5'. Output is written to stdout.
         Pipadoc defines 'PIPADOC' while processing the sourcefiles and
         further the preprocessing can be tuned with the CPPFLAGS and
         PIPADOCFLAGS environment variables.
         Tag Substitutions:
         It is possible to define substitutions for common tags to make
         the sourcecode easier readable:
         '//_=foo=bar=' will replaces any '//foo' with '//bar' in this file.
         '//__=foo=bar=' will replaces any '//foo' with '//bar' in all proccessed files.
         example: '//__=AA=__70A5=' all '//AA' comments are processed at '//__70A5'
example:
         pipadoc preamble.cpp title.cpp doc.cpp prolog.cpp

EOF
}

if [ $# = 0 ]; then
    usage;
    exit;
fi

# ----------------------------------------------------------------------------
# -------- READING THE SOURCE BEYOND THIS LINE CAN DAMAGE UR HEALTH ----------
# ----------------------------------------------------------------------------














































# YOU ARE WARNED !!!!!!!!!!!!!!!!!!!!!!!!!!!






































TMP_GSUB=/tmp/pipadoc_$$_gsub
TMP_GTMP=/tmp/pipadoc_$$_gtmp

for i in "$@"; do
    echo "$i" >&2

    TMP_RIP=/tmp/pipadoc_$$_rip
    TMP_LSUB=/tmp/pipadoc_$$_lsub

    cat $i | g++ -DPIPADOC -E -C -P -I. $PIPADOCFLAGS $CPPFLAGS - \
	| egrep "^.*${COM}" | uniq >$TMP_RIP

    cat $TMP_RIP |  egrep "^.*${COM}${DOC}${DOC}${SUB}" |
    sed -e "s|\(${COM}\)\(${DOC}${DOC}\)\(${SUB}\)\([[:alnum:]][[:alnum:]_]*\)\
${SUB}\([[:alnum:]_]*\)${SUB}|s\3^.*\1\4\3\1\5\3|" >>$TMP_GSUB

    cat $TMP_RIP |  egrep "^.*${COM}${DOC}${SUB}" |
    sed -e "s|\(${COM}\)\(${DOC}\)\(${SUB}\)\([[:alnum:]][[:alnum:]_]*\)\
${SUB}\([[:alnum:]_]*\)${SUB}|s\3^.*\1\4\3\1\5\3|" >>$TMP_LSUB

    sed -f $TMP_LSUB $TMP_RIP \
	-e "s|^.*\(${COM}.*\)|\1|" \
	-e "\|^${COM}[[:space:]]?*.*$|d" \
	-e "\|^${COM}${DOC}*${SUB}|d" \
	-e "s|^\(${COM}${DOC}\)\([[:space:]].*\)$|\15\2|" \
	| sort +0 -1 -s \
	| sed -e "s|^\(${COM}${DOC}\)\([^${DOC}][[:alnum:]_]*\)\(.*\)$|\1${DOC}5\3|"


    rm -f $TMP_LSUB
    rm -f $TMP_RIP
done >>$TMP_GTMP

sed -f $TMP_GSUB $TMP_GTMP \
    -e "s|^${COM}${DOC}${DOC}\([[:space:]]?*\)|${DOC}5\1|" \
    -e "s|^${COM}${DOC}${DOC}|${DOC}|" \
    -e "\|^${COM}.*$|d" \
    | sort +0 -1 -s \
    | sed -e "s|^${DOC}[[:alnum:]][[:alnum:]_]*[[:space:]]*||" \
    -e 's|@/|\\|g'


rm -f $TMP_GSUB
rm -f $TMP_GTMP

# arch-tag: c7f177a4-3ebc-4a91-86d3-2901f4c86a62
