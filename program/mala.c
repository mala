/*
    mala.c - standalone MaLa interpreter

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "mala.h"
#include "std.h"

mala_actioninit my_actions[] =
  {
    //MALA_SUBSTITUTE_PARSER("--MALA-VERSION","MaLa 0.2 ("__DATE__" "__TIME__")"),
    //MALA_SUBSTITUTE_PARSER("-V","--version"),
    // TODO MALA_EXPAND_PARSER("--version",("--PRINTL", "--MALA-VERSION")),

    MALA_ACTIONS_END
  };

int
main(int argc, char * argv[])
{
  (void) argc;
  (void) argv;

  int ret;
  MalaEngine my_engine;

  my_engine = mala_engine_new ();
  //if (getenv ("MALA_GCTRACE"))
  //  *my_engine->gc_trace = MALA_TRACE;
  if (getenv ("MALA_TRACE"))
    my_engine->engine_trace = MALA_TRACE;

  //mala_module_std_init (my_engine);
  mala_engine_actions_register (my_engine, my_actions);

  //TODO  mala_engine_ppexpand_define (my_engine, "--COMMANDLINE", argc - 1, argv + 1);
  // TODO mala_engine_pushback_word (my_engine, "--COMMANDLINE");
  //mala_engine_run (my_engine);
  if (my_engine->engine_trace)
    {
      fprintf (stderr, "%-8s:\n", mala_engine_state_str (my_engine));
    }
  ret = mala_engine_state_get (my_engine);
  if (ret > MALA_EFAULT)
    {
      //      mala_engine_dumpprogram (my_engine, stderr, "", " ");
      putc ('\n', stderr);
    }
  //  else if (! mala_stringlist_is_empty (&my_engine->arguments))
  //  {
      //mala_engine_dumpargs (my_engine, stdout, "", " ");
  //    putc ('\n', stdout);
  //  }
  mala_engine_free (my_engine);
  return ret; // == MALA_SUCCESS ? 0 : ret;
}



/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: 0aebb81a-80ba-46d5-9738-4af8f3ceffc9
// end_of_file
*/
