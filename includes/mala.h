/*
    mala-glibc.h - declarations for the glibc implementation of MaLa

  Copyright (C) 2004, Christian Thaeter <chth@gmx.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2 as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, contact me.

*/

#ifndef MALA_H
#define MALA_H

#include "engine/mala_types.h"
#include "engine/engine.h"
#include "engine/action.h"
#include "engine/strings.h"
#include "engine/program.h"

/*
 * Macros for parsers
 */
/*TODO macros for common parsers MALA_SUBSTITUTE_PARSER("FOO","BAR")  etc */

#if 0
#define MALA_CHECK_NOTNEGATED                                           \
  if (mala_engine_is_negated (eng))                                     \
    return mala_engine_exception (eng, pptr, pptr, "--ERROR-NEGATED")

#define MALA_CHECK_HASARG                                               \
  if (llist_get_next ((LList) pptr) == mala_engine_getprogram (eng))    \
    return mala_engine_exception (eng, pptr, pptr, "--ERROR-MISSINGARG")

#define MALA_CHECK_HASARGS(n)                                           \
  if (llist_distance ((LList) pptr, mala_engine_getprogram (eng)) < n)  \
    return mala_engine_exception (eng, pptr,                            \
           llist_get_tail (mala_engine_getprogram (eng)),               \
           "--ERROR-MISSINGARG")
#endif

#endif
/*
// Local Variables:
// mode: C
// c-file-style: "gnu"
// End:
// arch-tag: bf4fcf70-efe9-4c6e-baab-629a8e195bc3
// end_of_file
*/
