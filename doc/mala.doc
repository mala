/*
if u want to add something to the main info-node menu use NODEnn for any
menu definitions, where nnn is a number for ordering the items .. to get them
in the right order u need to acknowledge with the other developers, the
chapternumber of your part multiplied with 10 plus 5 might be
a good choice nnn must be of 3 digits!
example:
 NODE105 * The Third Chapter:: This blah...
*/
//__=NODE=__45=

/*
same as above but prefixed with APPENDIXNnnn
where N is the uppercase appendix character-number
*/
//__=APPENDIX=__70=

/*
adding words to the glossary:
 GLOSSfoobar @GLOSS{foobar,a common metasyntactic variable}
the first sorting 'foobar' must be lowercase
the index-letters need to be enabled in /doc/doc.cpp
*/
//__=GLOSS=__75=

/*
Adding your name on the main book-copyright page
if you write some part of the documentation and you are not mentioned in the copyright
of the main Book copyright notice then add the following lines to the main part of ur
documentation (please don't change the documentation system setup for this)

 NAME1lastname Firstname Lastname,
 NAME2lastname Firstname Lastname,
 NAME3lastname @author firstname Lastname

note: 26 and 36 no other numbers!  don't forget the commas.
the first lowercase 'lastname' is used for sorting the people in alphabetical order
take care that u dont do this more than once.
*/
/*
Authors for chapters
When u setup a new chapter in a book, add the following lines to introduce the chapter:

 CHAPTER @chapter{title}
 CHAPTERAUTHORS @emph{ This Chapter is written by
 CHAPTEREND .}

so later anyone who writes some part of the chapter can add himself with:

 AUTHORlastname Firstname Lastname

to the list of authors. this line is a bit free form, i can add annotations what u have done,
commas and 'and' and so on. please check the results if u do something special.
*/
//__=CHAPTER=_150=
//__=CHAPTERAUTHORS=_151=
//__=CHAPTEREND=_153=
//__=AUTHOR=_152=

/*
macro shortcuts:
there are some macros to explain some issues

@INFO Background information
@NOTE surprising feature
@IMPO requirement
@WARN common pitfall
*/



//__=preamble=__000=
//__=epilog=__999=

// preamble \input texinfo
// preamble @include version.texi
//preamble @settitle Pipapomisc
//preamble @c index of macros
//preamble @defcodeindex ma
// preamble @c index of modules
// preamble @defcodeindex mo

//preamble @macro NOTE {}
//preamble
//preamble @noindent @strong{Note: }
//preamble @end macro

//preamble @macro INFO {}
//preamble
//preamble @noindent @strong{Info: }
//preamble @end macro

//preamble @macro IMPO {}
//preamble
//preamble @noindent @strong{Important: }
//preamble @end macro

//preamble @macro WARN {}
//preamble
//preamble @noindent @strong{Warning: }
//preamble @end macro

//preamble @rmacro TAB {what}
//preamble @ @ @ @ @ @ @ @ \what\
//preamble @end rmacro

//preamble @macro GLOSS {word,expl}
//preamble
//preamble @item \word\ @dots{}
//preamble @noindent \expl\
//preamble
//preamble @end macro


//__20 @ifinfo
//__20 The MaLa extension language.
//__25 Copyright @copyright{} 2005
//__27 Pipapo Project.
//__27 @end ifinfo
//__=NAMEINFO=__26=


//__=TITLEPAGE=__30=
//__=ENDTITLEPAGE=__34=
//__=NAMETITLE=__32=

//TITLEPAGE @titlepage
//TITLEPAGE @title The Mala extension language
//TITLEPAGE @subtitle Pipapo Project
//TITLEPAGE @subtitle Edition @value{EDITION}, version @value{VERSION}
//TITLEPAGE @subtitle @value{UPDATED}
//ENDTITLEPAGE @end titlepage

//__=COLOPHON1=__35=
//__=COLOPHON2=__37=
//__=NAMECOLOPHON=__36=

//COLOPHON1 @page
//COLOPHON1 @vskip 0pt plus 1filll
//COLOPHON1 Copyright @copyright{} 2005
//COLOPHON2 Pipapo Project.
// COLOPHON2 @end page


//__=MENUBEGIN=__40=
//__=MENU=__41=
//__=MENUEND=__42=

// MENUBEGIN main node
// MENUBEGIN @menu
// user-menues
// MENUEND @end menu

//__48 @shortcontents

//__=BODY=__50=

// __60  end node

// __70 appendixes
//__74 @unnumbered Glossary
//__74 @table @sc
//__76 @end table

// GLOSS0 @*@GLOSS{0,}
// GLOSS1 @*@GLOSS{1,}
// GLOSS2 @*@GLOSS{2,}
// GLOSS3 @*@GLOSS{3,}
// GLOSS4 @*@GLOSS{4,}
// GLOSS5 @*@GLOSS{5,}
// GLOSS6 @*@GLOSS{6,}
// GLOSS7 @*@GLOSS{7,}
// GLOSS8 @*@GLOSS{8,}
// GLOSS9 @*@GLOSS{9,}
// GLOSSa @*@GLOSS{A,}
// GLOSSb @*@GLOSS{B,}
// GLOSSc @*@GLOSS{C,}
// GLOSSd @*@GLOSS{D,}
// GLOSSe @*@GLOSS{E,}
// GLOSSf @*@GLOSS{F,}
// GLOSSg @*@GLOSS{G,}
// GLOSSh @*@GLOSS{H,}
// GLOSSi @*@GLOSS{I,}
// GLOSSi @*@GLOSS{I,}
// GLOSSj @*@GLOSS{J,}
// GLOSSk @*@GLOSS{K,}
// GLOSSl @*@GLOSS{L,}
// GLOSSm @*@GLOSS{M,}
// GLOSSn @*@GLOSS{N,}
// GLOSSo @*@GLOSS{O,}
// GLOSSp @*@GLOSS{P,}
// GLOSSq @*@GLOSS{Q,}
// GLOSSr @*@GLOSS{R,}
// GLOSSs @*@GLOSS{S,}
// GLOSSt @*@GLOSS{T,}
// GLOSSu @*@GLOSS{U,}
// GLOSSv @*@GLOSS{V,}
// GLOSSw @*@GLOSS{W,}
// GLOSSx @*@GLOSS{X,}
// GLOSSy @*@GLOSS{Y,}
// GLOSSz @*@GLOSS{Z,}


//__=INDEX=__80=

// INDEX printindices
// INDEX @unnumbered Exception Index
// INDEX @printindex ex
// INDEX @sp 3
// INDEX @unnumbered Hierachy Index
// INDEX @printindex hi
// INDEX @sp 3
//INDEX @unnumbered Macro Index
//INDEX @printindex ma
// INDEX @sp 3
// INDEX @unnumbered Namespace Index
// INDEX @printindex na
// INDEX @sp 3
// INDEX @unnumbered Type Index
// INDEX @printindex tp
// INDEX @sp 3
//INDEX @unnumbered Concept Index
//INDEX @printindex cp
//INDEX
//INDEX @contents

// arch-tag: 29b6958e-a4cc-4161-9a63-744b07a47713
